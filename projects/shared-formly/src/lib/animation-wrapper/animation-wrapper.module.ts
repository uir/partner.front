import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {FormlyModule} from "@ngx-formly/core";
import {AnimationWrapperComponent} from "./animation-wrapper.component";

export class AnimationWrapper {
  run(fc) {
    fc.templateManipulators.preWrapper
      .push(field => 'animation');
  }
}

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FormlyModule.forChild({
      wrappers: [
        {name: 'animation', component: AnimationWrapperComponent},
      ],
      manipulators: [
        {class: AnimationWrapper, method: 'run'},
      ],
    })
  ],
  declarations: [AnimationWrapperComponent],
  exports: [AnimationWrapperComponent]
})
export class AnimationWrapperModule {
}
