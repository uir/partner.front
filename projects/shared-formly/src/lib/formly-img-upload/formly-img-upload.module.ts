import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormlyImgUploadComponent} from './formly-img-upload/formly-img-upload.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {FormlyModule} from "@ngx-formly/core";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FormlyModule.forChild({
      types: [
        {name: 'img-upload', component: FormlyImgUploadComponent},
      ]
    })
  ],
  declarations: [FormlyImgUploadComponent],
  exports: [FormlyImgUploadComponent]
})
export class FormlyImgUploadModule { }
