import {Component} from '@angular/core';
import {FieldType} from "@ngx-formly/core";

@Component({
  selector: 'app-formly-img-upload',
  template: `
    <div class="file-upload">
      <input id="file-5" type="file" accept="image/*" (change)="handleInputChange($event)"/>
      <label for="file-5">
        <img *ngIf="imageSrc" [src]="imageSrc" style="max-width:300px;max-height:300px"/>
        <figure *ngIf="!imageSrc">
          <svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
            <path
              d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"></path>
          </svg>
        </figure>
        <span [class.mat-form-field-invalid]="form.invalid">
          {{to.label}}
          <span *ngIf="to.required"> *</span>
        </span>
      </label>
    </div>
  `,
  styles: [`
    .file-upload {
      margin-bottom: 25px;
    }

    .file-upload label {
      max-width: 80%;
      text-overflow: ellipsis;
      white-space: nowrap;
      cursor: pointer;
      display: flex;
      flex-direction: column-reverse;
      overflow: hidden;
      width: 150px;
    }

    .file-upload input {
      width: 0.1px;
      height: 0.1px;
      opacity: 0;
      overflow: hidden;
      position: absolute;
      z-index: -1;
    }

    .file-upload figure {
      width: 150px;
      height: 150px;
      border-radius: 50%;
      background-color: #d3394c;
      display: block;
      padding: 30px;
      padding-top: 18px;
      margin-top: 10px;
    }

    .file-upload svg {
      width: 100%;
      height: 100%;
      fill: #f1e5e6;
    }

    .file-upload figure:hover {
      background-color: #722040;
    }

    .file-upload img {
      height: 150px;
      width: 150px;
      border-radius: 100%;
    }
  `]
})
export class FormlyImgUploadComponent extends FieldType {
  private imageSrc: string = null;

  handleInputChange(e) {
    let file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    let pattern = /image-*/;
    let reader = new FileReader();
    if (!file.type.match(pattern)) {
      alert('invalid format');
      return;
    }
    reader.onload = (e2: any) => {
      let reader = e2.target;
      this.imageSrc = reader.result;
      this.form.get(this.field.key).setValue({
        file: reader.result.replace(/^data:(.*;base64,)?/, ''),
        name: file.name
      });
    };
    reader.readAsDataURL(file);
  }
}
