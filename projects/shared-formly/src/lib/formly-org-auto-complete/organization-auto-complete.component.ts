import {Component, OnInit} from '@angular/core';
import {FieldType} from "@ngx-formly/core";
import {Suggestion} from "./dadata.model";

declare var $: any;

@Component({
  selector: 'app-organization-auto-complete',
  template: `
    <mat-form-field class="example-full-width">
      <input matInput id="party" type="text" name="party"
             [formControl]="formControl" 
             [formlyAttributes]="field" 
             [placeholder]="field.templateOptions.label">
    </mat-form-field>
  `,
  styleUrls: ['./organization-auto-complete.component.css']
})
export class OrganizationAutoCompleteComponent extends FieldType implements OnInit {
  ngOnInit() {
    $("#party").suggestions({
      token: "e1c7ad541aba4a1fc2721be83b26179d022ef4b4",
      type: "PARTY",
      count: 5,
      onSelect: (suggestion: Suggestion) => {
        this.to.onSelect(suggestion, this.form);
      }
    });
  }
}
