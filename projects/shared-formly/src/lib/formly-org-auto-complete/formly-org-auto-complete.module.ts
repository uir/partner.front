import {NgModule} from '@angular/core';
import {FormlyModule} from "@ngx-formly/core";
import {OrganizationAutoCompleteComponent} from "./organization-auto-complete.component";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule, MatInputModule} from "@angular/material";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    FormlyModule.forChild({
      types: [
        {name: 'org-autocomplete', component: OrganizationAutoCompleteComponent},
      ]
    })
  ],
  declarations: [OrganizationAutoCompleteComponent],
  exports: [OrganizationAutoCompleteComponent]
})
export class FormlyOrgAutoCompleteModule {
}
