import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {FormlyMaterialModule} from "@ngx-formly/material";
import {FormlyMatToggleModule} from "@ngx-formly/material/toggle";
import {FormlyMatDatepickerModule} from "@ngx-formly/material/datepicker";
import {FormlyModule} from "@ngx-formly/core";
import {FormlyOrgAutoCompleteModule} from "./formly-org-auto-complete/formly-org-auto-complete.module";
import {FormlyAddressAutoCompleteModule} from "./formly-address-auto-complete/formly-address-auto-complete.module";
import {AnimationWrapperModule} from "./animation-wrapper/animation-wrapper.module";
import {FormlyImgUploadModule} from "./formly-img-upload/formly-img-upload.module";
import {FormlyQuillModule} from "./formly-quill/formly-quill.module";
import {
  emailValidation,
  emailValidationMessage,
  maxValidationMessage,
  passwordValidation, passwordValidationMessage
} from "./validators/formly.validator";

export const formlyConf = {
  validators: [
    {name: 'email', validation: emailValidation},
    {name: 'password', validation: passwordValidation}
  ],
  validationMessages: [
    {name: 'required', message: 'Обязательно для заполнения'},
    {name: 'max', message: maxValidationMessage},
    {name: 'email', message: emailValidationMessage},
    {name: 'password', message: passwordValidationMessage},
  ]
};

export const FormModules = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
  HttpClientModule
];

export const FormlyModules = [
  ...FormModules,
  FormlyQuillModule,
  FormlyOrgAutoCompleteModule,
  FormlyAddressAutoCompleteModule,
  FormlyImgUploadModule,
  AnimationWrapperModule,
  FormlyMaterialModule,
  FormlyMatToggleModule,
  FormlyMatDatepickerModule,
  FormlyModule.forRoot(formlyConf),
];
