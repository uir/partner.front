import {ModuleWithProviders, NgModule} from '@angular/core';
import {FormlyQuillModule} from "./formly-quill/formly-quill.module";
import {FormlyOrgAutoCompleteModule} from "./formly-org-auto-complete/formly-org-auto-complete.module";
import {FormlyAddressAutoCompleteModule} from "./formly-address-auto-complete/formly-address-auto-complete.module";
import {FormlyImgUploadModule} from "./formly-img-upload/formly-img-upload.module";
import {AnimationWrapperModule} from "./animation-wrapper/animation-wrapper.module";
import {FormlyMaterialModule} from "@ngx-formly/material";
import {FormlyMatToggleModule} from "@ngx-formly/material/toggle";
import {FormlyMatDatepickerModule} from "@ngx-formly/material/datepicker";
import {FormlyModule} from "@ngx-formly/core";
import {formlyConf} from "./shared-form.module";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  imports: [],
  declarations: [],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormlyModule,
    FormlyQuillModule,
    FormlyOrgAutoCompleteModule,
    FormlyAddressAutoCompleteModule,
    FormlyImgUploadModule,
    AnimationWrapperModule,
    FormlyMaterialModule,
    FormlyMatToggleModule,
    FormlyMatDatepickerModule,
  ]
})
export class SharedFormlyModule {
  public static forRoot(): ModuleWithProviders[] {
    return [
      {ngModule: SharedFormlyModule, providers: []},
      FormlyModule.forRoot(formlyConf),
    ];
  }
}
