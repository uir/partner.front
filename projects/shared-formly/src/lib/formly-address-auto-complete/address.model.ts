export class Address{
  building: string;
  street: string;
  locality: string;
  region: string;
  country: string;
  index: number;
  formattedAddress: string;
  lat: string;
  lng: string;
}
