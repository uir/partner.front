import {Component, ViewChild} from '@angular/core';
import {FieldType} from "@ngx-formly/core";
import {Address} from "../address.model";

declare var google: any;

@Component({
  selector: 'app-formly-address-auto-complete',
  template: `
    <mat-form-field class="example-full-width">
      <input matInput #input type="text"
             [formControl]="formControl"
             [formlyAttributes]="field"
             [placeholder]="field.templateOptions.label">
    </mat-form-field>
  `,
  styleUrls: ['./formly-address-auto-complete.component.css']
})
export class FormlyAddressAutoCompleteComponent extends FieldType {

  @ViewChild('input') input;

  ngOnInit() {
    let autocomplete = new google.maps.places.Autocomplete(this.input.nativeElement, {
      types: ["address"]
    });
    autocomplete.addListener("place_changed", () => {
      let place = autocomplete.getPlace();

      if (place.geometry === undefined || place.geometry === null) {
        return;
      }

      let model = new Address();
      model.building = this.getAddressComponent(place, 'street_number');
      model.street = this.getAddressComponent(place, 'route');
      model.locality = this.getAddressComponent(place, 'administrative_area_level_2');
      model.region = this.getAddressComponent(place, 'administrative_area_level_1');
      model.country = this.getAddressComponent(place, 'country');
      model.index = Number(this.getAddressComponent(place, 'postal_code'));
      model.formattedAddress = place.formatted_address;
      model.lat = place.geometry.location.lat();
      model.lng = place.geometry.location.lng();

      this.to.onSelect(model, this.form);
    });
  }

  getAddressComponent(place, type) {
    let addComponent = place.address_components.find(x => x.types && x.types[0] == type);
    return addComponent ? addComponent.long_name : '';
  }

}
