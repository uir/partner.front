import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormlyAddressAutoCompleteComponent} from './formly-address-auto-complete/formly-address-auto-complete.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule, MatInputModule} from "@angular/material";
import {FormlyModule} from "@ngx-formly/core";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    FormlyModule.forChild({
      types: [
        {name: 'address-autocomplete', component: FormlyAddressAutoCompleteComponent},
      ]
    })
  ],
  declarations: [FormlyAddressAutoCompleteComponent],
  exports: [FormlyAddressAutoCompleteComponent]
})
export class FormlyAddressAutoCompleteModule { }
