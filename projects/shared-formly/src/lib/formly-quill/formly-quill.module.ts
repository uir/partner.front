import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormlyQuillComponent} from './formly-quill/formly-quill.component';
import {QuillEditorModule} from "ngx-quill-editor/quillEditor.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {FormlyModule} from "@ngx-formly/core";

@NgModule({
  imports: [
    CommonModule,
    QuillEditorModule,
    FormsModule,
    ReactiveFormsModule,
    FormlyModule.forChild({
      types: [
        {name: 'quill', component: FormlyQuillComponent},
      ]
    })
  ],
  declarations: [FormlyQuillComponent],
  exports: [FormlyQuillComponent]
})
export class FormlyQuillModule { }
