import {Component} from '@angular/core';
import {FieldType} from "@ngx-formly/core";

@Component({
  selector: 'app-formly-quill',
  template: `
    <div class="quill-wrapper">
      <quill-editor [formControl]="formControl"
                    [options]="quillOptions"
                    (change)="onContentChanged($event)">
      </quill-editor>
    </div>
  `,
  styles: [`
    .quill-wrapper {
      margin-bottom: 25px;
    }

    :host >>> .quill-editor {
      height: min-content;
    }
  `]
})
export class FormlyQuillComponent extends FieldType {
  onContentChanged({quill, html, text}) {
    this.form.get(this.field.key).setValue(html);
  }

  get quillOptions(){
    return {
      ...this.to.quillOptions,
      readOnly: this.to.readOnly
    }
  }
}
