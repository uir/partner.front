import {ErrorHandler} from "../types/error-handler.types";
import {HttpErrorResponse} from "@angular/common/http";
import {LOGIN_URL} from "../services/auth.service";
import {Router} from "@angular/router";
import {notificationType} from "../model/notification.model";

export const defaultErrorHandlers: ErrorHandler[] = [
  (error, injector) => {
    if (error instanceof HttpErrorResponse && error.status == 400 && error.error.error === 'invalid_grant') {
      let loginUrl = injector.get(LOGIN_URL);
      let router = injector.get(Router);
      router.navigate([loginUrl]);
      return {
        type: notificationType.danger,
        message: null
      }
    }
    return null;
  },
  (error, injector) => {
    if (error instanceof HttpErrorResponse && error.status == 401 || error.status == 403) {
      let loginUrl = injector.get(LOGIN_URL);
      let router = injector.get(Router);
      router.navigate([loginUrl]);
      return {
        type: notificationType.danger,
        message: error.error.message
      }
    }
    return null;
  },
  error => {
    if (error instanceof HttpErrorResponse && error.status == 400) {
      return {
        type: notificationType.danger,
        message: error.error.message
      }
    }
    return null;
  },
  error => {
    if (error instanceof HttpErrorResponse && error.status == 422) {
      return {
        type: notificationType.danger,
        message: "Ошибка!"
      }
    }
    return null;
  }
];
