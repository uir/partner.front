import {Inject, Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivateChild, Router, RouterStateSnapshot} from '@angular/router';
import {AUTH_STORAGE_KEY, LOGIN_URL} from "../services/auth.service";

@Injectable({providedIn: "root"})
export class AuthTokenGuard implements CanActivateChild {

  constructor(private router: Router,
              @Inject(AUTH_STORAGE_KEY) private storageKey: string,
              @Inject(LOGIN_URL) private loginUrl: string
  ) {
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (localStorage.getItem(this.storageKey))
      return true;
    else
      this.router.navigate([this.loginUrl]);
    return false;
  }
}
