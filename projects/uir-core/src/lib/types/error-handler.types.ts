import {Injector} from "@angular/core";

export type HandlerResult = { type: string, message: string; };
export type ErrorHandler = (error: any, injector?: Injector) => HandlerResult;
