import {ModuleWithProviders, NgModule} from '@angular/core';
import {HttpService} from "./services/http.service";
import {AUTH_STORAGE_KEY, AuthService} from "./services/auth.service";
import {AuthTokenGuard} from "./guards/auth-token.guard";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {NotificationService} from "./services/notification.service";
import {defaultErrorHandlers} from "./data/default-error-handlers.data";
import {ErrorHandler} from "./types/error-handler.types";
import {ERROR_HANDLERS, ErrorsInterceptor} from "./services/error.interceptor";

@NgModule({
  imports: [],
  providers: [
    HttpService,
    AuthService
  ]
})
export class UirCoreModule {
  public static forRoot(options: { storageKey: string; errorHandlers?: ErrorHandler[] }): ModuleWithProviders[] {
    return [
      {
        ngModule: UirCoreModule,
        providers: [
          NotificationService,
          AuthTokenGuard,
          HttpService,
          AuthService,
          {provide: HTTP_INTERCEPTORS, useClass: ErrorsInterceptor, multi: true},
          {provide: AUTH_STORAGE_KEY, useValue: options.storageKey},
          {provide: ERROR_HANDLERS, useValue: [...defaultErrorHandlers, ...options.errorHandlers]}
        ]
      }
    ];
  }
}
