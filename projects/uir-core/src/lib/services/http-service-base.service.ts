import {HttpService} from "./http.service";
import {HttpClient} from "@angular/common/http";
import {Inject} from "@angular/core";
import {AUTH_STORAGE_KEY} from "./auth.service";

export class HttpServiceBase {

  protected http: HttpService;

  constructor(private httpClient: HttpClient, @Inject(AUTH_STORAGE_KEY) storageKey: string) {
    this.http = new HttpService(httpClient, storageKey);
  }
}
