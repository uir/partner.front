import { Injectable } from '@angular/core';

declare var $: any;
declare var swal: any;

@Injectable()
export class NotificationService {
    notify(type, message) {
        $.notify(
            {
                icon: "notifications",
                message: message
            },
            {
                type: type,
                timer: 3000,
                placement: {
                    from: 'bottom',
                    align: 'right'
                }
            });
    }

    alertDeleteSuccess(){
        swal({
            title: 'Удалено!',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
            buttonsStyling: false
        });
    }

    alertSaveSuccess(){
        swal({
            title: 'Сохранено!',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
            buttonsStyling: false
        });
    }
}