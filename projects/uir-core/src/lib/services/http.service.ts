import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from "rxjs";
import {Inject, Injectable} from "@angular/core";
import {AUTH_STORAGE_KEY, retrieveTokens} from "./auth.service";

@Injectable()
export class HttpService {

  constructor(protected http: HttpClient, @Inject(AUTH_STORAGE_KEY) private storageKey: string) {
  }

  POST<T>(url, data?, headers?: HttpHeaders): Observable<T> {
    return this.http.post<T>(url, data, {
      headers: headers != null ? headers : this.getHeaders()
    });
  }

  PATCH<T>(url, data?, headers?: HttpHeaders): Observable<T> {
    return this.http.patch<T>(url, data, {
      headers: headers != null ? headers : this.getHeaders()
    });
  }

  DELETE<T>(url, data?): Observable<T> {
    return this.http.delete<T>(url, {
      headers: this.getHeaders(),

    });
  }

  GET<T>(url, params?: HttpParams, headers?: HttpHeaders): Observable<T> {
    return this.http.get<T>(url, {
      headers: headers != null ? headers : this.getHeaders(),
      params: params
    });
  }

  getHeaders(): HttpHeaders {
    console.log('HttpService', this.storageKey);
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    let token = retrieveTokens(this.storageKey);
    let accessToken = token && token.access_token;
    if (token != null) {
      headers = headers.set('Authorization', 'bearer ' + accessToken)
    }
    return headers;
  }
}
