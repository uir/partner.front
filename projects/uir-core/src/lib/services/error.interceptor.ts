import {Inject, Injectable, InjectionToken, Injector} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Router} from "@angular/router";
import {EMPTY, Observable} from "rxjs";
import {catchError} from "rxjs/operators";
import {NotificationService} from "./notification.service";
import {notificationType} from "../model/notification.model";
import {LOGIN_URL} from "./auth.service";
import {ErrorHandler} from "../types/error-handler.types";

export const ERROR_HANDLERS = new InjectionToken<(error: any) => boolean>('errorHandlers');

@Injectable()
export class ErrorsInterceptor implements HttpInterceptor {

  constructor(
    private notification: NotificationService,
    private router: Router,
    private injector: Injector,
    @Inject(ERROR_HANDLERS) private errorHandlers: ErrorHandler[]) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    request = request.clone({
      withCredentials: true
    });
    return next.handle(request)
      .pipe(
        catchError((error: any) => {
          for (let handler of this.errorHandlers) {
            if (!handler)
              continue;
            let res = handler(error, this.injector);
            if (res) {
              if (res.message)
                this.notification.notify(res.type, res.message);
              return EMPTY;
            }
          }
          this.notification.notify(notificationType.danger, "Ошибка!");
          console.error(error);
          return Observable.throw(error);
        })
      );
  }
}
