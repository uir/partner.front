export const getTokenState = (state): any => state.subMainState.authState.tokenState;
export const getToken = (state): string => getTokenState(state) && getTokenState(state).item && getTokenState(state).item.access_token;
