import {Component, Input} from '@angular/core';
import {Observable} from "rxjs";
import {LoaderService} from "./loader.service";
import {map} from "rxjs/operators";

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent{

  @Input() allowAlways = false;
  @Input() diameter: number = 100;
  @Input() keyWords: string[];
  public isActive: Observable<boolean>;

  constructor(private loaderService: LoaderService) {
    this.isActive = loaderService.isActive().pipe(map(value => value.status));
  }

  ngOnInit(): void {
    this.loaderService.keyWords = this.keyWords;
  }

}
