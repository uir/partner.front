import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoaderService} from "./loader.service";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {LoaderInterceptor} from "./loader.interceptor";
import {LoaderComponent} from "./loader.component";

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [LoaderService,],
  declarations: [LoaderComponent],
  exports: [LoaderComponent]
})
export class LoaderModule {
  public static forRoot(): ModuleWithProviders[] {
    return [
      {
        ngModule: LoaderModule,
        providers: [
          {provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true},
        ]
      }
    ];
  }
}
