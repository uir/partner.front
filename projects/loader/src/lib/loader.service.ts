import {Injectable} from '@angular/core';
import {LoaderStatus} from "./loader.model";
import {BehaviorSubject, Observable, merge} from "rxjs";
import {filter, sampleTime, throttleTime} from "rxjs/operators";

@Injectable()
export class LoaderService {

  private _requestsStates: BehaviorSubject<LoaderStatus>;
  private _keyWords: string[];

  constructor() {
    this._requestsStates = new BehaviorSubject<LoaderStatus>(null);

  }

  addRequestStatus(status: LoaderStatus) {
    this._requestsStates.next(status);
  }

  isActive(): Observable<LoaderStatus> {
    let req = this._requestsStates
      .pipe(
        filter(value => value != null),
      );

    let obsTrue = req
      .pipe(
        filter(value => value.status),
        throttleTime(500)
      );

    let obsFalse = req
      .pipe(
        filter(value => !value.status),
        sampleTime(500)
      );

    return merge(obsTrue, obsFalse)
  }

  get keyWords(): string[] {
    return this._keyWords;
  }

  set keyWords(value: string[]) {
    this._keyWords = value;
  }
}
