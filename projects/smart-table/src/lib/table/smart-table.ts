import {SmartTableOptions} from "../models/table-options.model";
import {Column} from "../models/column.model";
import {OrganizationList} from "../../../../../src/app/pages/admin/models/organization-list.model";

export interface SmartTable {
  options: SmartTableOptions;
  columns: Column<OrganizationList>[];
}
