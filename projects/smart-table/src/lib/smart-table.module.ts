import {NgModule} from '@angular/core';
import {CrudTableComponent} from './crud-table/crud-table.component';
import {MatButtonModule, MatDialogModule, MatIconModule, MatInputModule, MatNativeDateModule} from "@angular/material";
import {NgxDatatableModule} from "@swimlane/ngx-datatable";
import {TableComponent} from './table/table.component';
import {SmartTableDialogComponent} from './table-dialog/smart-table-dialog.component';
import {SharedFormlyModule} from "../../../shared-formly/src/lib/uir-formly.module";
import {UirCoreModule} from "../../../uir-core/src/lib/uir-core.module";
import {CommonModule} from "@angular/common";
import {HttpClientModule} from "@angular/common/http";
import {RestService} from "./services/rest.service";
import {CreateAction} from "./services/default actions/create.action";
import {InfoAction} from "./services/default actions/info.action";
import {EditAction} from "./services/default actions/edit.action";
import {DeleteAction} from "./services/default actions/delete.action";

@NgModule({
  imports: [
    CommonModule,
    UirCoreModule,
    SharedFormlyModule.forRoot(),
    MatNativeDateModule,
    NgxDatatableModule,
    MatDialogModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    HttpClientModule
  ],
  declarations: [CrudTableComponent, TableComponent, SmartTableDialogComponent],
  exports: [CrudTableComponent, TableComponent, SmartTableDialogComponent],
  bootstrap: [SmartTableDialogComponent],
  providers: [
    CreateAction,
    DeleteAction,
    InfoAction,
    EditAction,
    RestService
  ]
})
export class SmartTableModule {
}
