import {RouterModule} from '@angular/router';
import {AppComponent} from './app.component';
import {AppRoutes} from './app.routing';
import {AuthGuard} from "app/shared/core/util/auth.guard";
import {NavigationModule} from './shared/core/modules/navigation/navigation.module';
import {LOCALE_ID, NgModule} from '@angular/core';
import {NotificationModule} from './shared/core/modules/notification/notification.module';
import {StoreModule} from '@ngrx/store';
import {reducer} from "app/app.reducer";
import {CommonModule, registerLocaleData} from "@angular/common";
import localeRu from '@angular/common/locales/ru';
import {LoaderModule} from "../../projects/loader/src/lib/loader.module";
import {CheckPaidModule} from "./pages/user/modules/check-paid/check-paid.module";
import {BrowserModule} from "@angular/platform-browser";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {UirCoreModule} from "../../projects/uir-core/src/lib/uir-core.module";

registerLocaleData(localeRu, 'ru');
export const uirPartnerStorageKey = 'uir-partner-auth-tokens';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    BrowserModule,
    StoreModule.forRoot(reducer),
    RouterModule.forRoot(AppRoutes),
    NotificationModule,
    LoaderModule.forRoot(),
    NavigationModule,
    CheckPaidModule,
    UirCoreModule.forRoot({storageKey: uirPartnerStorageKey})
  ],
  providers: [
    {provide: LOCALE_ID, useValue: 'ru'},
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
