import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";
import {select, Store} from "@ngrx/store";
import {map, skip, switchMap} from "rxjs/operators";
import {HttpService} from "../../../projects/uir-core/src/lib/services/http.service";

@Injectable({providedIn: "root"})
export class UserPaymentPlanService {

  private _allowContent: string = environment.api + "api/user/allowcontent";

  constructor(private http: HttpService, private store: Store<any>) {
  }

  public getImagesAllowCount(): Observable<number> {
    return this.store
      .pipe(
        select(state => state.mainState.filialsState.imagesState.imagesState.items),
        skip(1),
        map(x => x.length),
        switchMap(count => this.http.GET<any>(this._allowContent)
          .pipe(
            map(m => m.imagesMaxCount - count),
            map(value => value < 0 ? 0 : value)
          )
        )
      );
  }

  public getVideosAllowCount(): Observable<number> {
    return this.store
      .pipe(
        select(state => state.mainState.filialsState.videosState.items),
        skip(1),
        switchMap(value => this.http.GET<any>(this._allowContent)
          .pipe(
            map(m => m.videosMaxCount - value.length)
          )
        ),
        map(value => value < 0 ? 0 : value)
      );
  }

}

