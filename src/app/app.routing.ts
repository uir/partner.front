import {Routes} from '@angular/router';

const adminModule = "app/pages/admin/admin.module#AdminModule";
const userModule = "app/pages/user/user.module#UserModule";
const mainModule = "app/pages/main/main.module#MainModule";

export const AppRoutes: Routes = [
  {
    path: '',
    redirectTo: 'user/companies',
    pathMatch: 'full'
  },
  {
    path: 'user',
    loadChildren: userModule
  },
  {
    path: 'companies',
    loadChildren: mainModule
  },
  {
    path: 'admin',
    loadChildren: adminModule
  }
];
