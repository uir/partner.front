import {companiesListState} from "app/pages/user/modules/companies/list/companieslist.reducer";
import {createCompanyState} from "app/pages/user/modules/companies/create/create.company.reducer";
import {createReducerTree} from 'create-reducer-tree';
import {mainState} from './pages/main/admin-layout.reducer';
import {notifyState} from "app/shared/core/modules/notification/notification.reducer";
import {tokenState} from "app/pages/user/modules/auth/components/token/token.reducer";
import {loginState} from "./pages/user/modules/auth/components/login/login.reducer";

let reducerComposer = {
  mainState: mainState,
  subMainState: {
    companiesState: {
      createCompanyState: createCompanyState,
      companiesList: companiesListState
    },
    authState: {
      loginState: loginState,
      tokenState: tokenState
    }
  },
  notifyState: notifyState
};

const a = createReducerTree(reducerComposer);

export const reducer = {...a};
