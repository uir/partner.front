import {Component, OnInit} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {LoadAction} from "app/shared/core/ngrx/action/base.action";
import {Store} from '@ngrx/store';
import {tokenActionTypes} from './pages/user/modules/auth/components/token/token.reducer';
import {filter} from "rxjs/operators";

declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(private store: Store<any>, private router: Router) {
    this.store.dispatch(new LoadAction(tokenActionTypes.get));
  }

  ngOnInit() {
    this.router
      .events
      .pipe(
        filter(event => event instanceof NavigationEnd)
      )
      .subscribe(() => {
        const contentContainer = document.querySelector('.main-panel');
        if (contentContainer != null)
          contentContainer.scrollTop = 0;
      });
    let body = document.getElementsByTagName('body')[0];
    var isWindows = navigator.platform.indexOf('Win') > -1 ? true : false;
    if (isWindows) {
      // if we are on windows OS we activate the perfectScrollbar function
      body.classList.add("perfect-scrollbar-on");
    } else {
      body.classList.add("perfect-scrollbar-off");
    }
    $.material.init();
  }
}
