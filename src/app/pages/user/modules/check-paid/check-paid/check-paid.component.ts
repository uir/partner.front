import {Component} from '@angular/core';
import {FormComponent} from "../../../../../shared/core/form/form-component";
import {FormlyFieldConfig} from "@ngx-formly/core";
import {phoneField} from "../../../../../shared/core/form/formly/fromly.fields";
import {CheckPaidService} from "../check-paid.service";

declare var $: any;
declare var swal: any;

@Component({
  selector: 'app-check-paid',
  styleUrls: ['check-paid.component.css'],
  template: `
    <div class="check-paid-container">
      <div class="check-paid">
        <img src="assets/logo.svg" class="check-paid__img">
        <form [formGroup]="form" class="check-paid__form">
          <formly-form [model]="model" [fields]="fields" [options]="options" [form]="form">
            <button type="submit" class="btn btn-success" (click)="submit()" [disabled]="!form.valid">
              Проверить
            </button>
          </formly-form>
        </form>
      </div>
    </div>
  `
})
export class CheckPaidComponent extends FormComponent<{ phone: string }> {

  get fields(): FormlyFieldConfig[] {
    return [phoneField];
  }

  constructor(private checkPaidService: CheckPaidService) {
    super();
  }

  submit() {
    if (this.form.valid)
      this.checkPaidService.check(this.form.value).subscribe(x => {
        if (x)
          swal({
            title: 'Успешно',
            type: 'success',
            confirmButtonClass: 'btn btn-success'
          });
        else
          swal({
            title: 'Отклонено',
            type: 'error',
            confirmButtonClass: 'btn btn-success'
          });
      });
  }
}
