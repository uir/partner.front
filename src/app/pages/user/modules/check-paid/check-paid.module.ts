import {NgModule} from '@angular/core';
import {CheckPaidComponent} from './check-paid/check-paid.component';
import {SharedFormlyModule} from "../../../../../../projects/shared-formly/src/lib/uir-formly.module";

@NgModule({
  imports: [
    SharedFormlyModule.forRoot()
  ],
  declarations: [CheckPaidComponent],
  exports: [CheckPaidComponent]
})
export class CheckPaidModule {
}
