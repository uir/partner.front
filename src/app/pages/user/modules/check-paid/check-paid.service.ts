import {Injectable} from '@angular/core';
import {environment} from "../../../../../environments/environment";
import {Observable} from "rxjs";
import {HttpService} from "../../../../../../projects/uir-core/src/lib/services/http.service";

@Injectable({
  providedIn: 'root'
})
export class CheckPaidService {

  private checkpaid = environment.api + 'api/user/checkpaid/';

  constructor(private http: HttpService) {
  }

  check(data): Observable<boolean> {
    return this.http.GET(this.checkpaid + data.phone);
  }
}
