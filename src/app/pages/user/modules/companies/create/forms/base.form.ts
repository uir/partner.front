import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { BaseForm } from "app/shared/core/form/base.form";
import {BaseCreate} from '../create.company.model';
import { Injectable } from '@angular/core';

@Injectable()
export class BaseCreateForm extends BaseForm<BaseCreate>{
  public activeTypes = [
    {value: 1, name: 'Организация'},
    {value: 2, name: 'Индвидуальный предприниматель'},
    {value: 3, name: 'Самозанятость'}];

  protected getForm(): FormGroup {
    return this.fb.group({
      name: [this.data.name,
        [
          Validators.required,
          Validators.pattern("^.{1,40}$")
        ]],
      address: [this.data.address, [
        Validators.required,
        Validators.pattern("^.{6,100}$")
      ]],
      fio: [this.data.fio, [
        Validators.required,
        Validators.pattern("^.{6,50}$")
      ]],
      logo: [this.data.logo],
      activeType: [this.data.activeType],
      categoriesIds: [this.data.categoriesIds]
    });
  }

  protected getValidationMessages() {
    return {
      name: {required: this.required, pattern: "Поле не должно превышать 50 символов"},
      address: {required: this.required,  pattern: "Поле должно содержать от 6 до 50 символов"},
      fio: {required: this.required, pattern: "Поле должно содержать от 6 до 100 символов"},
      activeType: {required: this.required}
    };
  }
  protected getPlaceholders() {
    return {
      name: '',
      address: '',
      fio: '',
      activeType: ''
    };
  }
  protected getLables() {
    return {
      name: 'Название',
      address: 'Юр.адрес',
      fio: 'ФИО',
      activeType: ''
    };
  }
  protected getIcons() {
    return {
      name: 'spellcheck',
      address: 'pin_drop',
      fio: 'perm_contact_calendar',
      activeType: 'work'
    };
  }

  setType(e){
    this.setFormData({...this.getValue(), activeType: e.value})
  }

  onCheck(checkedCategories) {
    this.setFormData({...this.getValue(), categoriesIds: checkedCategories})
  }

  getType(){
    return this.getValueData('activeType')
  }

  getTypeName(){
    return this.getValueData('activeType') == -1 ? 'Тип деятельности' :
      this.activeTypes.filter(x => x.value == this.getType())[0].name
  }

  getAddressLabel(){
    if (this.getType() == 3) return 'Адрес'
    else return 'Юр.адрес'
  }

  disabled(companyForm, ipForm, selfEmployedForm) {
    if (!this.isValid()) return false
    switch (this.getType()) {
      case -1: {
        return false;
      }
      case 1: {
        return companyForm.isValid()
      }
      case 2: {
        return ipForm.isValid()
      }
      case 3: {
        return selfEmployedForm.isValid()
      }
    }
  }
  constructor(fb: FormBuilder) {
    super({
      logo: null,
      name: '',
      address: '',
      fio: '',
      activeType: -1,
      categoriesIds: []
    }, fb);
    this.validationMessages = this.getValidationMessages();
    this.nonVisibleKeys = ['checkedCategories'];
    this.form = this.getForm();
    this.placeholders = this.getPlaceholders();
    this.lables = this.getLables();
    this.icons = this.getIcons();
  }
}
