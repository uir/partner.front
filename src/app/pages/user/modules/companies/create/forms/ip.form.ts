import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { BaseForm } from "app/shared/core/form/base.form";
import {IpCreate} from '../create.company.model';
import { Injectable } from '@angular/core';

@Injectable()
export class IpCreateForm extends BaseForm<IpCreate>{
  protected getForm(): FormGroup {
    return this.fb.group({
      inn: [this.data.inn, [
        Validators.required,
        Validators.pattern("^\\d{12}$"),
      ]],
      ogrn: [this.data.ogrn, [
        Validators.required,
      ]],
    });
  }
  protected getValidationMessages() {
    return {
      inn: {required: this.required, pattern: "Значение должно содержать 12 цифр"},
      ogrn: {required: this.required},
    };
  }
  protected getPlaceholders() {
    return {
      inn: '',
      ogrn: '',
    };
  }
  protected getLables() {
    return {
      inn: 'ИНН',
      ogrn: 'ОГРН',
    };
  }
  protected getIcons() {
    return {
      inn: 'description',
      ogrn: 'description',
    };
  }
  constructor(fb: FormBuilder) {
    super({
      inn: '',
      ogrn: '',
    }, fb);
    this.validationMessages = this.getValidationMessages();
    this.form = this.getForm();
    this.placeholders = this.getPlaceholders();
    this.lables = this.getLables();
    this.icons = this.getIcons();
  }
}
