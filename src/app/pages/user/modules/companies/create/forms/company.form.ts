import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { BaseForm } from "app/shared/core/form/base.form";
import { CompanyCreate } from '../create.company.model';
import { Injectable } from '@angular/core';

@Injectable()
export class CompanyCreateForm extends BaseForm<CompanyCreate>{
    protected getForm(): FormGroup {
        return this.fb.group({
            inn: [this.data.inn, [
              Validators.required,
              Validators.pattern("^\\d{10}$"),
            ]],
            kpp: [this.data.kpp, [
              Validators.required,
              Validators.pattern("^\\d{9}$"),
            ]],
            ogrn: [this.data.ogrn, [
              Validators.required
            ]],
        });
    }

    protected getValidationMessages() {
        return {
            inn: {required: this.required, pattern: "Значение должно содержать 10 цифр"},
            kpp: {required: this.required, pattern: "Значение должно содержать 9 цифр"},
            ogrn: {required: this.required},
        };
    }
    protected getPlaceholders() {
        return {
            inn: '',
            kpp: '',
            ogrn: '',
        };
    }
    protected getLables() {
        return {
            inn: 'ИНН',
            kpp: 'КПП',
            ogrn: 'ОГРН',
        };
    }
    protected getIcons() {
        return {
            inn: 'description',
            kpp: 'description',
            ogrn: 'description',
        };
    }
    constructor(fb: FormBuilder) {
        super({
          inn: '',
          kpp: '',
          ogrn: '',
        }, fb);
        this.validationMessages = this.getValidationMessages();
        this.form = this.getForm();
        this.placeholders = this.getPlaceholders();
        this.lables = this.getLables();
        this.icons = this.getIcons();
    }
}
