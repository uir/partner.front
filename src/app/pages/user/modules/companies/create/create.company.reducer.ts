import {TemplateCompanyCreate} from './create.company.model';
import {
  RequestGetActions,
  RequestListState,
  RequestObjectState,
  RequestSubmitActions
} from "app/shared/core/ngrx/reducer/responce.reducer";

import {StateBase} from "app/shared/core/ngrx/reducer/base.state";

export interface CreateCompanyState{
    categoriesState: StateBase;
    companyState: StateBase;
}

export const createCompanyCategoriesActionTypes: RequestGetActions = new RequestGetActions('COMPANY_CREATE_CATEGORIES');
export const createCompanyActionTypes: RequestSubmitActions = new RequestSubmitActions('COMPANY_CREATE');

export const createCompanyState: CreateCompanyState = {
    categoriesState: {
        actions: createCompanyCategoriesActionTypes.getActionsArr(),
        initialState: RequestListState.getInitialLoad<TemplateCompanyCreate>(),
        reducer: (s, a): any => {
            return RequestListState.getReducer(s, a, createCompanyCategoriesActionTypes)
        }
    },
    companyState: {
        actions: createCompanyActionTypes.getActionsArr(),
        initialState: RequestObjectState.getInitialSave(),
        reducer: (s, a): any => {
            return RequestObjectState.getReducer(s, a, createCompanyActionTypes)
        }
    }
}

export const getCompanyCreateCategories = (state): TemplateCompanyCreate[] => state.subMainState.companiesState.createCompanyState.categoriesState.items
