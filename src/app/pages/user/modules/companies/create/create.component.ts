import {AfterViewInit, Component, OnChanges, OnInit} from '@angular/core';
import {createCompanyActionTypes,} from './create.company.reducer';
import {CompanyCreateForm} from "app/pages/user/modules/companies/create/forms/company.form";
import {LoadAction} from "app/shared/core/ngrx/action/base.action";
import {Store} from '@ngrx/store';
import {BaseCreateForm} from "./forms/base.form";
import {IpCreateForm} from "./forms/ip.form";
import {SelfEmployedCreateForm} from "./forms/selfEployed.form";

declare var $: any;

interface FileReaderEventTarget extends EventTarget {
  result: string
}
interface FileReaderEvent extends Event {
  target: FileReaderEventTarget;
  getMessage(): string;
}
@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateCompanyComponent implements OnInit, OnChanges, AfterViewInit {

  constructor(public baseForm: BaseCreateForm,
              public companyForm: CompanyCreateForm,
              public ipForm: IpCreateForm,
              public selfEmployedForm: SelfEmployedCreateForm,
              private store: Store<any>) {

  }

  create() {
    if (this.baseForm.disabled(this.companyForm, this.ipForm, this.selfEmployedForm)) {
      let base = this.baseForm.getValue();
      let info = null;
      switch (base.activeType) {
        case 1: {
          info = this.companyForm.getValue();
          break
        }
        case 2: {
          info = this.ipForm.getValue();
          break
        }
        case 3: {
          info = this.selfEmployedForm.getValue();
          break
        }
      }
      this.store.dispatch(new LoadAction(createCompanyActionTypes.submit, {...base, ...info}));
    }
  }

  checkFullPageBackgroundImage() {
    let $page = $('.full-page');
    let image_src = $page.data('image');

    if (image_src !== undefined) {
      let image_container = '<div class="full-page-background" style="background-image: url(' + image_src + ') "/>'
      $page.append(image_container);
    }
  };

  readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e: any) {
        $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
      }
      reader.readAsDataURL(input.files[0]);
    }
  }

  ngOnInit() {
    let me = this;
    this.baseForm.buildForm();
    this.companyForm.buildForm();
    this.ipForm.buildForm();
    this.selfEmployedForm.buildForm();
    this.checkFullPageBackgroundImage();

    // Wizard Initialization
    $('.wizard-card').bootstrapWizard({
      'tabClass': 'nav nav-pills',
      'nextSelector': '.btn-next',
      'previousSelector': '.btn-previous',
      onNext: function (tab, navigation, index) {
        if (!me.baseForm.isValid()) {
          return false;
        }
        if (index == 2 && me.baseForm.getValueData('categoriesIds').length == 0) {
          return false;
        }
      },
      onInit: function (tab, navigation, index) {
        var $total = navigation.find('li').length;
        var $width = 100 / $total;
        var $wizard = navigation.closest('.wizard-card');

        var $display_width = $(document).width();

        if ($display_width < 600 && $total > 3) {
          $width = 50;
        }

        navigation.find('li').css('width', $width + '%');
        var $first_li = navigation.find('li:first-child a').html();
        var $moving_div = $('<div class="moving-tab">' + $first_li + '</div>');
        $('.wizard-card .wizard-navigation').append($moving_div);

        //    this.refreshAnimation($wizard, index);
        var total_steps = $wizard.find('li').length;
        var move_distance = $wizard.width() / total_steps;
        var step_width = move_distance;
        move_distance *= index;

        var $current = index + 1;

        if ($current == 1) {
          move_distance -= 8;
        } else if ($current == total_steps) {
          move_distance += 8;
        }

        $wizard.find('.moving-tab').css('width', step_width);
        $('.moving-tab').css({
          'transform': 'translate3d(' + move_distance + 'px, 0, 0)',
          'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'

        });

        $('.moving-tab').css('transition', 'transform 0s');
      },
      onTabClick: function (tab, navigation, index) {
        if (!me.baseForm.isValid()) {
          return false;
        }
        if (index == 1 && me.baseForm.getValueData('categoriesIds').length == 0) {
          return false;
        }
      },
      onTabShow: function (tab, navigation, index) {
        var $total = navigation.find('li').length;
        var $current = index + 1;

        var $wizard = navigation.closest('.wizard-card');

        // If it's the last tab then hide the last submitButtonText and show the finish instead
        if ($current >= $total) {
          $($wizard).find('.btn-next').hide();
          $($wizard).find('.btn-finish').show();
        } else {
          $($wizard).find('.btn-next').show();
          $($wizard).find('.btn-finish').hide();
        }

        var button_text = navigation.find('li:nth-child(' + $current + ') a').html();

        setTimeout(function () {
          $('.moving-tab').text(button_text);
        }, 150);

        var checkbox = $('.footer-checkbox');

        if (index !== 0) {
          $(checkbox).css({
            'opacity': '0',
            'visibility': 'hidden',
            'position': 'absolute'
          });
        } else {
          $(checkbox).css({
            'opacity': '1',
            'visibility': 'visible'
          });
        }

        // this.refreshAnimation($wizard, index);
        var total_steps = $wizard.find('li').length;
        var move_distance = $wizard.width() / total_steps;
        var step_width = move_distance;
        move_distance *= index;

        var $current = index + 1;

        if ($current == 1) {
          move_distance -= 8;
        } else if ($current == total_steps) {
          move_distance += 8;
        }

        $wizard.find('.moving-tab').css('width', step_width);
        $('.moving-tab').css({
          'transform': 'translate3d(' + move_distance + 'px, 0, 0)',
          'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'

        });
      }
    });


    // Prepare the preview for profile picture
    $("#wizard-picture").change(function () {

      this.readURL(this);
    });

    $('[data-toggle="wizard-radio"]').click(function () {
      console.log('click');

      var wizard = $(this).closest('.wizard-card');
      wizard.find('[data-toggle="wizard-radio"]').removeClass('active');
      $(this).addClass('active');
      $(wizard).find('[type="radio"]').removeAttr('checked');
      $(this).find('[type="radio"]').attr('checked', 'true');
    });

    $('[data-toggle="wizard-checkbox"]').click(function () {
      if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        $(this).find('[type="checkbox"]').removeAttr('checked');
      } else {
        $(this).addClass('active');
        $(this).find('[type="checkbox"]').attr('checked', 'true');
      }
    });

    $('.set-full-height').css('height', 'auto');
  }

  ngOnChanges() {
    var input = $(this);
    var target: EventTarget;
    if (input.files && input.files[0]) {
      var reader: any = new FileReader();

      reader.onload = function (e) {
        $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
      }
      reader.readAsDataURL(input.files[0]);
    }
  }

  ngAfterViewInit() {
    $('.wizard-card').each(function () {

      var $wizard = $(this);
      var index = $wizard.bootstrapWizard('currentIndex');
      // this.refreshAnimation($wizard, index);

      var total_steps = $wizard.find('li').length;
      var move_distance = $wizard.width() / total_steps;
      var step_width = move_distance;
      move_distance *= index;

      var $current = index + 1;

      if ($current == 1) {
        move_distance -= 8;
      } else if ($current == total_steps) {
        move_distance += 8;
      }

      $wizard.find('.moving-tab').css('width', step_width);
      $('.moving-tab').css({
        'transform': 'translate3d(' + move_distance + 'px, 0, 0)',
        'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'

      });

      $('.moving-tab').css({
        'transition': 'transform 0s'
      });
    });
  }
}
