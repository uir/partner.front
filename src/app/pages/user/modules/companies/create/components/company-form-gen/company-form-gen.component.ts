import {Component, Input, OnInit} from '@angular/core';
import {BaseForm} from "../../../../../../../shared/core/form/base.form";

@Component({
  selector: 'app-company-form-gen',
  templateUrl: './company-form-gen.component.html',
  styleUrls: ['./company-form-gen.component.css']
})
export class CompanyFormGenComponent implements OnInit {

  @Input() from: BaseForm<any>;
  @Input() keys: string[];

  constructor() {
  }

  public getKeys(): string[] {
    if (this.keys != null && this.keys.length > 0)
      return this.keys;
    else
      return this.from.getKeys();
  }

  ngOnInit() {
  }

}
