import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Suggestion} from "../../dadata.model";

declare var $: any;

@Component({
  selector: 'app-company-auto-input',
  templateUrl: './company-auto-input.component.html',
  styleUrls: ['./company-auto-input.component.css']
})
export class CompanyAutoInputComponent implements OnInit {

  @Input() isError: boolean;
  @Input() icon: string;
  @Input() label: string;
  @Input() error: string;
  @Input() item;
  @Input() fromGroup;
  @Output() select: EventEmitter<Suggestion> = new EventEmitter<Suggestion>();

  constructor() {
  }

  ngOnInit() {
    $("#party").suggestions({
      token: "e1c7ad541aba4a1fc2721be83b26179d022ef4b4",
      type: "PARTY",
      count: 5,
      onSelect: (suggestion: Suggestion) => {
        this.select.emit(suggestion);
      }
    });
  }
}
