import { Component, OnInit } from '@angular/core';
import {BaseCreateForm} from "../../../forms/base.form";
import {LoadAction} from "../../../../../../../../shared/core/ngrx/action/base.action";
import {createCompanyCategoriesActionTypes, getCompanyCreateCategories} from "../../../create.company.reducer";
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";
import {TemplateCompanyCreate} from "../../../create.company.model";

@Component({
  selector: 'app-category-tab',
  templateUrl: './category-tab.component.html',
  styleUrls: ['./category-tab.component.css']
})
export class CategoryTabComponent implements OnInit {

  public categories: Observable<TemplateCompanyCreate[]>;

  constructor(public baseForm: BaseCreateForm, private store: Store<any>) {
    store.dispatch(new LoadAction(createCompanyCategoriesActionTypes.get));
    this.categories = store.select(getCompanyCreateCategories);
  }

  ngOnInit() {
  }

}
