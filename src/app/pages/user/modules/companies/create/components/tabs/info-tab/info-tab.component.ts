import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {BaseCreateForm} from "../../../forms/base.form";
import {CompanyCreateForm} from "../../../forms/company.form";
import {IpCreateForm} from "../../../forms/ip.form";
import {SelfEmployedCreateForm} from "../../../forms/selfEployed.form";
import {Suggestion} from "../../../dadata.model";
import {BaseCreate, CompanyCreate, IpCreate} from "../../../create.company.model";

@Component({
  selector: 'app-info-tab',
  templateUrl: './info-tab.component.html',
  styleUrls: ['./info-tab.component.css']
})
export class InfoTabComponent implements OnInit {

  @ViewChild("orgTemp") orgTemp: TemplateRef<any>;
  @ViewChild("ipTemp") ipTemp: TemplateRef<any>;
  @ViewChild("personTemp") personTemp: TemplateRef<any>;

  constructor(public baseForm: BaseCreateForm,
              public companyForm: CompanyCreateForm,
              public ipForm: IpCreateForm,
              public selfEmployedForm: SelfEmployedCreateForm) {
  }

  ngOnInit() {
  }

  public getTemplate() {
    switch (this.baseForm.getType()) {
      case 1:
        return this.orgTemp;
      case 2:
        return this.ipTemp;
      case 3:
        return this.personTemp;
    }
  }

  public onSelect(suggestion: Suggestion) {
    let data = <BaseCreate>{
      fio: suggestion.data.type == 'INDIVIDUAL' ? suggestion.data.name.full : suggestion.data.management.name,
      address: suggestion.data.address.value,
      name: suggestion.value,
      activeType: suggestion.data.inn.length === 10 ? 1 : 2
    };
    this.baseForm.setFormData(data);
    if (data.activeType === 1)
      this.companyForm.setFormData(<CompanyCreate>{
        inn: suggestion.data.inn,
        kpp: suggestion.data.kpp,
        ogrn: suggestion.data.ogrn
      }, {
        emitModelToViewChange: true,
        emitViewToModelChange: true
      });
    if (data.activeType === 2)
      this.ipForm.setFormData(<IpCreate>{
        inn: suggestion.data.inn,
        ogrn: suggestion.data.ogrn
      }, {
        emitModelToViewChange: true,
        emitViewToModelChange: true
      });

    let elems = Array.from(document.getElementsByClassName('is-empty'));
    for (let i = 0; i <= elems.length; i++) {
      elems[i].classList.remove('is-empty')
    }
  }

}
