import {Component, OnInit} from '@angular/core';
import {FileUploader} from "ng2-file-upload";
import {environment} from "../../../../../../../../environments/environment";
import {getToken} from "../../../../auth/components/token/token.reducer";
import {select, Store} from "@ngrx/store";
import {filter} from "rxjs/operators";
import {AuthService} from "../../../../../../../../../projects/uir-core/src/lib/services/auth.service";

@Component({
  selector: 'app-company-logo-uploader',
  templateUrl: './company-logo-uploader.component.html',
  styleUrls: ['./company-logo-uploader.component.css']
})
export class CompanyLogoUploaderComponent implements OnInit {

  public uploader: FileUploader;

  constructor(private store: Store<any>, private auth: AuthService) {
    this.uploader = new FileUploader(
      {
        isHTML5: true,
        allowedMimeType: ['image/png', 'image/jpg', 'image/jpeg'],
      }
    );

    this.store
      .pipe(
        select(state => state.subMainState.companiesState.createCompanyState.companyState.item),
        filter(a => a)
      )
      .subscribe(a => {
        this.uploader.queue[0].url = environment.api + "api/companies/" + a + "/logo";
        this.uploader.uploadAll()
      });

    this.uploader.options.headers = [
      {
        name: 'Authorization',
        value: 'bearer ' + this.auth.accessToken
      }
    ]
  }

  ngOnInit() {
  }

}
