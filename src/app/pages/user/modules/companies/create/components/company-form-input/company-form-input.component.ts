import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-company-form-input',
  templateUrl: './company-form-input.component.html',
  styleUrls: ['./company-form-input.component.css']
})
export class CompanyFormInputComponent implements OnInit {

  @Input() isError: boolean;
  @Input() icon: string;
  @Input() label: string;
  @Input() error: string;
  @Input() item;
  @Input() fromGroup;

  constructor() { }

  ngOnInit() {
  }

}
