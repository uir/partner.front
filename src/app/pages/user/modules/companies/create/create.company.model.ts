export class BaseCreate {
  name: string;
  fio: string;
  address: string;
  categoriesIds?: number[];
  activeType: number;
  logo?: string
}

export class IpCreate {
  inn: string;
  ogrn: string;
}

export class CompanyCreate {
  kpp: string;
  inn: string;
  ogrn: string;
}

export class SelfEmployedCreate {
  numberPassport: string;
  seriesPassport: string;
  residenceAddress: string;
  birthday: string;
  passportDepartment: string;
  datePassport: string;
}

export class TemplateCompanyCreate {
  id: number;
  name: string;
  categories: {
    id: number;
    name: string;
  }
}
