import {ActionWithPayload, ErrorAction, getActionTypeLoad, SuccessAction} from 'app/shared/core/ngrx/action/base.action';
import {createCompanyActionTypes, createCompanyCategoriesActionTypes} from './create/create.company.reducer';
import {getRequestEffect, getRequestEffectWithData} from "app/shared/core/ngrx/effect/responce.effects";
import {Actions, Effect, ofType} from '@ngrx/effects';
import {CompaniesService} from './companies.service';
import {Injectable} from "@angular/core";
import {Router} from '@angular/router';
import {companiesListActionsTypes} from './list/companieslist.reducer';
import {catchError, mergeMap, switchMap, tap} from "rxjs/operators";
import {of} from "rxjs";

@Injectable()
export class CompaniesEffects {
  @Effect()
  getCompanies$ = getRequestEffectWithData(
    this.actions$,
    companiesListActionsTypes.get,
    this.companiesService.getCompanies);


  @Effect()
  companyCategories$ = getRequestEffect(
    this.actions$,
    createCompanyCategoriesActionTypes.get,
    this.companiesService.getCategories);

  @Effect()
  companyCreate$ = this.actions$
    .pipe(
      ofType<ActionWithPayload>(getActionTypeLoad(createCompanyActionTypes.submit)),
      switchMap(a => this.companiesService.addCompany(a.payload)
        .pipe(
          mergeMap(data =>
            of(new SuccessAction(createCompanyActionTypes.submit, data))
              .pipe(
                tap((this.router.navigate(['user/companies']) as any)))
          ),
          catchError(() => of(new ErrorAction(createCompanyActionTypes.submit, 'ошибка')))
        )
      )
    );

  constructor(private actions$: Actions<ActionWithPayload>, private companiesService: CompaniesService, private router: Router) {
  }
}
