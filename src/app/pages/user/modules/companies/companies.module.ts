import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {CompaniesComponent} from './list/companies.component';
import {CompaniesEffects} from './companies.effects';
import {CompanyComponent} from "app/shared/core/components/company/company.component";
import {CompanyCreateForm} from './create/forms/company.form';
import {EffectsModule} from "@ngrx/effects";
import {NgModule} from "@angular/core";
import {RouterModule} from '@angular/router';
import {CategoriesCheckerModule} from "../../../../shared/categories-checker/categories-checker.module";
import {BaseCreateForm} from "./create/forms/base.form";
import {IpCreateForm} from "./create/forms/ip.form";
import {SelfEmployedCreateForm} from "./create/forms/selfEployed.form";
import {CreateCompanyComponent} from "./create/create.component";
import {Ng2DropdownModule} from "ng2-material-dropdown";
import {FileUploadModule} from "ng2-file-upload";
import {HttpClientModule} from "@angular/common/http";
import {CompanyLogoUploaderComponent} from './create/components/company-logo-uploader/company-logo-uploader.component';
import {InfoTabComponent} from './create/components/tabs/info-tab/info-tab.component';
import {CategoryTabComponent} from './create/components/tabs/category-tab/category-tab.component';
import {PayTabComponent} from './create/components/tabs/pay-tab/pay-tab.component';
import {CompanyFormGenComponent} from './create/components/company-form-gen/company-form-gen.component';
import {CompanyFormInputComponent} from './create/components/company-form-input/company-form-input.component';
import {CompanyAutoInputComponent} from './create/components/company-auto-input/company-auto-input.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FileUploadModule,
    Ng2DropdownModule,
    ReactiveFormsModule,
    EffectsModule.forFeature([CompaniesEffects]),
    CategoriesCheckerModule,
    FileUploadModule,
    RouterModule,
    HttpClientModule
  ],
  declarations: [
    CompaniesComponent,
    CompanyComponent,
    CreateCompanyComponent,
    CompanyLogoUploaderComponent,
    InfoTabComponent,
    CategoryTabComponent,
    PayTabComponent,
    CompanyFormGenComponent,
    CompanyFormInputComponent,
    CompanyAutoInputComponent
  ],
  providers: [
    CompaniesEffects,
    BaseCreateForm,
    IpCreateForm,
    SelfEmployedCreateForm,
    CompanyCreateForm
  ]
})

export class CompaniesModule {
}
