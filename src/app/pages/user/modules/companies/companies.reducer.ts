import { CreateCompanyState } from './create/create.company.reducer';
import { StateBase } from "app/shared/core/ngrx/reducer/base.state";

export interface CompaniesState {
    createCompanyState: CreateCompanyState;
    companiesList: StateBase;
}
