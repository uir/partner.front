import { RequestGetActions, RequestListState } from "app/shared/core/ngrx/reducer/responce.reducer";

import { CompanyList } from './companieslist.model';
import { StateBase } from "app/shared/core/ngrx/reducer/base.state";

export const companiesListActionsTypes = new RequestGetActions('COMPANY_LIST');
export const companiesListState: StateBase = {
    actions: companiesListActionsTypes.getActionsArr(),
    initialState: RequestListState.getInitialLoad<CompanyList>(),
    reducer: (s, a): any => {
        return RequestListState.getReducer(s, a, companiesListActionsTypes)
    }
} 
