import {Component, OnInit} from '@angular/core';
import {CompanyList} from './companieslist.model';
import {LoadAction} from "app/shared/core/ngrx/action/base.action";
import {select, Store} from '@ngrx/store';
import {companiesListActionsTypes} from './companieslist.reducer';
import {environment} from "../../../../../../environments/environment";
import {Observable} from "rxjs";
import {loginActionTypes} from "../../auth/components/login/login.reducer";

declare var $: any;

@Component({
  selector: 'companies-cmp',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.css']
})

export class CompaniesComponent implements OnInit {

  public companies: Observable<CompanyList[]>;

  constructor(private store: Store<any>) {
    this.store.dispatch(new LoadAction(companiesListActionsTypes.get));
    this.companies = this.store.pipe(select(state => {
      let arr = state.subMainState.companiesState.companiesList.items;
      if (arr != null)
        return arr;
    }));
  }

  checkFullPageBackgroundImage() {
    let $page = $('.full-page');
    let image_src = $page.data('image');

    if (image_src !== undefined) {
      let image_container = '<div class="full-page-background" style="background-image: url(' + image_src + ') "/>';
      $page.append(image_container);
    }
  };

  logout() {
    this.store.dispatch(new LoadAction(loginActionTypes.delete))
  }

  ngOnInit() {
    this.checkFullPageBackgroundImage();
  }

  toMain() {
    location.href = environment.mainSite;
  }
}
