export class CompanyList {
  id: number;
  name: string;
  address: string;
  logo: string;
}
