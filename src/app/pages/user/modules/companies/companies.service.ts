import {CompanyCreate, TemplateCompanyCreate} from './create/create.company.model';
import {environment} from "environments/environment";
import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {HttpService} from "../../../../../../projects/uir-core/src/lib/services/http.service";

@Injectable({providedIn: "root"})
export class CompaniesService {
    private shortcategories = environment.api + 'api/category/all/short';
    private company = environment.api + 'api/companies';

  constructor(private http: HttpService) {
  }

    getCategories = (): Observable<TemplateCompanyCreate> => {
        return this.http.GET(this.shortcategories);
    }

    addCompany = (company: CompanyCreate): Observable<any> => {
        return  this.http.POST(this.company, company)
    }

    getCompanies = (): Observable<any> => {
        return  this.http.GET(this.company);
    }
}
