import {Component} from '@angular/core';
import {FormComponent} from "../../../../../../shared/core/form/form-component";
import {CreateUserByRef} from "../../../../../../shared/user/model/create-user-by-ref.model";
import {FormlyFieldConfig} from "@ngx-formly/core";
import {CreateUserService} from "../../../../../../shared/user/create-user.service";
import {ActivatedRoute, Router} from "@angular/router";
import {registerByRefFields} from "../../forms/register-by-ref.form";
import {OrganizationService} from "../../../../../../shared/organization/services/organization.service";

@Component({
  selector: 'app-register-by-ref',
  templateUrl: './register-by-ref.component.html',
  styleUrls: ['./register-by-ref.component.css']
})
export class RegisterByRefComponent extends FormComponent<CreateUserByRef> {

  private referral: string;

  constructor(
    private orgService: OrganizationService,
    private createUserService: CreateUserService,
    private router: Router,
    private route: ActivatedRoute) {
    super();
    this.referral = this.route.snapshot.paramMap.get('ref');
    this.orgService.getInfoByRef(this.referral).subscribe(x => {
      this.model = {
        ...this.model,
        ...{
          orgName: x.name,
          address: x.address
        }
      };
    });
  }

  get fields(): FormlyFieldConfig[] {
    return registerByRefFields;
  }

  submit() {
    let data = {...this.form.value, referral: this.referral};
    this.createUserService
      .registerByRef(data)
      .subscribe(() => this.router.navigate(['user/register/' + this.referral + '/confirm']));
  }
}
