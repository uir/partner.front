import { RequestActions, RequestObjectState } from 'app/shared/core/ngrx/reducer/responce.reducer';

import { StateBase } from "app/shared/core/ngrx/reducer/base.state";
import {Info} from "../../auth.model";

export const tokenActionTypes: RequestActions = new RequestActions('USER_TOKEN');
export const tokenState: StateBase = {
    actions: tokenActionTypes.getActionsArr(),
    initialState: RequestObjectState.getInitial(),
    reducer: (s, a): any => {
        return RequestObjectState.getReducer(s, a, tokenActionTypes)
    }
};

export const getTokenState = (state): any => state.subMainState.authState.tokenState;

export const isLogin = (state): boolean => getTokenState(state).item != null;
export const getToken = (state): string => getTokenState(state) && getTokenState(state).item && getTokenState(state).item.access_token;
export const getUserEmail = (state): string => getTokenState(state) && getTokenState(state).item && getTokenState(state).item.email;
export const getTokenItem = (state): Info => getTokenState(state) && getTokenState(state).item;
