import {FormComponent} from "../../../../../../shared/core/form/form-component";
import {Component} from "@angular/core";
import {emailFiled} from "../../../../../../shared/core/form/formly/fromly.fields";
import {Router} from "@angular/router";
import {ChangePasswordService} from "../../services/change-password.service";
import {FormlyFieldConfig} from "@ngx-formly/core";

@Component({
  selector: 'app-restore-password',
  templateUrl: 'restore-password.component.html'
})
export class RestorePasswordComponent extends FormComponent<{ email: string }> {

  get fields(): FormlyFieldConfig[] {
    return [emailFiled];
  }

  constructor(private router: Router, private changePasswordService: ChangePasswordService) {
    super();
  }

  submit() {
    this.changePasswordService
      .change(this.form.value)
      .subscribe(() => this.router.navigate(['user', 'restorepassword', 'confirm']));
  }
}
