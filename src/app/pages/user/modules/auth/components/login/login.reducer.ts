import { RequestActions, RequestObjectState } from "app/shared/core/ngrx/reducer/responce.reducer";

import { StateBase } from "app/shared/core/ngrx/reducer/base.state";

export const loginActionTypes: RequestActions = new RequestActions('USER_LOGIN');
export const loginState: StateBase = {
    actions: loginActionTypes.getActionsArr(),
    initialState: RequestObjectState.getInitial(),
    reducer: (s, a): any => {
        return RequestObjectState.getReducer(s, a, loginActionTypes)
    }
}

export const getLoginState = (state): any => state.subMainState.loginState;

export const isAuthLoading = (state): boolean => getLoginState(state).itemLoad.isLoading;
export const isAuthSuccess = (state): boolean => getLoginState(state).itemLoad.isSuccess;
export const getAuthErrorMessage = (state): string => getLoginState(state).itemLoad.message;
export const getRoles = (state): string[] => getLoginState(state).item
    && getLoginState(state).item.roles;
