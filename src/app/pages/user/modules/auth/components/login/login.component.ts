import {Component, OnInit} from '@angular/core';
import {LoginForm} from '../../forms/login.form';
import {Router} from "@angular/router";
import {AuthService} from "../../../../../../../../projects/uir-core/src/lib/services/auth.service";

declare var $: any;

@Component({
  selector: 'login-cmp',
  templateUrl: './login.component.html',
  styles: [`
    .restore-link {
      display: flex;
      flex-direction: row-reverse;
    }
  `]
})

export class LoginComponent implements OnInit {

  test: Date = new Date();

  constructor(public form: LoginForm, private auth: AuthService, private router: Router) {
  }

  login() {
    if (this.form.isValid())
      this.auth
        .login({
          username: this.form.getValue().login,
          password: this.form.getValue().password
        })
        .subscribe(() => this.router.navigate(['user/companies']));
  }

  checkFullPageBackgroundImage() {
    let $page = $('.full-page');
    let image_src = $page.data('image');

    if (image_src !== undefined) {
      let image_container = '<div class="full-page-background" style="background-image: url(' + image_src + ') "/>'
      $page.append(image_container);
    }
  };

  ngOnInit() {
    this.form.buildForm();
    this.checkFullPageBackgroundImage();

    setTimeout(function () {
      $('.card').removeClass('card-hidden');
    }, 700)
  }
}
