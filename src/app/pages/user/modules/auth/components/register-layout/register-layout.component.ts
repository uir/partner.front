import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";

declare var $: any;

@Component({
  selector: 'app-register-layout',
  templateUrl: './register-layout.component.html',
  styleUrls: ['./register-layout.component.css']
})
export class RegisterLayoutComponent implements OnInit {

  public title: string;

  constructor(private route: ActivatedRoute) {
    route.data.subscribe(x => {
      this.title = x.title;
    })
  }

  ngOnInit() {
    this.checkFullPageBackgroundImage();
  }

  checkFullPageBackgroundImage() {
    let $page = $('.full-page');
    let image_src = $page.data('image');

    if (image_src !== undefined) {
      let image_container = '<div class="full-page-background" style="background-image: url(' + image_src + ') "/>'
      $page.append(image_container);
    }
  }
}
