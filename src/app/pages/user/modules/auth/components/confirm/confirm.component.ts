import {Component} from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {Observable} from "rxjs";
import {FormComponent} from "../../../../../../shared/core/form/form-component";
import {FormlyFieldConfig} from "@ngx-formly/core";
import {AuthService} from "../../../../../../../../projects/uir-core/src/lib/services/auth.service";

export interface Confirm {
  email: string;
  data: any;
  submitFunc: (data) => Observable<any>;
  fields: FormlyFieldConfig[]
  navigateAfterLogin: string
}

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.css']
})
export class ConfirmComponent extends FormComponent<{ code: string }> {
  public email: string;
  public data: Confirm;

  get fields(): FormlyFieldConfig[] {
    return this.data.fields;
  }

  get onConfirm(): (formData) => void {
    return (data) => {
      this.auth
        .login({username: data.email, password: data.password})
        .subscribe(() => this.roter.navigate([this.data.navigateAfterLogin]));
    }
  }

  constructor(
    private roter: Router,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private auth: AuthService) {
    super();
  }

  confirm() {
    if (this.form.valid) {
      let data = {...this.form.value, ...this.data.data};
      this.data.submitFunc(data).subscribe(() => this.onConfirm(data));
    }
  }

  ngOnInit(): void {
    this.route.data.subscribe(data => {
      this.data = data.confirm;
      this.email = this.data.email;
    });
  }
}
