import {Token} from "./models/token.model";

export class User {
  id: number;
  email: string;
  name: string;
  roles: string;
}

export class Info implements User, Token {
  access_token: string;
  email: string;
  expires_in: number;
  id: number;
  name: string;
  roles: string;
  token_type: string;

}
