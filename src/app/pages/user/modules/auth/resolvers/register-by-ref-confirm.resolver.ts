import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Confirm} from "../components/confirm/confirm.component";
import {CreateUserService} from "../../../../../shared/user/create-user.service";
import {codeField} from "../../../../../shared/core/form/formly/fromly.fields";
import {environment} from "../../../../../../environments/environment";
import {Observable} from "rxjs";
import {HttpService} from "../../../../../../../projects/uir-core/src/lib/services/http.service";

@Injectable({providedIn: "root"})
export class RegisterByRefConfirmResolver implements Resolve<Confirm> {

  private _userRegConfirm = environment.api + 'api/auth/reg/partner/$ref/confirm';

  constructor(private createUserService: CreateUserService, private http: HttpService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Confirm {
    let data = this.createUserService.regByRefData().getValue();
    return {
      data: data,
      email: data && data.email,
      submitFunc: (x): Observable<any> => {
        let url = this._userRegConfirm.replace('$ref', data.referral);
        return this.http.POST<number>(url, {code: x.code, password: x.password});
      },
      fields: [codeField],
      navigateAfterLogin: '/user/companies'
    }
  }
}
