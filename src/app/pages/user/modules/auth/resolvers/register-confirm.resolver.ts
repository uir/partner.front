import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Confirm} from "../components/confirm/confirm.component";
import {CreateUserService} from "../../../../../shared/user/create-user.service";
import {codeField} from "../../../../../shared/core/form/formly/fromly.fields";
import {environment} from "../../../../../../environments/environment";
import {Observable} from "rxjs";
import {HttpService} from "../../../../../../../projects/uir-core/src/lib/services/http.service";

@Injectable({providedIn: "root"})
export class RegisterConfirmResolver implements Resolve<Confirm> {

  private _userRegConfirm = environment.api + 'api/auth/confirm/partner';

  constructor(private createUserService: CreateUserService, private http: HttpService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Confirm {
    let data = this.createUserService.getRegData().getValue();
    return {
      data: data,
      email: data && data.email,
      submitFunc: (data): Observable<any> => {
        return this.http.POST<number>(this._userRegConfirm, data);
      },
      fields: [codeField],
      navigateAfterLogin: '/user/companies'
    }
  }
}
