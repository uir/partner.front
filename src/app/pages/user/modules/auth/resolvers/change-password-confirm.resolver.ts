import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Confirm} from "../components/confirm/confirm.component";
import {ChangePasswordService} from "../services/change-password.service";
import {select, Store} from "@ngrx/store";
import {getUserEmail} from "../components/token/token.reducer";
import {merge, Observable} from "rxjs";
import {filter, first, map, switchMap} from "rxjs/operators";
import {codeField, newPasswordField} from "../../../../../shared/core/form/formly/fromly.fields";

@Injectable({providedIn: "root"})
export class ChangePasswordConfirmResolver implements Resolve<Observable<Confirm>> {

  private fields = [newPasswordField, codeField];

  constructor(private changePasswordService: ChangePasswordService, private store: Store<any>) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Confirm> {
    const changeRequest$ = this.store
        .pipe(
          select(getUserEmail),
          filter(x => x != null),
          switchMap(x => this.changePasswordService.change({email: x}), x => x),
          map(x => {
            let data = {email: x};
            return {
              data: data,
              email: data.email,
              submitFunc: this.changePasswordService.changePasswordConfirm,
              fields: this.fields,
              navigateAfterLogin: '/user/companies'
            }
          }),
          filter(x => x.email != null)
        );

    const restoreRequest$ = this.changePasswordService.changePasswordData
      .pipe(
        filter(x => x != null),
        map(x => {
          return {
            data: x,
            email: x && x.email,
            submitFunc: this.changePasswordService.changePasswordConfirm,
            fields: this.fields,
            navigateAfterLogin: '/user/companies'
          }
        }),
        filter(x => x.email != null)
      );

    return merge(changeRequest$, restoreRequest$).pipe(first())
  }
}
