import {AuthEffects} from './auth.effects';
import {CookieService} from 'angular2-cookie/core';
import {EffectsModule} from "@ngrx/effects";
import {NgModule} from '@angular/core';
import {RegForm} from './forms/reg.form';
import {ConfirmComponent} from './components/confirm/confirm.component';
import {RouterModule} from "@angular/router";
import {RegisterLayoutComponent} from './components/register-layout/register-layout.component';
import {LoginComponent} from "./components/login/login.component";
import {LoginForm} from "./forms/login.form";
import {RegisterConfirmResolver} from "./resolvers/register-confirm.resolver";
import {RestorePasswordComponent} from './components/restore-password/restore-password.component';
import {UserModule} from "../../../../shared/user/user.module";
import {RegisterByRefComponent} from './components/refister-by-ref/register-by-ref.component';
import {RegisterByRefConfirmResolver} from "./resolvers/register-by-ref-confirm.resolver";
import {SharedFormlyModule} from "../../../../../../projects/shared-formly/src/lib/uir-formly.module";

@NgModule({
  imports: [
    SharedFormlyModule.forRoot(),
    UserModule,
    RouterModule,
    EffectsModule.forFeature([AuthEffects]),
  ],
  declarations: [
    LoginComponent,
    ConfirmComponent,
    RegisterLayoutComponent,
    RestorePasswordComponent,
    RegisterByRefComponent
  ],
  providers: [
    RegisterConfirmResolver,
    RegisterByRefConfirmResolver,
    AuthEffects,
    LoginForm,
    RegForm,
    CookieService
  ]
})

export class AuthModule {
}
