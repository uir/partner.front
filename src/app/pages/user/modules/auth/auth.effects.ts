import {Actions, Effect, ofType} from '@ngrx/effects';
import {ErrorAction, getActionTypeLoad, SuccessAction} from 'app/shared/core/ngrx/action/base.action';
import {CookieService} from 'angular2-cookie/core';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {ActionWithPayload, getActionTypeSuccess} from '../../../../shared/core/ngrx/action/base.action';
import {tokenActionTypes} from './components/token/token.reducer';
import {catchError, concat, filter, map, mergeMap, switchMap, tap} from "rxjs/operators";
import {Observable, of} from "rxjs";
import {Token} from "./models/token.model";
import {Info, User} from "./auth.model";
import {loginActionTypes} from "./components/login/login.reducer";
import {AuthData} from "./models/login.model";
import {AuthService} from "../../../../../../projects/uir-core/src/lib/services/auth.service";
import {HttpHeaders} from "@angular/common/http";
import {HttpService} from "../../../../../../projects/uir-core/src/lib/services/http.service";
import {environment} from "../../../../../environments/environment";

@Injectable()
export class AuthEffects {

  @Effect()
  userTokenLoad$ = this.actions$
    .pipe(
      ofType(getActionTypeLoad(tokenActionTypes.get)),
      switchMap(() => of(this.cookieService.getObject('token_partner'))),
      filter(token => token != null),
      switchMap(token => of(new SuccessAction(tokenActionTypes.get, token)))
    );

  @Effect({dispatch: false})
  tokenSuccess$ = this.actions$
    .pipe(
      ofType<ActionWithPayload>(getActionTypeSuccess(tokenActionTypes.get)),
      map(action => action.payload),
      switchMap(token => of(this.cookieService.putObject('token_partner', token)))
    );

  @Effect({dispatch: false})
  userLogined$ = this.actions$
    .pipe(
      ofType(getActionTypeSuccess(loginActionTypes.get)),
      switchMap(() => of(this.router.navigate(['user/companies'])))
    );

  @Effect()
  userLogin$ = this.actions$
    .pipe(
      ofType<ActionWithPayload>(getActionTypeLoad(loginActionTypes.get)),
      map(action => action.payload),
      switchMap((_data: AuthData) =>
        this.authService.login({
          password: _data.password,
          username: _data.login
        })
          .pipe(
            switchMap((value: Token) => this.getInfo()),
            mergeMap((info: Info) => {
              if (info.roles.includes('Partner')) {
                return of(new SuccessAction(loginActionTypes.get, info))
                  .pipe(
                    concat(of(new SuccessAction(tokenActionTypes.get, info))),
                    catchError(() => of(new ErrorAction(loginActionTypes.get, 'Неверный логин или пароль')))
                  );
              } else {
                return of(new ErrorAction(loginActionTypes.get, 'Ошибка доступа'));
              }
            }),
            catchError(err => of(new ErrorAction(loginActionTypes.get, err)))
          )
      )
    );

  @Effect({dispatch: false})
  userLogout$ = this.actions$
    .pipe(
      ofType(getActionTypeLoad(loginActionTypes.delete)),
      tap(() => {
        this.cookieService.remove('token_partner');
        this.router.navigate(['user/login']);
      })
    );

  getInfo(): Observable<User> {
    return this.http.GET<User>(environment.api + 'api/user/info');
  }

  constructor(private router: Router,
              private http: HttpService,
              private authService: AuthService,
              private actions$: Actions<ActionWithPayload>,
              private cookieService: CookieService) {
  }
}
