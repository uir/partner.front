import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {environment} from "../../../../../../environments/environment";
import {ChangePassword} from "../models/change-password.model";
import {tap} from "rxjs/operators";
import {HttpService} from "../../../../../../../projects/uir-core/src/lib/services/http.service";

@Injectable({
  providedIn: 'root'
})
export class ChangePasswordService {

  private changepassword = environment.api + 'api/auth/changepassword';
  private changepasswordConfirm= environment.api + 'api/auth/changepassword/confirm';
  public changePasswordData = new BehaviorSubject<{email: string}>(null);

  constructor(private http: HttpService) { }

  change(data){
    return this.http.POST(this.changepassword, data).pipe(tap(() => this.changePasswordData.next(data)));
  }

  changePasswordConfirm = (data: ChangePassword): Observable<any> => {
    return this.http.POST(this.changepasswordConfirm, data);
  }
}
