import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthData } from '../models/login.model';
import { BaseForm } from "app/shared/core/form/base.form";
import { Injectable } from '@angular/core';

@Injectable()
export class LoginForm extends BaseForm<AuthData>{
    protected getForm(): FormGroup {
        return this.fb.group({
            login: [
                this.data.login,
                [
                    Validators.required
                ]
            ],
            password: [
                this.data.password,
                [
                    Validators.required
                ]
            ]
        });
    }
    protected getValidationMessages() {
        return {
            login: {
                required: this.required
            },
            password: {
                required: this.required
            }
        };
    }
    protected getPlaceholders() {
        return {
            login: 'Логин',
            password: 'Пароль'
        };
    }
    protected getLables() {
        return {
            login: 'Логин',
            password: 'Пароль'
        };
    }
    constructor(fb: FormBuilder) {
        super({
          login: '',
          password: ''
        }, fb);
        this.validationMessages = this.getValidationMessages();
        this.form = this.getForm();
        this.placeholders = this.getPlaceholders();
        this.lables = this.getLables();
    }
}
