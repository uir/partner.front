import {emailFiled, passwordField, phoneField} from "../../../../../shared/core/form/formly/fromly.fields";
import {FormlyFieldConfig} from "@ngx-formly/core";

export const registerByRefFields: FormlyFieldConfig[] = [
  {
    key: 'orgName',
    type: 'input',
    templateOptions: {
      disabled: true,
      label: 'Организация',
      required: true,
    }
  },
  {
    key: 'address',
    type: 'input',
    templateOptions: {
      disabled: true,
      label: 'Адрес',
      required: true,
    }
  },
  {
    key: 'name',
    type: 'input',
    templateOptions: {
      label: 'ФИО',
      required: true,
    }
  },
  emailFiled,
  phoneField,
  passwordField
];
