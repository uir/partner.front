import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { BaseForm } from "app/shared/core/form/base.form";
import { Injectable } from '@angular/core';
import { CreateUser } from '../../../../../shared/user/model/create-user.model';

@Injectable()
export class RegForm extends BaseForm<CreateUser>{
    protected getForm(): FormGroup {
        return this.fb.group({
            email: [
                this.data.email,
                [
                    Validators.required,
                    Validators.pattern("^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$")
                ]
            ],
            phone: [
                this.data.phone,
                [
                    Validators.required,
                    Validators.pattern("^\\+7[\\d]{10}$")
                ]
            ],
            name: [
                this.data.name,
                [
                    Validators.required,
                    Validators.pattern("^.{1,50}$")
                ]
            ],
            password: [
                this.data.password,
                [
                    Validators.required,
                    Validators.pattern("^.{6,40}$")
                ]
            ]
        });
    }
    protected getValidationMessages() {
        return {
            email: {required: this.required, pattern: "Пример поля: uir@yandex.ru"},
            phone: {required: this.required, pattern: "Формат поля: +7XXXXXXXXXX"},
            name: {required: this.required, pattern: 'Имя должно быть не больше 50 символов'},
            password: {required: this.required, pattern: 'Пароль должен от 6 до 40 символов'}
        };
    }
    protected getPlaceholders() {
        return {
            email: 'Email',
            phone: 'Телефон',
            name: 'ФИО',
            password: 'Пароль'
        };
    }
    protected getLables() {
        return {
            email: 'E-mail',
            phone: 'Телефон',
            name: 'ФИО',
            password: 'Пароль'
        };
    }
    constructor(fb: FormBuilder) {
        super({
          email: '',
          phone: '',
          name: '',
          password: ''
        }, fb);
        this.validationMessages = this.getValidationMessages();
        this.form = this.getForm();
        this.placeholders = this.getPlaceholders();
        this.lables = this.getLables();
        this.icons = this.getIcons();
    }
}
