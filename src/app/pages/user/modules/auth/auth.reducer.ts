import { StateBase } from "app/shared/core/ngrx/reducer/base.state";

export interface AuthState {
    loginState: StateBase;
    regState: StateBase;
    tokenState: StateBase;
}
