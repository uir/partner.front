import {Routes} from "@angular/router";
import {LoginComponent} from "./modules/auth/components/login/login.component";
import {RegisterLayoutComponent} from "./modules/auth/components/register-layout/register-layout.component";
import {CreateUserComponent} from "../../shared/user/create-user/create-user.component";
import {ConfirmComponent} from "./modules/auth/components/confirm/confirm.component";
import {RegisterConfirmResolver} from "./modules/auth/resolvers/register-confirm.resolver";
import {ChangePasswordConfirmResolver} from "./modules/auth/resolvers/change-password-confirm.resolver";
import {RestorePasswordComponent} from "./modules/auth/components/restore-password/restore-password.component";
import {CompaniesComponent} from "./modules/companies/list/companies.component";
import {CreateCompanyComponent} from "./modules/companies/create/create.component";
import {EmptyLayoutComponent} from "./components/empt-layout.component";
import {RegisterByRefComponent} from "./modules/auth/components/refister-by-ref/register-by-ref.component";
import {RegisterByRefConfirmResolver} from "./modules/auth/resolvers/register-by-ref-confirm.resolver";
import {CheckPaidComponent} from "./modules/check-paid/check-paid/check-paid.component";

export const UserRoutes: Routes = [
  {
    path: '',
    component: EmptyLayoutComponent,
    children: [
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'register',
        component: RegisterLayoutComponent,
        data: {
          title: "Регистрация"
        },
        children: [
          {
            path: '',
            component: CreateUserComponent,
          },
          {
            path: 'confirm',
            component: ConfirmComponent,
            resolve: {
              confirm: RegisterConfirmResolver
            }
          }
        ]
      },
      {
        path: 'register/:ref',
        component: RegisterLayoutComponent,
        data: {
          title: "Регистрация организации"
        },
        children: [
          {
            path: '',
            component: RegisterByRefComponent,
          },
          {
            path: 'confirm',
            component: ConfirmComponent,
            resolve: {
              confirm: RegisterByRefConfirmResolver
            }
          }
        ]
      },
      {
        path: 'changepassword',
        component: RegisterLayoutComponent,
        data: {
          title: "Изменение пароля"
        },
        children: [
          {
            path: 'confirm',
            component: ConfirmComponent,
            resolve: {
              confirm: ChangePasswordConfirmResolver
            }
          }
        ]
      },
      {
        path: 'restorepassword',
        component: RegisterLayoutComponent,
        data: {
          title: "Востановление пароля"
        },
        children: [
          {
            path: '',
            component: RestorePasswordComponent
          },
          {
            path: 'confirm',
            component: ConfirmComponent,
            resolve: {
              confirm: ChangePasswordConfirmResolver
            }
          }
        ]
      },
      {
        path: 'companies',
        children: [{
          path: '',
          component: CompaniesComponent
        }, {
          path: 'add',
          component: CreateCompanyComponent
        }]
      }
    ]
  },
  {
    path: 'check',
    component: CheckPaidComponent
  },
];
