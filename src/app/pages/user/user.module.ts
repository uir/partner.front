import {NgModule} from '@angular/core';
import {RouterModule} from "@angular/router";
import {UserRoutes} from "./user.routing";
import {AuthModule} from "./modules/auth/auth.module";
import {CompaniesModule} from "./modules/companies/companies.module";
import {EmptyLayoutComponent} from "./components/empt-layout.component";
import {SharedFormlyModule} from "../../../../projects/shared-formly/src/lib/uir-formly.module";
import {CheckPaidModule} from "./modules/check-paid/check-paid.module";

@NgModule({
  imports: [
    SharedFormlyModule.forRoot(),
    AuthModule,
    CompaniesModule,
    CheckPaidModule,
    RouterModule.forChild(UserRoutes),
  ],
  declarations: [EmptyLayoutComponent],
})
export class UserModule {
}
