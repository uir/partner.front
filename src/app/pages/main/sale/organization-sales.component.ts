import {Component} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {map} from "rxjs/operators";

@Component({
  selector: 'app-organization-sales',
  template: `
    <div class="main-content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div>
              <div class="card-header card-header-text" data-background-color="rose">
                <h4 class="card-title">{{title}}</h4>
              </div>
              <div>
                <app-sales *ngIf="companyId" [owner]="2" [ownerId]="companyId"></app-sales>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  `
})
export class OrganizationSalesComponent {

  public title = "Скидки организации";
  public companyId: number;

  constructor(private router: ActivatedRoute) {
    this.router.parent.params
      .pipe(map(params => +params['id']))
      .subscribe(id => {
        if (id != null)
          this.companyId = id;
      });
  }
}


