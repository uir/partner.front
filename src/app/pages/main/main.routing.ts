import {Routes} from "@angular/router";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {InfoComponent} from "./info/info.component";
import {UserComponent} from "./user/user.component";
import {TagsComponent} from "./tags/tags.component";
import {OrganizationSalesComponent} from "./sale/organization-sales.component";
import {FilialCreateComponent, FilialCreateComponentType} from "./filials/create/filial-create.component";
import {FilialDashboardComponent} from "./filials/dashboard/filial-dashboard.component";
import {FilialEditComponent} from "./filials/edit/filial-edit.component";
import {FilialImagesComponent} from "./filials/images/filial-images.component";
import {FilialVideosComponent} from "./filials/videos/filial-videos.component";
import {VideoEditComponent} from "./filials/videos/edit/video-edit.component";
import {FilialSalesComponent} from "./filials/sales/filial-sales.component";
import {AdminLayoutComponent} from "./admin-layout.component";

export const MainRoutes: Routes = [
  {
    path: ':id',
    component: AdminLayoutComponent,
    children: [
      {
        path: '',
        component: DashboardComponent
      },
      {
        path: 'info',
        component: InfoComponent
      },
      {
        path: 'user',
        component: UserComponent
      },
      {
        path: 'tags',
        component: TagsComponent
      },
      {
        path: 'sales',
        component: OrganizationSalesComponent
      },
      // {
      //   path: 'stat',
      //   component: StatisticsComponent
      // },
      {
        path: 'filials/add',
        component: FilialCreateComponent,
        data: {
          title: 'Добавление филиала',
          action: 'Создать',
          type: FilialCreateComponentType.Create
        }
      },
      {
        path: 'filials/:id/copy',
        component: FilialCreateComponent,
        data: {
          title: 'Копирование филиала',
          action: 'Скопировать',
          type: FilialCreateComponentType.Copy
        }
      },
      {
        path: 'filials/:id',
        component: FilialDashboardComponent
      },
      {
        path: 'filials/:id/edit',
        component: FilialEditComponent
      },
      {
        path: 'filials/:id/images',
        component: FilialImagesComponent
      },
      {
        path: 'filials/:id/videos',
        component: FilialVideosComponent,
      },
      {
        path: 'filials/:filialId/videos/:id',
        component: VideoEditComponent,
      },
      {
        path: 'filials/:filialId/sales',
        component: FilialSalesComponent,
      }
    ]
  }
];
