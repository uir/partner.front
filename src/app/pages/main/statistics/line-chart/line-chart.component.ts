// import { Component, OnInit } from '@angular/core';
// import { Color, BaseChartDirective, Label } from 'ng2-charts';
//
// @Component({
//   selector: 'app-line-chart',
//   templateUrl: './line-chart.component.html',
//   styleUrls: ['./line-chart.component.css']
// })
// export class LineChartComponent implements OnInit {
//   public lineChartData: ChartDataSets[] = [
//     { data: [65, 59, 80, 81, 56, 55, 40], label: 'Новые посетители' },
//     { data: [28, 48, 40, 19, 86, 27, 90], label: 'Повторные заходы' },
//     { data: [180, 480, 770, 90, 1000, 270, 400], label: 'Роботы', yAxisID: 'y-axis-1' }
//   ];
//   public lineChartLabels: Label[] = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль'];
//   public lineChartOptions: (ChartOptions & { annotation: any }) = {
//     responsive: true,
//     scales: {
//       // We use this empty structure as a placeholder for dynamic theming.
//       xAxes: [{}],
//       yAxes: [
//         {
//           id: 'y-axis-0',
//           position: 'left',
//         },
//         {
//           id: 'y-axis-1',
//           position: 'right',
//           gridLines: {
//             color: 'rgba(255,0,0,0.3)',
//           },
//           ticks: {
//             fontColor: 'red',
//           }
//         }
//       ]
//     },
//     annotation: {
//       annotations: [
//         {
//           type: 'line',
//           mode: 'vertical',
//           scaleID: 'x-axis-0',
//           value: 'March',
//           borderColor: 'orange',
//           borderWidth: 2,
//           label: {
//             enabled: true,
//             fontColor: 'orange',
//             content: 'LineAnno'
//           }
//         },
//       ],
//     },
//   };
//   public lineChartColors: Color[] = [
//     { // grey
//       backgroundColor: 'rgba(148,159,177,0.2)',
//       borderColor: 'rgba(148,159,177,1)',
//       pointBackgroundColor: 'rgba(148,159,177,1)',
//       pointBorderColor: '#fff',
//       pointHoverBackgroundColor: '#fff',
//       pointHoverBorderColor: 'rgba(148,159,177,0.8)'
//     },
//     { // dark grey
//       backgroundColor: 'rgba(77,83,96,0.2)',
//       borderColor: 'rgba(77,83,96,1)',
//       pointBackgroundColor: 'rgba(77,83,96,1)',
//       pointBorderColor: '#fff',
//       pointHoverBackgroundColor: '#fff',
//       pointHoverBorderColor: 'rgba(77,83,96,1)'
//     },
//     { // red
//       backgroundColor: 'rgba(255,0,0,0.3)',
//       borderColor: 'red',
//       pointBackgroundColor: 'rgba(148,159,177,1)',
//       pointBorderColor: '#fff',
//       pointHoverBackgroundColor: '#fff',
//       pointHoverBorderColor: 'rgba(148,159,177,0.8)'
//     }
//   ];
//   public lineChartLegend = true;
//   public lineChartType = 'line';
//
//   constructor() { }
//
//   ngOnInit() {
//   }
//   public randomize(): void {
//     const lineChartData: ChartDataSets[] = new Array(this.lineChartData.length);
//     for (let i = 0; i < this.lineChartData.length; i++) {
//       lineChartData[i] = { data: new Array(this.lineChartData[i].data.length), label: this.lineChartData[i].label };
//       for (let j = 0; j < this.lineChartData[i].data.length; j++) {
//         lineChartData[i].data[j] = Math.floor((Math.random() * (i < 2 ? 100 : 1000)) + 1);
//       }
//     }
//     this.lineChartData = lineChartData;
//   }
//
//   // events
//   public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
//     console.log(event, active);
//   }
//
//   public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
//     console.log(event, active);
//   }
// }
