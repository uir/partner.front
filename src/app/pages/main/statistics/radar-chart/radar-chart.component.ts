// import { Component, OnInit } from '@angular/core';
// import { ChartDataSets, ChartType, ChartOptions } from 'chart.js';
// import { Label } from 'ng2-charts';
//
// @Component({
//   selector: 'radar-chart',
//   templateUrl: './radar-chart.component.html',
//   styleUrls: ['./radar-chart.component.css']
// })
// export class RadarChartComponent implements OnInit {
//   // Radar
//   public radarChartOptions: ChartOptions = {
//     responsive: true,
//   };
//   public radarChartLabels: Label[] = ['Мобильные устройства', 'Ipad', 'Iphone', 'Samsung', 'iMac', 'Windows', 'Linux'];
//
//   public radarChartData: ChartDataSets[] = [
//     { data: [65, 59, 90, 81, 56, 55, 40], label: 'Мужчины' },
//     { data: [28, 48, 40, 19, 96, 27, 100], label: 'Женщины' }
//   ];
//   public radarChartType: ChartType = 'radar';
//   constructor() { }
//
//   ngOnInit() {
//   }
//   // events
//   public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
//     console.log(event, active);
//   }
//
//   public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
//     console.log(event, active);
//   }
// }
