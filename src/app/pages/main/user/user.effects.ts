import { Actions } from '@ngrx/effects';
import { Injectable } from "@angular/core";
import { UserService } from './user.service';

@Injectable()
export class UserEffects {

    // @Effect()
    // tags$ = getRequestEffectWithData(this.actions$, tagsListActionTypes.get, this.userService.tags)

    constructor(private actions$: Actions, private userService: UserService) {
    }
}
