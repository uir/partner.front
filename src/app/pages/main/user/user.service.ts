import {Injectable} from '@angular/core';
import {environment} from "environments/environment";

@Injectable({providedIn: "root"})
export class UserService {

  constructor() {
  }

  private tagsSearch = environment.api + 'api/tag/tags';
}
