import {EffectsModule} from '@ngrx/effects';
import {NgModule} from '@angular/core';
import {UserComponent} from './user.component';
import {UserEffects} from './user.effects';
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  imports: [
    EffectsModule.forRoot([UserEffects]), HttpClientModule
  ],
  exports: [],
  declarations: [UserComponent]
})
export class UserModule {
}
