import { Component, OnInit } from '@angular/core';

import { RequestEditActions } from '../../../shared/core/ngrx/reducer/responce.reducer';
import { StateBase } from '../../../shared/core/ngrx/reducer/base.state';

export const userEditActionTypes = new RequestEditActions("USER_EDIT");
export const userEditState = StateBase.createForEdit(userEditActionTypes);

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
