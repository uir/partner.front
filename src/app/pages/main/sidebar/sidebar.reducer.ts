import { filialsListlState } from './filials-list/filials-list.component';
import { userInfoState } from './user/user.component';

export const sidebarState = {
    filalsListState: filialsListlState,
    userInfoState: userInfoState
}