import {FilialList} from './filials-list/filials-list.component';
import {Injectable} from '@angular/core';
import {SidebarUser} from './user/user.model';
import {environment} from "environments/environment";
import {Observable} from "rxjs";
import {HttpService} from "../../../../../projects/uir-core/src/lib/services/http.service";

@Injectable({providedIn: "root"})
export class SidebarService {

  constructor(private http: HttpService) {
  }

  private userInfo = environment.api + "api/user/info";
  private filial = (id): string => {
    return environment.api + 'api/companies/' + id + '/places';
  }

  filialsList = (companyId): Observable<FilialList[]> => {
    let url = this.filial(companyId);
    return this.http.GET<FilialList[]>(url);
  }

  getUserInfo = (): Observable<SidebarUser> => {
    return this.http.GET<SidebarUser>(this.userInfo);
  }
}
