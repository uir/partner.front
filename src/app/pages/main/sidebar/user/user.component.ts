import {Component, OnInit} from '@angular/core';
import {LoadAction} from 'app/shared/core/ngrx/action/base.action';
import {RequestGetActions} from '../../../../shared/core/ngrx/reducer/responce.reducer';
import {SidebarUser} from './user.model';
import {StateBase} from '../../../../shared/core/ngrx/reducer/base.state';
import {select, Store} from '@ngrx/store';
import {getCompanyId} from 'app/pages/main/admin-layout.reducer';
import {Observable} from "rxjs";

export const userInfoActionType = new RequestGetActions("SIDEBAR_USER");
export const userInfoState = StateBase.createForObject(userInfoActionType);

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  public user$: Observable<SidebarUser>;
  public companyId: number;

  constructor(private store: Store<any>) {
    this.store.dispatch(new LoadAction(userInfoActionType.get));
    this.user$ = this.store.pipe(select(state => state.mainState.sidebarState.userInfoState.item));
    this.store.pipe(select(getCompanyId)).subscribe(id => this.companyId = id);
  }

  ngOnInit() {
  }

}
