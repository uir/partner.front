import {getActionTypeSuccess, LoadAction} from '../../../shared/core/ngrx/action/base.action';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Injectable} from "@angular/core";
import {SidebarService} from './sidebar.service';
import {select, Store} from '@ngrx/store';
import {createFilialActionTypes} from '../filials/create/filial-create.reducer';
import {filialsListActionTypes} from './filials-list/filials-list.component';
import {getCompanyId} from '../admin-layout.reducer';
import {getRequestEffectWithData} from "app/shared/core/ngrx/effect/responce.effects";
import {userInfoActionType} from './user/user.component';
import {switchMap, withLatestFrom} from "rxjs/operators";
import {of} from "rxjs";

@Injectable()
export class SidebarEffects {
  @Effect()
  filialsList$ = getRequestEffectWithData(this.actions$, filialsListActionTypes.get, this.sidebarService.filialsList);

  @Effect()
  userInfo$ = getRequestEffectWithData(this.actions$, userInfoActionType.get, this.sidebarService.getUserInfo);

  @Effect()
  filialsUpdate$ = this.actions$
    .pipe(
      ofType(getActionTypeSuccess(createFilialActionTypes.submit)),
      withLatestFrom(this.store.pipe(select(getCompanyId))),
      switchMap(arr => of(new LoadAction(filialsListActionTypes.get, arr[1])))
    );

  constructor(
    private store: Store<any>,
    private actions$: Actions,
    private sidebarService: SidebarService) {
  }
}
