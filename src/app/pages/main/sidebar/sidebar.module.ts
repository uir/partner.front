import {CommonModule} from '@angular/common';
import {EffectsModule} from "@ngrx/effects";
import {FilialsListComponent} from './filials-list/filials-list.component';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {SidebarComponent} from './sidebar.component';
import {SidebarEffects} from './sidebar.effects';
import {UserComponent} from './user/user.component';
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  imports: [
    CommonModule,
    EffectsModule.forFeature([SidebarEffects]),
    RouterModule,
    HttpClientModule
  ],
  declarations: [
    SidebarComponent,
    FilialsListComponent,
    UserComponent
  ],
  exports: [SidebarComponent],
  providers: [SidebarEffects]
})

export class SidebarModule {
}
