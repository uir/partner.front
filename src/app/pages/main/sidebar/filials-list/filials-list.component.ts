import {Component, OnInit} from '@angular/core';
import {RequestGetActions, RequestListState} from "app/shared/core/ngrx/reducer/responce.reducer";
import {LoadAction} from "app/shared/core/ngrx/action/base.action";
import {StateBase} from "app/shared/core/ngrx/reducer/base.state";
import {select, Store} from '@ngrx/store';
import {getCompanyId} from '../../admin-layout.reducer';
import {Observable} from "rxjs";
import {filter, first} from "rxjs/operators";

export const filialsListActionTypes = new RequestGetActions('FILIAL_LIST');
export const filialsListlState = StateBase.create(filialsListActionTypes, RequestListState.getInitialLoad, RequestListState.getReducer);

export class FilialList {
  id: number;
  name: string;
}

@Component({
  selector: '[app-filials-list]',
  templateUrl: './filials-list.component.html',
  styleUrls: ['./filials-list.component.css']
})
export class FilialsListComponent implements OnInit {

  public filials: Observable<FilialList[]>;
  public companyId: number;

  constructor(private store: Store<any>) {
    this.store
      .pipe(
        select(getCompanyId),
        first()
      )
      .subscribe(id => {
        this.companyId = id;
        this.store.dispatch(new LoadAction(filialsListActionTypes.get, id));
      });
    this.filials = this.store.pipe(select(state => state.mainState.sidebarState.filalsListState.items));

    this.store
      .pipe(
        select(state => state.mainState.dashboardState.dashboardFilialsState.filialDeleteState.itemDelete.isSuccess),
        filter(a => a)
      )
      .subscribe(() => {
        this.store.dispatch(new LoadAction(filialsListActionTypes.get, this.companyId))
      })
  }

  ngOnInit() {
  }

}
