import {Component, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {getCompanyId} from '../admin-layout.reducer';
import {Observable} from "rxjs";
import {first} from "rxjs/operators";

declare var $: any;
var sidebarTimer;

@Component({
  selector: 'sidebar-cmp',
  templateUrl: 'sidebar.component.html',
  styles: [`
    .sidebar-wrapper {
      height: auto !important;
    }

    .menu-title {
      color: white;
      padding-left: 28px;
      padding-top: 32px;
      font-size: 16px;
    }

    .logo-normal {
      display: flex;
      align-items: center;
      padding-left: 20px;
    }
  `]
})

export class SidebarComponent implements OnInit {

  public companyId: Observable<number>;

  constructor(private store: Store<any>) {
    this.companyId = this.store.pipe(select(getCompanyId), first());
  }

  isNotMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  }

  ngOnInit() {
    var isWindows = navigator.platform.indexOf('Win') > -1 ? true : false;
    if (isWindows) {
      // if we are on windows OS we activate the perfectScrollbar function
      var $sidebar = $('.sidebar-wrapper');
      $sidebar.perfectScrollbar();
    }
    isWindows = navigator.platform.indexOf('Win') > -1 ? true : false;

    if (isWindows) {
      // if we are on windows OS we activate the perfectScrollbar function
      $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();
      $('html').addClass('perfect-scrollbar-on');
    } else {
      $('html').addClass('perfect-scrollbar-off');
    }
  }
}
