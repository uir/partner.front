import {Injectable} from '@angular/core';
import {environment} from "environments/environment";
import {NavbarInfo} from "./navbar.model";
import {Observable} from "rxjs";
import {HttpService} from "../../../../../projects/uir-core/src/lib/services/http.service";

@Injectable({providedIn: "root"})
export class NavbarService {

  constructor(private http: HttpService) {
  }

  private getNavbarInfo = (id): string => {
    return environment.api + 'api/companies/' + id;
  }

  mainInfo = (companyId): Observable<NavbarInfo[]> => {
    let url = this.getNavbarInfo(companyId);
    return this.http.GET<NavbarInfo[]>(url);
  }
}
