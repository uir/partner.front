import {CommonModule} from '@angular/common';
import {EffectsModule} from "@ngrx/effects";
import {NavbarComponent} from './navbar.component';
import {NavbarEffects} from "./navbar.effects";
import {NgModule} from "@angular/core";
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  imports: [CommonModule, EffectsModule.forFeature([NavbarEffects]), HttpClientModule],
  declarations: [NavbarComponent],
  exports: [NavbarComponent],
  providers: [NavbarEffects],
})

export class NavbarModule {
}
