import {Actions, Effect} from '@ngrx/effects';
import {Injectable} from "@angular/core";
import {getRequestEffectWithData} from "app/shared/core/ngrx/effect/responce.effects";
import {NavbarService} from "./navbar.service";
import {navbarInfoActionTypes} from "./navbar.component";

@Injectable()
export class NavbarEffects {

  @Effect()
  navbarInfo$ = getRequestEffectWithData(this.actions$, navbarInfoActionTypes.get, this.navbarService.mainInfo)

  constructor(private actions$: Actions, private navbarService: NavbarService) {
  }
}
