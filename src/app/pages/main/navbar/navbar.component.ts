import {Component, ElementRef, OnInit, Renderer, ViewChild} from '@angular/core';
import {Location} from '@angular/common';
import {RequestGetActions} from "../../../shared/core/ngrx/reducer/responce.reducer";
import {StateBase} from "../../../shared/core/ngrx/reducer/base.state";
import {select, Store} from "@ngrx/store";
import {NavbarInfo} from "./navbar.model";
import {getCompanyId} from "../admin-layout.reducer";
import {LoadAction} from "../../../shared/core/ngrx/action/base.action";
import {environment} from "../../../../environments/environment";
import {filter, take} from "rxjs/operators";
import {loginActionTypes} from "../../user/modules/auth/components/login/login.reducer";

export const navbarInfoActionTypes = new RequestGetActions("MAIN_INFO");


export const navbarState = {
  mainInfoState: StateBase.createForObject(navbarInfoActionTypes),
}

var misc: any = {
  navbar_menu_visible: 0,
  active_collapse: true,
  disabled_collapse_init: 0,
}
declare var $: any;

@Component({
  selector: 'navbar-cmp',
  templateUrl: 'navbar.component.html',
  styles: [`
    .org-name {
      font-size: 25px;
    }

    .navbar {
      z-index: 1;
    }
  `]
})

export class NavbarComponent implements OnInit {
  private listTitles: any[];
  location: Location;
  private nativeElement: Node;
  private toggleButton;
  private sidebarVisible: boolean;
  public navbarInfo: NavbarInfo;
  private companyId: number;

  @ViewChild("navbar-cmp") button;

  constructor(location: Location, private renderer: Renderer, private element: ElementRef, private store: Store<any>) {
    this.location = location;
    this.nativeElement = element.nativeElement;
    this.sidebarVisible = false;
    this.store
      .pipe(
        select(state => state.mainState.navbarState.mainInfoState.item),
        filter(info => info)
      )
      .subscribe((info: NavbarInfo) => this.navbarInfo = info);

    this.store
      .pipe(select(getCompanyId), take(1))
      .subscribe(id => {
        if (id != null) {
          this.companyId = id;
          this.store.dispatch(new LoadAction(navbarInfoActionTypes.get, id));
        }
      });
  }

  ngOnInit() {

    var navbar: HTMLElement = this.element.nativeElement;
    this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
    if ($('body').hasClass('sidebar-mini')) {
      misc.sidebar_mini_active = true;
    }
    $('#minimizeSidebar').click(function () {
      var $btn = $(this);

      if (misc.sidebar_mini_active == true) {
        $('body').removeClass('sidebar-mini');
        misc.sidebar_mini_active = false;

      } else {
        setTimeout(function () {
          $('body').addClass('sidebar-mini');

          misc.sidebar_mini_active = true;
        }, 300);
      }

      // we simulate the window Resize so the charts will get updated in realtime.
      var simulateWindowResize = setInterval(function () {
        window.dispatchEvent(new Event('resize'));
      }, 180);

      // we stop the simulation of Window Resize after the animations are completed
      setTimeout(function () {
        clearInterval(simulateWindowResize);
      }, 1000);
    });
  }

  isMobileMenu() {
    if ($(window).width() < 991) {
      return false;
    }
    return true;
  }

  logout() {
    this.store.dispatch(new LoadAction(loginActionTypes.delete))
  }

  toMain() {
    location.href = environment.mainSite;
  }

  sidebarToggle() {
    var toggleButton = this.toggleButton;
    var body = document.getElementsByTagName('body')[0];

    if (this.sidebarVisible == false) {
      setTimeout(function () {
        toggleButton.classList.add('toggled');
      }, 500);
      body.classList.add('nav-open');
      this.sidebarVisible = true;
    } else {
      this.toggleButton.classList.remove('toggled');
      this.sidebarVisible = false;
      body.classList.remove('nav-open');
    }
  }
}
