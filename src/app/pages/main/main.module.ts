import {DashboardModule} from './dashboard/dashboard.module';
import {FilialsModule} from './filials/filials.module';
import {FooterModule} from './footer/footer.module';
import {InfoModule} from "./info/info.module";
import {NavbarModule} from './navbar/navbar.module';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {SidebarModule} from './sidebar/sidebar.module';
import {TagsModule} from "app/pages/main/tags/tags.module";
import {UserModule} from './user/user.module';
import {OrganizationSalesComponent} from "./sale/organization-sales.component";
import {SalesModule} from "../../shared/sales/sales.module";
import {AdminLayoutComponent} from "./admin-layout.component";
import {MainRoutes} from "./main.routing";
import {CommonModule} from "@angular/common";

@NgModule({
  imports: [
    CommonModule,
    FilialsModule,
    FooterModule,
    NavbarModule,
    SidebarModule,
    DashboardModule,
    TagsModule,
    UserModule,
    InfoModule,
    RouterModule,
    SalesModule,
    RouterModule.forChild(MainRoutes),
  ],
  exports: [AdminLayoutComponent],
  declarations: [
    OrganizationSalesComponent,
    AdminLayoutComponent,
  ],
  providers: [],
})
export class MainModule {
}
