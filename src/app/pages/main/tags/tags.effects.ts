import { tagCreateActionTypes, tagDeleteActionTypes, tagsListActionTypes, tagsSearchActionTypes } from './tags.component';
import { Actions } from '@ngrx/effects';
import { Effect } from '@ngrx/effects';
import { Injectable } from "@angular/core";
import { TagsService } from "app/pages/main/tags/tags.service";
import { getRequestEffectWithData } from "app/shared/core/ngrx/effect/responce.effects";

@Injectable()
export class TagsEffects {

    @Effect()
    tags$ = getRequestEffectWithData(this.actions$, tagsListActionTypes.get, this.tagsService.tags)

    @Effect()
    tagsSearch$ = getRequestEffectWithData(this.actions$, tagsSearchActionTypes.get, this.tagsService.search)

    @Effect()
    tagsBind$ = getRequestEffectWithData(this.actions$, tagsSearchActionTypes.get, this.tagsService.search)

    @Effect()
    tagBind$ = getRequestEffectWithData(this.actions$, tagCreateActionTypes.submit, this.tagsService.bind)

    @Effect()
    tagUntie$ = getRequestEffectWithData(this.actions$, tagDeleteActionTypes.delete, this.tagsService.untie)

    constructor(private actions$: Actions, private tagsService: TagsService) {
    }
}
