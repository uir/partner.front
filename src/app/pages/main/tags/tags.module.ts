import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {EffectsModule} from "@ngrx/effects";
import {NgModule} from '@angular/core';
import {TagsComponent} from "app/pages/main/tags/tags.component";
import {TagsEffects} from "app/pages/main/tags/tags.effects";
import {HttpClientModule} from "@angular/common/http";
import {TagInputModule} from "../../../shared/core/components/nfx-chips/modules";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TagInputModule,
    ReactiveFormsModule,
    EffectsModule.forFeature([TagsEffects]),
    HttpClientModule
  ],
  exports: [],
  declarations: [TagsComponent]
})
export class TagsModule {
}

