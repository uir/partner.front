import {Component} from '@angular/core';
import {RequestDeleteActions, RequestGetActions, RequestSubmitActions} from '../../../shared/core/ngrx/reducer/responce.reducer';
import {getCompanyId} from '../admin-layout.reducer';
import {LoadAction} from "app/shared/core/ngrx/action/base.action";
import {StateBase} from 'app/shared/core/ngrx/reducer/base.state';
import {select, Store} from '@ngrx/store';
import {Tag} from './tag.model';
import {filter} from "rxjs/operators";

export const tagsListActionTypes = new RequestGetActions("TAGS_LIST");
export const tagsSearchActionTypes = new RequestGetActions("TAGS_SEARCH", true);
export const tagCreateActionTypes = new RequestSubmitActions("TAGS_ADD");
export const tagDeleteActionTypes = new RequestDeleteActions("TAGS_DELETE");

export const tagsState = {
  tagsListState: StateBase.createForList(tagsListActionTypes),
  tagsSearchState: StateBase.createForList(tagsSearchActionTypes),
  tagAddState: StateBase.createForAdd(tagCreateActionTypes),
  tagDeleteState: StateBase.createForDelete(tagDeleteActionTypes)
};

@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.css']
})
export class TagsComponent {

  public tagsResult: any[];
  public tags: any[];
  private companyId: number;

  constructor(private store: Store<any>) {
    store
      .pipe(
        select(state => state.mainState.tagsState.tagsListState.items),
        filter(tags => tags != null)
      )
      .subscribe((tags: Tag[]) => this.tags = tags.map(tag => {
        return {
          display: tag.name,
          value: tag.id,
          readonly: false
        };
      }));
    store
      .pipe(
        select(state => state.mainState.tagsState.tagsSearchState.items),
        filter(tags => tags != null)
      )
      .subscribe((tags: Tag[]) => this.tagsResult = tags.map(tag => {
        return {
          display: tag.name,
          value: tag.id,
          readonly: false
        };
      }));

    this.store
      .pipe(select(getCompanyId))
      .subscribe(id => {
        if (id != null) {
          this.companyId = id;
          this.store.dispatch(new LoadAction(tagsListActionTypes.get, id));
        }
      });

    this.store
      .pipe(
        select(state => state.mainState.tagsState.tagAddState.itemSave.isSuccess),
        filter(a => a)
      )
      .subscribe(() => {
        this.store.dispatch(new LoadAction(tagsListActionTypes.get, this.companyId))
      })
  }

  onTextChange(text) {
    this.store.dispatch(new LoadAction(tagsSearchActionTypes.get, text));
  }

  onItemRemoved(tag) {
    this.store.dispatch(new LoadAction(tagDeleteActionTypes.delete, {
      companyId: this.companyId,
      tagId: tag.value
    }))
  }

  onItemAdded(tag) {
    this.store.dispatch(new LoadAction(tagCreateActionTypes.submit, {
      companyId: this.companyId,
      tagId: tag.value != tag.display ? tag.value : 0,
      value: tag.display
    }));
  }
}
