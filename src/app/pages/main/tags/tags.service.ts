import 'rxjs/add/operator/map';
import {Injectable} from '@angular/core';
import {Tag} from './tag.model';
import {environment} from "environments/environment";
import {HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {HttpService} from "../../../../../projects/uir-core/src/lib/services/http.service";

@Injectable({providedIn: "root"})
export class TagsService {

  constructor(private http: HttpService) {
  }

  private tagsSearch = environment.api + 'api/tag/tags';
  private getTags = (id): string => {
    return environment.api + 'api/companies/' + id + '/tags';
  }

  private tag = (companyId): string => {
    return environment.api + 'api/companies/' + companyId + '/tags';
  }

  tags = (companyId): Observable<Tag[]> => {
    let url = this.getTags(companyId);
    return this.http.GET<Tag[]>(url);
  }

  search = (text): Observable<Tag[]> => {
    let params = new HttpParams();
    params = params.set('name', text);
    return this.http.GET<Tag[]>(this.tagsSearch, params);
  }

  bind = (data): Observable<any> => {
    let url = this.tag(data.companyId);
    return this.http.POST<Tag[]>(url, data);
  }

  untie = (data): Observable<any> => {
    let url = this.tag(data.companyId) + '/' + data.tagId;
    return this.http.DELETE<Tag[]>(url);
  }
}
