import {Component, Input, OnInit} from '@angular/core';
import {DashboardFilial} from "../dashboard-filials/dashboard-filials.model";
import {LoadAction} from "../../../../shared/core/ngrx/action/base.action";
import {dashboardFilialDeleteActionsType} from "../dashboard-filials/dashboard-filials.component";
import {Store} from "@ngrx/store";

declare var swal: any;
declare var $: any;

@Component({
  selector: 'app-dashboard-filial',
  templateUrl: './dashboard-filial.component.html',
  styleUrls: ['./dashboard-filial.component.css']
})
export class DashboardFilialComponent implements OnInit {

  @Input() filial: DashboardFilial;
  @Input() companyId: number;

  constructor(private store: Store<any>) {

  }

  ngOnInit() {
  }

  copy() {

  }

  delete(filialId) {
    let me = this;
    let companyId = me.companyId;
    swal({
      title: 'Вы уверены?',
      text: 'Вы точно хотите безвозвратно удалить этот филиал?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Да, удалить его!',
      cancelButtonText: 'Нет',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(function () {
      me.store.dispatch(new LoadAction(dashboardFilialDeleteActionsType.delete, {
        companyId: companyId,
        filialId: filialId
      }));
    });
  }

}
