import {Actions, Effect} from '@ngrx/effects';
import {DashboardService} from "app/pages/main/dashboard/dashboard.service";
import {Injectable} from "@angular/core";
import {Store} from '@ngrx/store';
import {
  dashboardFilialDeleteActionsType,
  dashboardFilialsActionsType
} from './dashboard-filials/dashboard-filials.component';
import {getRequestEffectWithData} from "app/shared/core/ngrx/effect/responce.effects";

@Injectable()
export class DashboardEffects {
    @Effect()
    filialsList$ = getRequestEffectWithData(this.actions$, dashboardFilialsActionsType.get, this.dashboardService.filialsList)

    @Effect()
    filialDelete$ = getRequestEffectWithData(this.actions$, dashboardFilialDeleteActionsType.delete, this.dashboardService.filialDelete)

    constructor(
        private store: Store<any>,
        private actions$: Actions,
        private dashboardService: DashboardService) {
    }
}
