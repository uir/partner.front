import {DashboardFilial} from './dashboard-filials/dashboard-filials.model';
import {Injectable} from '@angular/core';
import {environment} from "environments/environment";
import {Observable} from "rxjs";
import {HttpService} from "../../../../../projects/uir-core/src/lib/services/http.service";

@Injectable({providedIn: "root"})
export class DashboardService {

  constructor(private http: HttpService) {
  }

  private filial = (id): string => {
    return environment.api + 'api/companies/' + id + '/dashboard/places';
  }

  private filialDeleteUrl = (data): string => {
    return environment.api + 'api/companies/' + data.companyId + '/places/' + data.filialId;
  }

  filialsList = (companyId): Observable<DashboardFilial[]> => {
    let url = this.filial(companyId);
    return this.http.GET<DashboardFilial[]>(url);
  }

  filialDelete = (data): Observable<any> => {
    console.log(data)
    let url = this.filialDeleteUrl(data);
    return this.http.DELETE<any>(url);
  }
}
