import {AppRoutes} from "app/app.routing";
import {CommonModule} from '@angular/common';
import {DashboardComponent} from './dashboard.component';
import {DashboardEffects} from "app/pages/main/dashboard/dashboard.effects";
import {DashboardFilialsComponent} from './dashboard-filials/dashboard-filials.component';
import {EffectsModule} from "@ngrx/effects";
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from "@angular/common/http";
import {DashboardFilialComponent} from './dashboard-filial/dashboard-filial.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    EffectsModule.forFeature([DashboardEffects]),
    HttpClientModule
  ],
  declarations: [
    DashboardComponent,
    DashboardFilialsComponent,
    DashboardFilialComponent
  ],
  providers: [
    DashboardEffects
  ]
})
export class DashboardModule {
}
