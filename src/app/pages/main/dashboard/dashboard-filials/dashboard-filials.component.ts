import {Component, OnInit} from '@angular/core';
import {RequestDeleteActions, RequestGetActions, RequestListState} from '../../../../shared/core/ngrx/reducer/responce.reducer';

import {DashboardFilial} from "app/pages/main/dashboard/dashboard-filials/dashboard-filials.model";
import {LoadAction} from '../../../../shared/core/ngrx/action/base.action';
import {StateBase} from '../../../../shared/core/ngrx/reducer/base.state';
import {select, Store} from '@ngrx/store';
import {getCompanyId} from "app/pages/main/admin-layout.reducer";
import {Observable} from "rxjs";
import {filter, first} from "rxjs/operators";

export const dashboardFilialsActionsType = new RequestGetActions("DASHBOARD_FILIALS");
export const dashboardFilialDeleteActionsType = new RequestDeleteActions("FILIAL_DELETE");

export const dashboardFilialsState = {
  filialsListState: StateBase.create(dashboardFilialsActionsType, RequestListState.getInitialLoad, RequestListState.getReducer),
  filialDeleteState: StateBase.createForDelete(dashboardFilialDeleteActionsType),
};

@Component({
  selector: 'app-dashboard-filials',
  templateUrl: './dashboard-filials.component.html',
  styleUrls: ['./dashboard-filials.component.css']
})
export class DashboardFilialsComponent implements OnInit {

  public filials: Observable<DashboardFilial[]>;
  public companyId: number;

  constructor(private store: Store<any>) {
    this.store
      .pipe(
        select(getCompanyId),
        first()
      )
      .subscribe(id => {
        this.companyId = id;
        this.store.dispatch(new LoadAction(dashboardFilialsActionsType.get, id));
      });

    this.filials = this.store.select(state => state.mainState.dashboardState.dashboardFilialsState.filialsListState.items);

    this.store
      .pipe(
        select(state => state.mainState.dashboardState.dashboardFilialsState.filialDeleteState.itemDelete.isSuccess),
        filter(a => a)
      )
      .subscribe(a => {
        this.store.dispatch(new LoadAction(dashboardFilialsActionsType.get, this.companyId))
      });
  }

  ngOnInit() {
  }
}
