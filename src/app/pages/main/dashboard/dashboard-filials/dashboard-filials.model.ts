export class DashboardFilial {
    name: string;
    id: number;
    address: string;
    website: string;
    phone: string;
    vicinity: string;
    lat: number;
    lng: number;
    isActive: boolean;
    sales: DashboardFilialSale[];
    images: DashboardFilialImage[];
    videos: DashboardFilialVideo[];
}

export class DashboardFilialSale {
    name: string;
    amount: string;
}

export class DashboardFilialImage {
    file: string;
}

export class DashboardFilialVideo {
    name: string;
}
