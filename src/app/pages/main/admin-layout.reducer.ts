import {dashboardState} from "app/pages/main/dashboard/dashboard.reducer";
import {filialsState} from './filials/filials.reducer';
import {infoState} from "./info/info.component";
import {navbarState} from "./navbar/navbar.component";
import {sidebarState} from './sidebar/sidebar.reducer';
import {tagsState} from './tags/tags.component';

export const companyIdActionsType = {
  get: 'COMPANY_ID_SET'
}
export const mainState = {
  companyState: {
    actions: [companyIdActionsType.get],
    initialState: {
      companyId: null
    },
    reducer: (s, a): any => {
      if (a.type == companyIdActionsType.get)
        return {
          companyId: a.payload
        }
      return s;
    }
  },
  infoState: infoState,
  filialsState: filialsState,
  sidebarState: sidebarState,
  dashboardState: dashboardState,
  tagsState: tagsState,
  navbarState: navbarState
}


export const getCompanyId = (state): number => state.mainState.companyState.companyId;
