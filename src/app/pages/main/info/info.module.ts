import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {EffectsModule} from "@ngrx/effects";
import {InfoComponent} from "./info.component";
import {InfoEffects} from "./info.effects";
import {NgModule} from "@angular/core";
import {CategoriesCheckerModule} from "../../../shared/categories-checker/categories-checker.module";
import {InfoBaseEditForm} from "./edit/info-base-edit.form";
import {InfoCompanyEditForm} from "./edit/info-company-edit.form";
import {InfoIpEditForm} from "./edit/info-ip-edit.form";
import {InfoSelfEployedEditForm} from "./edit/info-selfEployed-edit.form";
import {FileUploadModule} from "ng2-file-upload";
import {HttpClientModule} from "@angular/common/http";
import {TagInputModule} from "../../../shared/core/components/nfx-chips/modules";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TagInputModule,
    CategoriesCheckerModule,
    EffectsModule.forFeature([InfoEffects]),
    FileUploadModule,
    HttpClientModule
  ],
  exports: [],
  declarations: [InfoComponent],
  providers: [
    InfoEffects,
    InfoBaseEditForm,
    InfoCompanyEditForm,
    InfoIpEditForm,
    InfoSelfEployedEditForm
  ],
})
export class InfoModule {
}
