import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {BaseForm} from "app/shared/core/form/base.form";
import {IpInfo} from "../info.model";
import {Injectable} from '@angular/core';

@Injectable()
export class InfoIpEditForm extends BaseForm<IpInfo> {

  protected getForm(): FormGroup {
    return this.fb.group({
      inn: [this.data.inn],
      ogrn: [this.data.ogrn, [
        Validators.required
      ]],
    });
  }


  protected getValidationMessages() {
    return {
      ogrn: {required: this.required},
    };
  }

  protected getLables() {
    return {
      inn: 'ИНН',
      ogrn: 'ОГРН',
    };
  }

  constructor(fb: FormBuilder) {
    super({
      inn: '',
      ogrn: '',
    }, fb);
    this.validationMessages = this.getValidationMessages();
    this.form = this.getForm();
    this.lables = this.getLables();
    this.readOnly = ['inn']
  }
}
