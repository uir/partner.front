import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { BaseForm } from "app/shared/core/form/base.form";
import {BaseInfo} from "../info.model";
import { Injectable } from '@angular/core';

@Injectable()
export class InfoBaseEditForm extends BaseForm<BaseInfo>{

  protected getForm(): FormGroup {
    return this.fb.group({
      id: [this.data.id],
      name: [this.data.name,
        [
          Validators.required,
          Validators.pattern("^.{1,40}$")
        ]],
      address: [this.data.address],
      fio: [this.data.fio, [
        Validators.required,
        Validators.pattern("^.{6,50}$")
      ]],
      categories: [this.data.categories],
    });
  }

  protected getValidationMessages() {
    return {
      name: {required: this.required, pattern: "Поле не должно превышать 50 символов"},
      fio: {required: this.required, pattern: "Поле должно содержать от 6 до 50 символов"}
    };
  }

  protected getLables() {
    return {
      name: 'Название',
      address: 'Адрес',
      fio: 'ФИО',
      categories: 'Категории',
    };
  }
  constructor(fb: FormBuilder) {
    super({
      id: null,
      name: '',
      address: '',
      fio: '',
      categories: [],
      activeType: null,
      file: null
    }, fb);
    this.validationMessages = this.getValidationMessages();
    this.nonVisibleKeys = ['id', 'categories', 'categoriesId', 'activeType', 'file'];
    this.form = this.getForm();
    this.lables = this.getLables();
    this.readOnly = ['address']
  }
}
