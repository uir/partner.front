import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { BaseForm } from "app/shared/core/form/base.form";
import {CompanyInfo} from "../info.model";
import { Injectable } from '@angular/core';

@Injectable()
export class InfoCompanyEditForm extends BaseForm<CompanyInfo>{

  protected getForm(): FormGroup {
    return this.fb.group({
      inn: [this.data.inn],
      kpp: [this.data.kpp],
      ogrn: [this.data.ogrn, [
        Validators.required
      ]],
    });
  }

  protected getValidationMessages() {
    return {
      ogrn: {required: this.required},
    };
  }

  protected getLables() {
    return {
      inn: 'ИНН',
      kpp: 'КПП',
      ogrn: 'ОГРН',
    };
  }
  constructor(fb: FormBuilder) {
    super({
      inn: '',
      kpp: '',
      ogrn: '',
    }, fb);
    this.validationMessages = this.getValidationMessages();
    this.form = this.getForm();
    this.lables = this.getLables();
    this.readOnly = ['inn', 'kpp']
  }
}
