import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { BaseForm } from "app/shared/core/form/base.form";
import { Injectable } from '@angular/core';
import {SelfEmployedInfo} from "../info.model";

@Injectable()
export class InfoSelfEployedEditForm extends BaseForm<SelfEmployedInfo>{
  protected getForm(): FormGroup {
    return this.fb.group({
      birthday: [this.data.birthday],
      seriesPassport: [this.data.seriesPassport, [
        Validators.required,
        Validators.pattern("^\\d{4}$"),
      ]],
      numberPassport: [this.data.numberPassport, [
        Validators.required,
        Validators.pattern("^\\d{6}$"),
      ]],
      residenceAddress: [this.data.residenceAddress, [
        Validators.required,
      ]],
      passportDepartment: [this.data.passportDepartment, [
        Validators.required,
      ]],
      datePassport: [this.data.datePassport, [
        Validators.required,
        Validators.pattern("^\\d{2}\\.\\d{2}\\.\\d{4}$")
      ]],
    });
  }
  protected getValidationMessages() {
    return {
      numberPassport: {required: this.required, pattern: "Значение должно содержать 6 цифр"},
      seriesPassport: {required: this.required, pattern: "Значение должно содержать 4 цифр"},
      datePassport: {required: this.required, pattern: "Дата должна быть в формате дд.мм.гггг"},
      residenceAddress: {required: this.required},
      passportDepartment: {required: this.required},
    };
  }
  protected getPlaceholders() {
    return {
      numberPassport: '',
      seriesPassport: '',
      residenceAddress: '',
      birthday: '',
      passportDepartment: '',
      datePassport: '',
    };
  }
  protected getLables() {
    return {
      birthday: 'День рождения',
      seriesPassport: 'Серия паспорта',
      numberPassport: 'Номер паспорта',
      residenceAddress: 'Адрес проживания по паспорту',
      passportDepartment: 'Кем выдан паспорт',
      datePassport: 'Когда выдан папорт',
    };
  }
  protected getIcons() {
    return {
      numberPassport: 'description',
      seriesPassport: 'description',
      residenceAddress: 'description',
      birthday: 'description',
      passportDepartment: 'description',
      datePassport: 'description',
    };
  }
  constructor(fb: FormBuilder) {
    super({
      numberPassport: '',
      seriesPassport: '',
      residenceAddress: '',
      birthday: '',
      passportDepartment: '',
      datePassport: '',
    }, fb);
    this.validationMessages = this.getValidationMessages();
    this.form = this.getForm();
    this.placeholders = this.getPlaceholders();
    this.lables = this.getLables();
    this.icons = this.getIcons();
    this.readOnly = ['birthday']
  }
}
