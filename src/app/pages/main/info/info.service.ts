import {Injectable} from '@angular/core';
import {environment} from "environments/environment";
import {TemplateCompanyCreate} from "../../user/modules/companies/create/create.company.model";
import {Observable} from "rxjs";
import {HttpService} from "../../../../../projects/uir-core/src/lib/services/http.service";

@Injectable({providedIn: "root"})
export class InfoService {

  constructor(private http: HttpService) {
  }

  private shortcategories = environment.api + 'api/category/all/short';

  private getInfo = (id): string => {
    return environment.api + 'api/companies/' + id;
  }

  info = (companyId): Observable<any> => {
    let url = this.getInfo(companyId);
    return this.http.GET<any>(url);
  }

  editInfo = (data): Observable<any> => {
    let url = this.getInfo(data.id);
    return this.http.POST<any>(url, data);
  }

  getCategories = (): Observable<TemplateCompanyCreate> => {
    return this.http.GET(this.shortcategories);
  }
}
