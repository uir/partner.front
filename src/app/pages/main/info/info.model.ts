export class BaseInfo {
  id: number;
  name: string;
  address: string;
  fio: string;
  activeType: number;
  categories?: any[];
  file: string;
}

export class IpInfo{
  inn: string;
  ogrn: string;
}

export class CompanyInfo extends IpInfo{
  kpp: string;
}

export class SelfEmployedInfo{
  numberPassport: string;
  seriesPassport: string;
  residenceAddress: string;
  birthday: string;
  passportDepartment: string;
  datePassport: string;
}

export class Category {
  name: string;
  id: number
}

