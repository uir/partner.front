import { Actions } from '@ngrx/effects';
import { Effect } from '@ngrx/effects';
import { Injectable } from "@angular/core";
import {getRequestEffect, getRequestEffectWithData} from "app/shared/core/ngrx/effect/responce.effects";
import {InfoService} from "./info.service";
import {categoriesListActionTypes, companyInfoActionTypes, companyInfoEditActionTypes} from "./info.component";

@Injectable()
export class InfoEffects {

  @Effect()
  info$ = getRequestEffectWithData(this.actions$, companyInfoActionTypes.get, this.infoService.info)

  @Effect()
  infoEdit$ = getRequestEffectWithData(this.actions$, companyInfoEditActionTypes.submit, this.infoService.editInfo)

  @Effect()
  companyCategories$ = getRequestEffect(this.actions$, categoriesListActionTypes.get, this.infoService.getCategories);

  constructor(private actions$: Actions, private infoService: InfoService) {
  }

}
