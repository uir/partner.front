import {RequestEditActions, RequestGetActions,} from "../../../shared/core/ngrx/reducer/responce.reducer";
import {Component, OnInit} from "@angular/core";
import {LoadAction} from "app/shared/core/ngrx/action/base.action";
import {StateBase} from 'app/shared/core/ngrx/reducer/base.state';
import {select, Store} from '@ngrx/store';
import {getCompanyId} from "../admin-layout.reducer";
import {TemplateCompanyCreate} from "../../user/modules/companies/create/create.company.model";
import {InfoBaseEditForm} from "./edit/info-base-edit.form";
import {InfoCompanyEditForm} from "./edit/info-company-edit.form";
import {InfoIpEditForm} from "./edit/info-ip-edit.form";
import {InfoSelfEployedEditForm} from "./edit/info-selfEployed-edit.form";
import {BaseInfo, Category, CompanyInfo, IpInfo, SelfEmployedInfo} from "./info.model";
import {FileUploader} from "ng2-file-upload";
import {environment} from "../../../../environments/environment";
import {filter} from "rxjs/operators";
import {NotificationService} from "../../../shared/core/modules/notification/notification.service";
import {AuthService} from "../../../../../projects/uir-core/src/lib/services/auth.service";

export const companyInfoActionTypes = new RequestGetActions("COMPANY_INFO");
export const companyInfoEditActionTypes = new RequestEditActions("COMPANY_INFO_EDIT");
export const categoriesListActionTypes = new RequestGetActions("CATEGORIES_LIST");


export const infoState = {
  companyInfoState: StateBase.createForObject(companyInfoActionTypes),
  companyInfoEditState: StateBase.createForEdit(companyInfoEditActionTypes),
  categoriesListActionTypes: StateBase.createForList(categoriesListActionTypes)
};

@Component({
  selector: 'app-info',
  templateUrl: 'info.component.html',
  styleUrls: ['./info.component.css']
})
export class InfoComponent implements OnInit {

  public baseInfo: BaseInfo;
  public companyInfo: CompanyInfo;
  public ipInfo: IpInfo;
  public selfEmployedInfo: SelfEmployedInfo;
  public uploader: FileUploader;
  public templateCategories: TemplateCompanyCreate[];
  public companyCategoriesIds: number[];

  private companyId: number;
  private allCategories: Category[];

  constructor(public baseInfoform: InfoBaseEditForm,
              public companyForm: InfoCompanyEditForm,
              public ipForm: InfoIpEditForm,
              public selfEmployedForm: InfoSelfEployedEditForm,
              private notifyService: NotificationService,
              private auth: AuthService,
              private store: Store<any>) {
    this.baseInfo = null;
    this.store
      .pipe(
        select(state => state.mainState.infoState.companyInfoState.item),
        filter(info => info != null)
      )
      .subscribe(info => {
          this.baseInfo = {
            id: info.id,
            name: info.name,
            address: info.address,
            fio: info.fio,
            activeType: info.activeType,
            categories: info.categories.map(c => {
              return {
                display: c.name,
                value: c.id,
                readonly: false
              };
            }),
            file: info.logo ? info.logo : 'assets/img/default-avatar.png',
          };
          switch (info.activeType) {
            case 1: {
              this.companyInfo = {
                inn: info.inn,
                kpp: info.kpp,
                ogrn: info.ogrn,
              };
              break
            }
            case 2: {
              this.ipInfo = {
                inn: info.inn,
                ogrn: info.ogrn,
              };
              break
            }
            case 3: {
              this.selfEmployedInfo = {
                numberPassport: info.numberPassport,
                seriesPassport: info.seriesPassport,
                residenceAddress: info.residenceAddress,
                birthday: info.birthday,
                passportDepartment: info.passportDepartment,
                datePassport: info.datePassport
              };
              break
            }
          }
          this.companyCategoriesIds = info.categories.map(x => x.id)
        }
      );

    this.store
      .pipe(
        select(state => state.mainState.infoState.companyInfoState.itemLoad.isSuccess),
        filter(a => a)
      )
      .subscribe(() => {
          this.baseInfoform.setFormData(this.baseInfo);
          switch (this.baseInfo.activeType) {
            case 1: {
              this.companyForm.setFormData(this.companyInfo);
              break
            }
            case 2: {
              this.ipForm.setFormData(this.ipInfo);
              break
            }
            case 3: {
              this.selfEmployedForm.setFormData(this.selfEmployedInfo);
              break
            }
          }
        }
      );

    this.store
      .pipe(
        select(state => state.mainState.infoState.categoriesListActionTypes.items),
        filter(a => a != null))
      .subscribe(categories => {
        this.templateCategories = categories;
        this.allCategories = this.templateCategories.map(x => x.categories).reduce(function (memo, item) {
          return memo.concat(item);
        }, [])
      });

    this.store
      .pipe(select(getCompanyId))
      .subscribe(id => {
        let load = new LoadAction(companyInfoActionTypes.get, id);
        this.store.dispatch(load);
        this.init(id);
      });

    this.store
      .pipe(
        select(state => state.mainState.infoState.companyInfoEditState.itemSave.isSuccess),
        filter(x => x)
      )
      .subscribe(() => this.notifyService.notify("info", "Информация изменена."));
  }

  getTypeName() {
    if (this.baseInfo == null) return '';
    else {
      switch (this.baseInfo.activeType) {
        case 1:
          return 'Организация';
        case 2:
          return 'Индивидуальный предприниматель';
        case 3:
          return 'Самозанятость'
      }
    }
  }

  disabled() {
    if (!this.baseInfoform.isValid()) return false;
    switch (this.baseInfo.activeType) {
      case 1: {
        return this.companyForm.isValid()
      }
      case 2: {
        return this.ipForm.isValid()
      }
      case 3: {
        return this.selfEmployedForm.isValid()
      }
    }
  }

  onCheck(checkedCategories) {
    this.companyCategoriesIds = checkedCategories;
    this.infoCategoryEdit();
  }

  infoCategoryEdit() {
    this.baseInfo.categories = this.allCategories.filter(x => this.companyCategoriesIds.includes(x.id)).map(c => {
      return {
        display: c.name,
        value: c.id, readonly: false
      };
    })
  }


  private init(id) {
    if (id != null) {
      this.companyId = id;
      this.uploader = new FileUploader(
        {
          isHTML5: true,
          url: environment.api + "api/companies/" + id + "/logo",
          allowedMimeType: ['image/png', 'image/jpeg'],
        }
      );
      this.uploader.options.headers = [
        {
          name: 'Authorization',
          value: 'bearer ' + this.auth.accessToken
        }
      ]
    }
  }

  edit() {
    let base = this.baseInfoform.getValue();
    let info = null;
    switch (this.baseInfo.activeType) {
      case 1: {
        info = this.companyForm.getValue();
        break
      }
      case 2: {
        info = this.ipForm.getValue();
        break
      }
      case 3: {
        info = this.selfEmployedForm.getValue();
        break
      }
    }

    delete base["categories"];
    base.id = this.companyId;
    base.activeType = this.baseInfo.activeType;
    let data = {...base, ...info, categoriesIds: this.companyCategoriesIds};
    this.store.dispatch(new LoadAction(companyInfoEditActionTypes.submit, data));

    this.uploader.uploadAll()
  }

  getCategories() {
    this.store.dispatch(new LoadAction(categoriesListActionTypes.get))
  }

  onItemRemoved(tag) {
    this.companyCategoriesIds = this.companyCategoriesIds.filter(x => x != tag.value);
    if (this.allCategories.length > 0) this.infoCategoryEdit();
  }

  ngOnInit() {
    this.baseInfoform.buildForm();
    this.companyForm.buildForm();
    this.ipForm.buildForm();
    this.selfEmployedForm.buildForm();

  }
}


