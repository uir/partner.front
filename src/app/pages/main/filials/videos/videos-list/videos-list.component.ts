import {Component, OnInit} from '@angular/core';
import {Video} from "../filial-video.model";
import {videosActionTypes} from "../filial-videos.reducer";
import {getActionTypeLoad, LoadAction} from "../../../../../shared/core/ngrx/action/base.action";
import {select, Store} from "@ngrx/store";
import {ActivatedRoute} from "@angular/router";
import {Observable} from "rxjs";
import {filter, map} from "rxjs/operators";

declare var $: any;
declare var swal: any;

@Component({
  selector: 'app-videos-list',
  templateUrl: './videos-list.component.html',
  styleUrls: ['./videos-list.component.css']
})
export class VideosListComponent implements OnInit {

  public videos$: Observable<Video[]>;
  private filialId: number;

  constructor(private store: Store<any>, private router: ActivatedRoute) {
    this.router.params
      .pipe(map(params => +params['id']))
      .subscribe(id => {
        if (id != null)
          this.init(id);
      });
    this.videos$ = this.store.pipe(select(state => state.mainState.filialsState.videosState.items));
  }

  private init(id) {
    this.filialId = id;
    this.store.dispatch(new LoadAction(videosActionTypes.get, id));

    this.store
      .pipe(
        select(state => state.mainState.filialsState.videosState.itemsDelete.isSuccess),
        filter(isSuccess => isSuccess)
      )
      .subscribe(() => this.store.dispatch(new LoadAction(videosActionTypes.get, id)))
  }

  ngOnInit() {
  }

  delete(id) {
    let me = this;
    swal({
      title: 'Вы уверены?',
      text: 'Вы точно хотите безвозвратно удалить данное видео?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Да, удалить его!',
      cancelButtonText: 'Нет',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(function () {
      me.store.dispatch({
        type: getActionTypeLoad(videosActionTypes.delete),
        payload: {
          filialId: me.filialId,
          id: id
        }
      });
    });
  }

}
