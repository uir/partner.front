import {Component, OnInit} from '@angular/core';
import {FileUploader} from "ng2-file-upload";
import {select, Store} from "@ngrx/store";
import {ActivatedRoute} from "@angular/router";
import {videosActionTypes} from "../filial-videos.reducer";
import {LoadAction} from "../../../../../shared/core/ngrx/action/base.action";
import {environment} from "../../../../../../environments/environment";
import {getToken} from "../../../../user/modules/auth/components/token/token.reducer";
import {UserPaymentPlanService} from "../../../../../services/user-payment-plan.service";
import {notifyActions} from "../../../../../shared/core/modules/notification/notification.reducer";
import {Error} from "../../../../../shared/core/model/error.model";
import {FileItem} from "ng2-file-upload/file-upload/file-item.class";
import {map} from "rxjs/operators";
import {AuthService} from "../../../../../../../projects/uir-core/src/lib/services/auth.service";

@Component({
  selector: 'app-videos-loader',
  templateUrl: './videos-loader.component.html',
  styleUrls: ['./videos-loader.component.css']
})
export class VideosLoaderComponent implements OnInit {

  private token: string;
  public uploader: FileUploader;
  public hasBaseDropZoneOver = false;
  public hasAnotherDropZoneOver = false;
  public videosAllowCount: number;

  constructor(private store: Store<any>,
              private router: ActivatedRoute,
              private auth: AuthService,
              private userService: UserPaymentPlanService) {
    this.userService
      .getVideosAllowCount()
      .subscribe(value => this.videosAllowCount = value);
    this.token = '';
    this.router.params
      .pipe(map(params => +params['id']))
      .subscribe(id => {
        if (id != null)
          this.init(id);
      });
  }

  private init(id) {
    this.uploader = new FileUploader(
      {
        isHTML5: true,
        url: environment.api + "api/places/" + id + "/videos/upload",
        allowedMimeType: ['video/mp4', 'video/avi', 'video/mpeg', 'video/mov', 'video/wmv', 'video/flv'],
      });
    this.uploader.onCompleteAll = () => {
      this.store.dispatch(new LoadAction(videosActionTypes.get, id));
      // this.store.dispatch({type: loaderActions.disable});
      this.uploader.clearQueue();
    };
    this.uploader.onBeforeUploadItem = () => {
      // this.store.dispatch({type: loaderActions.active});
    };
    this.uploader.onErrorItem = (tem: FileItem, response: string, status: number) => {
      // this.store.dispatch({type: loaderActions.disable});
      let error = null;
      if (response === '') {
        error = {
          message: 'Размер вашего файл превышает допустимый лимит в 50мб.',
          status: 400
        }
      } else
        error = Error.parse(response);
      this.store.dispatch({type: notifyActions.notify, payload: error.message})
    };

    this.uploader.options.headers = [
      {
        name: 'Authorization',
        value: 'bearer ' + this.auth.accessToken
      }
    ]
  }

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  public fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
  }

  ngOnInit() {
  }

}
