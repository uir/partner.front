import { EditableListActions} from '../../../../shared/core/ngrx/reducer/responce.reducer';

import { StateBase } from 'app/shared/core/ngrx/reducer/base.state';

export const videosActionTypes = new EditableListActions("FILIAL_VIDEOS");

export const videosState = StateBase.createForEditableList(videosActionTypes)

