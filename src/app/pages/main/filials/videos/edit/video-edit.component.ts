import {Component} from '@angular/core';
import {getCompanyId} from '../../../admin-layout.reducer';
import {videosActionTypes} from '../filial-videos.reducer';
import {ActivatedRoute} from '@angular/router';
import {EditComponent} from "app/shared/core/ngrx/component/edit.component";
import {NavigationAction} from '../../../../../shared/core/modules/navigation/navigation.module';
import {select, Store} from "@ngrx/store";
import {VideoEdit} from './video-edit.model';
import {VideoEditForm} from "app/pages/main/filials/videos/edit/video-edit.form";
import {getActionTypeLoad} from 'app/shared/core/ngrx/action/base.action';
import {filter, map, withLatestFrom} from "rxjs/operators";

@Component({
  selector: 'app-video-edit',
  templateUrl: './video-edit.component.html',
  styleUrls: ['./video-edit.component.css']
})
export class VideoEditComponent extends EditComponent<VideoEdit, VideoEditForm> {

  private _filialId: string;

  constructor(form: VideoEditForm, store: Store<any>, private router: ActivatedRoute) {
    super(form, store, videosActionTypes, (state) => state.mainState.filialsState.videosState.item);

    this.router.params
      .pipe(map(value => value["filialId"]))
      .subscribe(value => this._filialId = value);

    this.router.params
      .pipe(map(params => +params['id']))
      .subscribe(id => {
        this.load({
          type: getActionTypeLoad(videosActionTypes.getItem),
          payload: id
        });
      });

    this.router.params
      .pipe(
        map(params => +params['filialId']),
        withLatestFrom(this.store.select(getCompanyId))
      )
      // todo refactor flatten
      .subscribe(ids => {
        this.store
          .pipe(
            select(state => state.mainState.filialsState.videosState.itemSave.isSuccess),
            filter(x => x)
          )
          .subscribe(x =>
            this.store.dispatch(new NavigationAction('/companies/' + ids[1] + '/filials/' + ids[0] + '/videos')))
      })
  }

  public save(): void {
    let data = this.form.getValue();
    if (this.form.isValid() && this._filialId != null) {
      this.store.dispatch({
        type: getActionTypeLoad(this.actionsTypes.submit),
        payload: {...data, filialId: this._filialId}
      });
    }
  }
}
