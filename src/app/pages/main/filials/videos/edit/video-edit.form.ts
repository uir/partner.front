import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BaseForm } from "app/shared/core/form/base.form";
import { Injectable } from '@angular/core';
import { VideoEdit } from "app/pages/main/filials/videos/edit/video-edit.model";

@Injectable()
export class VideoEditForm extends BaseForm<VideoEdit>{

    protected getForm(): FormGroup {
        return this.fb.group({
            name: [this.data.name, [Validators.required]],
            id: [],
            discription: []
        });
    }

    protected getValidationMessages() {
        return {
            name: { required: this.required }
        };
    }
    protected getLables() {
        return {
            name: 'Название',
            discription: 'Описание',
        };
    }
    constructor(fb: FormBuilder) {
        super({
          id: 0,
          name: '',
          discription: '',
        }, fb);
        this.nonVisibleKeys = ['id'];
        this.validationMessages = this.getValidationMessages();
        this.form = this.getForm();
        this.lables = this.getLables();
    }
}
