import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BaseForm} from "app/shared/core/form/base.form";
import {FilialEdit} from './filial-edit.model';
import {Injectable} from '@angular/core';
import {Address} from "../../../../shared/google-place-autocomplete/model/address.model";

@Injectable()
export class FilialEditForm extends BaseForm<FilialEdit> {
  public roomTypes = [
    {value: 0, name: 'Не выбрано'},
    {value: 1, name: 'Офис'},
    {value: 2, name: 'Квартира'}
  ];

  public editorOptions = {
    placeholder: "Составте свое уникальное описание",
    modules: {
      toolbar: [
        ['bold', 'italic', 'underline', 'strike'],
        ['blockquote'],
        [{'list': 'ordered'}, {'list': 'bullet'}],
        [{'indent': '-1'}, {'indent': '+1'}],
        [{'size': ['small', false, 'large', 'huge']}],
        [{'color': []}, {'background': []}],
        [{'font': []}],
        [{'align': []}],
        ['image']
      ]
    }
  };

  protected getForm(): FormGroup {
    return this.fb.group({
      name: [this.data.name, [Validators.required, Validators.pattern("^.{1,40}$")]],
      lat: [this.data.lat, [Validators.required, Validators.pattern("^[-]?\\d*[.,]?\\d+$")]],
      lng: [this.data.lng, [Validators.required, Validators.pattern("^[-]?\\d*[.,]?\\d+$")]],
      phone: [this.data.phone, [Validators.pattern("^(\\d{7,11}){1}(\\s?\\,?\\s?(\\d{7,11}))*$")]],
      website: [this.data.website, [Validators.pattern("^(https?:\\/\\/)?([\\d,a-z\\.\\-]+)\\.([a-zA-Z\\.]{2,6})([\\/\\w \\.-]*)*\\/?$")]],
      address: [this.data.address, [Validators.pattern("^.{0,100}$")]],
      priceLevel: [this.data.priceLevel],
      id: [],
      infoBaseId: [],
      isActive: [this.data.isActive],
      roomType: [this.data.roomType],
      room: [this.data.room],
      description: [this.data.description],
    });
  }

  onContentChanged({quill, html, text}) {
    this.getValue().description = html
  }

  setAddressPlace(address: Address) {
    let lat = null;
    let lng = null;
    let error = '';
    if (address.street == null && address.street == "") {
      error = 'Укажите точный адрес'
    }
    else {
      lat = address.lat;
      lng = address.lng;
    }
    this.setFormData({...this.getValue(), lat: lat, lng: lng, address: address.formattedAddress});
    this.formErrors.address = error
  }

  getRoomType() {
    return this.getValueData('roomType') == null ? 0 : this.getValueData('roomType')
  }

  getTypeName() {
    return this.roomTypes.filter(x => x.value == this.getRoomType())[0].name
  }

  checkType(e) {
    this.setFormData({...this.getValue(), roomType: e.value})
  }

  isInput(f) {
    return (f != 'isActive' && f != 'address' && f != 'roomType' && f != 'room' && f != 'description') ||
      (f == 'room' && this.getRoomType() !== 0)
  }

  public isError(field: any): boolean {
    if (field == 'room')
      return this.getRoomType() != 0 && (this.getValueData('room') == null || this.getValueData('room') == '')
    return super.isError(field)
  }

  public getError(field): string {
    if (field == 'room')
      return this.isError(field) ? 'Обязательно для заполнения' : null
    return super.getError(field);
  }

  isDisabled() {
    return this.isValid() && !this.isError('room')
  }

  public getlabel(f) {
    if (f == 'room') {
      switch (this.getRoomType()) {
        case 0: {
          return null
        }
        case 1: {
          return 'Номер офиса'
        }
        case 2: {
          return 'Номер квартиры'
        }
      }
    }
    return super.getlabel(f);
  }

  public getValidationMessages() {
    return {
      name: {required: this.required, pattern: "Поле не должно превышать 40 символов"},
      lat: {required: this.required, pattern: "Не правильный формат"},
      lng: {required: this.required, pattern: "Не правильный формат"},
      phone: {pattern: "Номера должны состоять из 7-11 цифр без символов и разделяться запятыми"},
      website: {pattern: "Пример поля: UirWebSite.ru"},
      address: {pattern: "Значение должно состоять из 100 цифр"},
      roomType: {},
      room: {},
    };
  }

  protected getLables() {
    return {
      name: 'Название',
      phone: 'Телефон',
      website: "Web сайт",
      address: "Адрес",
      priceLevel: "Уровень цен",
      lat: "Долгота",
      lng: 'Широта',
      isActive: 'Активность',
      roomType: 'Тип помещения',
      description: "Описание",
    };
  }

  constructor(fb: FormBuilder) {
    super({
      infoBaseId: 0,
      id: 0,
      name: '',
      phone: '',
      website: "",
      address: "",
      priceLevel: 0,
      lat: null,
      lng: null,
      isActive: true,
      roomType: 0,
      room: '',
      description: '',
    }, fb);
    this.nonVisibleKeys = ['id', 'infoBaseId', 'priceLevel'];
    this.validationMessages = this.getValidationMessages();
    this.form = this.getForm();
    this.lables = this.getLables();
  }
}
