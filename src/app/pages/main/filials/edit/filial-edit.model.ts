export class FilialEdit {
  infoBaseId: number;
  id: number;
  name: string;
  phone: string;
  website: string;
  address: string;
  priceLevel: number;
  lat: number;
  lng: number;
  isActive: boolean;
  roomType: number;
  room: string;
  description: string;
}
