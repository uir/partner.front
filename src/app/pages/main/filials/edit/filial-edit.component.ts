import {Component, OnInit} from '@angular/core';
import {RequestEditActions, RequestObjectState} from '../../../../shared/core/ngrx/reducer/responce.reducer';
import {ActivatedRoute} from '@angular/router';
import {FilialEdit} from "app/pages/main/filials/edit/filial-edit.model";
import {FilialEditForm} from "app/pages/main/filials/edit/filial-edit.form";
import {NavigationAction} from '../../../../shared/core/modules/navigation/navigation.module';
import {StateBase} from '../../../../shared/core/ngrx/reducer/base.state';
import {select, Store} from "@ngrx/store";
import {getActionTypeLoad} from 'app/shared/core/ngrx/action/base.action';
import {getCompanyId} from 'app/pages/main/admin-layout.reducer';
import {Observable} from "rxjs";
import {filter, map, withLatestFrom} from "rxjs/operators";

export const editFilialActionType = new RequestEditActions("FILIAL_EDIT");
export const editFilialState = StateBase.create(editFilialActionType, RequestObjectState.getInitialEdit, RequestObjectState.getReducer);

@Component({
  selector: 'app-filial-edit',
  templateUrl: './filial-edit.component.html',
  styleUrls: ['./filial-edit.component.css']
})
export class FilialEditComponent implements OnInit {

  private filial: Observable<FilialEdit>;
  private companyId: number;

  constructor(public form: FilialEditForm, private store: Store<any>, private router: ActivatedRoute) {
    this.router.params
      .pipe(
        map(params => +params['id']),
        withLatestFrom(this.store.pipe(select(getCompanyId)))
      )
      .subscribe(arr => {
        let id = arr[0];
        this.companyId = arr[1];
        if (id != null) {
          let load = {
            type: getActionTypeLoad(editFilialActionType.get),
            payload: {
              companyId: arr[1],
              filialId: arr[0]
            }
          };
          this.store.dispatch(load);
          this.store
            .pipe(
              select(state => state.mainState.filialsState.editFilialState.itemSave.isSuccess),
              filter(x => x)
            )
            .subscribe(() =>
              this.store.dispatch(new NavigationAction('/companies/' + arr[1] + '/filials/' + arr[0])));
        }
      });
    this.filial = this.store.pipe(select(state =>
      state.mainState.filialsState.editFilialState.item
    ));
  }

  edit() {
    if (this.form.isDisabled()) {
      let filial = this.form.getValue();
      if (filial.roomType == 0) {
        filial.roomType = null;
        filial.room = null
      }
      filial.infoBaseId = this.companyId;
      this.store.dispatch({
        type: getActionTypeLoad(editFilialActionType.submit),
        payload: filial
      })
    }
  }

  ngOnInit() {
    this.filial.subscribe(filial => this.form.setFormData(filial));
    this.form.buildForm();
  }
}
