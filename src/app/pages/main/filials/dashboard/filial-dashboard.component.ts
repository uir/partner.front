import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {DashboardFilial} from '../../dashboard/dashboard-filials/dashboard-filials.model';
import {RequestGetActions, RequestObjectState} from 'app/shared/core/ngrx/reducer/responce.reducer';
import {StateBase} from 'app/shared/core/ngrx/reducer/base.state';
import {select, Store} from '@ngrx/store';
import {getActionTypeLoad} from "app/shared/core/ngrx/action/base.action";
import {getCompanyId} from '../../admin-layout.reducer';
import {map, withLatestFrom} from "rxjs/operators";

export const dashboardFilialActionTypes = new RequestGetActions('FILIAL_DASHBOARD');
export const dasgboardFilialState = StateBase.create(dashboardFilialActionTypes, RequestObjectState.getInitialLoad, RequestObjectState.getReducer);

@Component({
  selector: 'app-filial-dashboard',
  templateUrl: './filial-dashboard.component.html',
  styleUrls: ['./filial-dashboard.component.css']
})
export class FilialDashboardComponent implements OnInit {

  public filial: DashboardFilial;

  constructor(private store: Store<any>, private route: ActivatedRoute) {
    this.route.params
      .pipe(
        map(params => +params['id']),
        withLatestFrom(this.store.pipe(select(getCompanyId)))
      )
      .subscribe(arr => {
        let id = arr[0];
        if (id != null)
          this.store.dispatch({
            type: getActionTypeLoad(dashboardFilialActionTypes.get),
            payload: {
              companyId: arr[1],
              filialId: arr[0]
            }
          })
      });
    this.store.pipe(select(state => state.mainState.filialsState.dasgboardFilialState.item))
      .subscribe(filial => {
        if (filial != null)
          this.filial = filial;
      });
  }

  ngOnInit() {
  }

}
