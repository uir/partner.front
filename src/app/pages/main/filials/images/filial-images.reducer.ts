import {RequestActions, RequestSubmitActions,} from '../../../../shared/core/ngrx/reducer/responce.reducer';
import {RequestListState} from 'app/shared/core/ngrx/reducer/responce.reducer';
import {StateBase} from 'app/shared/core/ngrx/reducer/base.state';

export const imageActionTypes = new RequestActions("FILIAL_IMAGES");
export const imageMultipleLoadActionTypes = new RequestSubmitActions("FILIAL_IMAGES_MULTIPLE_LOAD");
export const imagesState =
  {
    imagesState: StateBase.create(imageActionTypes, RequestListState.getInitial, RequestListState.getReducer),
    imagesMultipleLoadState: StateBase.createForSubmit(imageMultipleLoadActionTypes)
  };
