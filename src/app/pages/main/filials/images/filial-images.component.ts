import {Component} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Image} from "app/pages/main/filials/images/filial-image.model";
import {LoadAction} from "app/shared/core/ngrx/action/base.action";
import {select, Store} from '@ngrx/store';
import {imageActionTypes} from './filial-images.reducer';
import {getCompanyId} from "../../admin-layout.reducer";
import {Observable} from "rxjs";
import {filter, map} from "rxjs/operators";

@Component({
  selector: 'app-filial-images',
  templateUrl: './filial-images.component.html',
  styleUrls: ['./filial-images.component.css']
})
export class FilialImagesComponent {
  public images$: Observable<Image[]>;
  public companyId: number;

  constructor(private store: Store<any>, private router: ActivatedRoute) {
    this.router.params
      .pipe(map(params => +params['id']))
      .subscribe(id => {
        if (id != null)
          this.init(id);
      });
    this.images$ = this.store.pipe(
      select(state => state.mainState.filialsState.imagesState.imagesState.items)
    );
  }

  private init(id) {
    this.store.dispatch(new LoadAction(imageActionTypes.get, id));

    this.store.pipe(select(getCompanyId)).subscribe(() => this.companyId = id);

    this.store
      .pipe(
        select(state => state.mainState.filialsState.imagesState.imagesState.itemsDelete.isSuccess),
        filter(isSuccess => isSuccess)
      )
      .subscribe(() => this.store.dispatch(new LoadAction(imageActionTypes.get, id)))
  }
}
