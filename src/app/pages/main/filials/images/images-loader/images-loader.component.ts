import {Component} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {LoadAction} from "../../../../../shared/core/ngrx/action/base.action";
import {FileUploader} from "ng2-file-upload";
import {imageActionTypes} from "../filial-images.reducer";
import {select, Store} from "@ngrx/store";
import {environment} from "../../../../../../environments/environment";
import {UserPaymentPlanService} from "../../../../../services/user-payment-plan.service";
import {FileItem} from "ng2-file-upload/file-upload/file-item.class";
import {Error} from "../../../../../shared/core/model/error.model";
import {notifyActions} from "../../../../../shared/core/modules/notification/notification.reducer";
import {filter, map} from "rxjs/operators";
import {AuthService} from "../../../../../../../projects/uir-core/src/lib/services/auth.service";

@Component({
  selector: 'app-images-loader',
  templateUrl: './images-loader.component.html',
  styleUrls: ['./images-loader.component.css']
})
export class ImagesLoaderComponent {

  public uploader: FileUploader;
  public hasBaseDropZoneOver = false;
  public hasAnotherDropZoneOver = false;
  public filialId: number;
  public imagesCount: number = 0;
  private token: string;

  constructor(private store: Store<any>,
              private auth: AuthService,
              private router: ActivatedRoute,
              private userService: UserPaymentPlanService) {
    this.token = '';
    this.router.params
      .pipe(map(params => +params['id']))
      .subscribe(id => {
        if (id != null)
          this.init(id);
      });
    this.userService
      .getImagesAllowCount()
      .subscribe(value => this.imagesCount = value);
  }

  private init(id) {
    this.filialId = id;
    this.uploader = new FileUploader(
      {
        isHTML5: true,
        url: environment.api + "api/places/" + id + "/images/upload",
        allowedMimeType: ['image/png', 'image/jpeg'],
      }
    );
    this.uploader.onErrorItem = (tem: FileItem, response: string, status: number) => {
      let error = Error.parse(response);
      this.store.dispatch({type: notifyActions.notify, payload: error.message})
    };
    this.uploader.onCompleteAll = () => {
      this.store.dispatch(new LoadAction(imageActionTypes.get, id));
      this.uploader.clearQueue();
    };

    this.uploader.options.headers = [
      {
        name: 'Authorization',
        value: 'bearer ' + this.auth.accessToken
      }
    ]

    this.store
      .pipe(
        select(state => state.mainState.filialsState.imagesState.imagesState.itemsDelete.isSuccess),
        filter(isSuccess => isSuccess)
      )
      .subscribe(() => this.store.dispatch(new LoadAction(imageActionTypes.get, id)))
  }

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  public fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
  }
}
