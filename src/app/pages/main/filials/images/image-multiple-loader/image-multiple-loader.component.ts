import {Component, Input, OnInit} from '@angular/core';
import {FormControl} from "@angular/forms";
import {select, Store} from "@ngrx/store";
import {LoadAction} from "../../../../../shared/core/ngrx/action/base.action";
import {imageActionTypes, imageMultipleLoadActionTypes} from "../filial-images.reducer";
import {filter} from "rxjs/operators";

@Component({
  selector: 'app-image-multiple-loader',
  templateUrl: './image-multiple-loader.component.html',
  styleUrls: ['./image-multiple-loader.component.css']
})
export class ImageMultipleLoaderComponent implements OnInit {

  @Input() filialId: number;
  public isShowBody = false;
  public links: string[];
  public input = new FormControl();

  constructor(private store: Store<any>) {
    this.links = [];
    this.store
      .pipe(
        select(state => state.mainState.filialsState.imagesState.imagesMultipleLoadState.itemSave.isSuccess),
        filter(isSuccess => isSuccess)
      )
      .subscribe(() => {
        this.store.dispatch(new LoadAction(imageActionTypes.get, this.filialId));
        this.links = [];
      });
  }

  ngOnInit() {
  }

  addLink() {
    this.links.push(this.input.value);
    this.input.reset();
  }

  remove(link: string) {
    this.links = this.links.filter(value => value != link);
  }

  load() {
    let data = {
      filialId: this.filialId,
      images: this.links
    };
    this.store.dispatch(new LoadAction(imageMultipleLoadActionTypes.submit, data))
  }

}
