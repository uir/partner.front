import {Component, Input, OnInit} from '@angular/core';
import {Image} from "../filial-image.model";
import {getActionTypeLoad} from "../../../../../shared/core/ngrx/action/base.action";
import {imageActionTypes} from "../filial-images.reducer";
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";

declare var $: any;
declare var swal: any;

@Component({
  selector: 'app-images-list',
  templateUrl: './images-list.component.html',
  styleUrls: ['./images-list.component.css']
})
export class ImagesListComponent implements OnInit {

  @Input() images$: Observable<Image[]>;
  @Input() companyId: number;
  public imageOpen: number = null;

  constructor(private store: Store<any>) { }

  ngOnInit() {
  }

  public openImage(img){
    this.imageOpen = img
  }

  delete(id) {
    let me = this;
    swal({
      title: 'Вы уверены?',
      text: 'Вы точно хотите безвозвратно удалить данное изображение?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Да, удалить его!',
      cancelButtonText: 'Нет',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(function () {
      me.store.dispatch({
        type: getActionTypeLoad(imageActionTypes.delete),
        payload: {id: me.companyId, imageId: id}
      });
      me.imageOpen = null
    });
  }

}
