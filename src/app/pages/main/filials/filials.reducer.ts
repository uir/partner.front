import { createFilialState } from './create/filial-create.reducer';
import { dasgboardFilialState } from "app/pages/main/filials/dashboard/filial-dashboard.component";
import { editFilialState } from "app/pages/main/filials/edit/filial-edit.component";
import { imagesState } from './images/filial-images.reducer';
import { videosState } from './videos/filial-videos.reducer';

export const filialsState = {
    createFilialState: createFilialState,
    editFilialState: editFilialState,
    dasgboardFilialState: dasgboardFilialState,
    imagesState: imagesState,
    videosState: videosState
}
