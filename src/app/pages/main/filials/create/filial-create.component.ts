import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FilialCreateForm} from './filial-create.form';
import {LoadAction} from "app/shared/core/ngrx/action/base.action";
import {Store} from '@ngrx/store';
import {createFilialActionTypes} from './filial-create.reducer';
import {getCompanyId} from '../../admin-layout.reducer';
import {FilialsService} from "../filials.service";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";

export interface FilialCreateComponentData {
  title: string;
  action: string;
  type: FilialCreateComponentType;
}

export enum FilialCreateComponentType {
  Create, Copy
}

@Component({
  selector: 'app-filial-create',
  templateUrl: './filial-create.component.html',
  styleUrls: ['./filial-create.component.css']
})
export class FilialCreateComponent implements OnInit {

  private companyId: number;
  private placeId: number;

  public data: FilialCreateComponentData;
  public mapHidden: boolean = true;
  public position: {lat: number, lng: number};
  public marker: {lat: number, lng: number};

  constructor(
    public form: FilialCreateForm,
    private store: Store<any>,
    private filialsService: FilialsService,
    private router: Router,
    private route: ActivatedRoute,
    protected http: HttpClient) {
    this.route.params.subscribe(x => this.placeId = +x.id);
    this.route.data.subscribe(x => this.data = x as FilialCreateComponentData);

    this.store.select(getCompanyId)
      .subscribe(id => {
        this.companyId = id;
      });

    this.form.form.valueChanges.subscribe(x => {
      if(x.lat && x.lng){
        this.position = {
          lat: x.lat,
          lng: x.lng
        }
      }
    })
  }

  public clickMap(e){
    this.form.setFormData({lat: e.coords.lat, lng: e.coords.lng});
    this.marker = {
      lat: e.coords.lat, lng: e.coords.lng
    };
    // this.getByGeo({
    //   latlng : `${e.coords.lat}, ${e.coords.lng}`,
    //   language: 'ru',
    //   key: 'AIzaSyD1ob1Kwk7Q8UNxfaNsTR0SJcX8AkSa2ls',
    //   sensor: 'false'
    // }).subscribe(x => {
    //   console.log(x)
    // })
  }

  getByGeo(spec: { [param: string]: string | string[]; }): Observable<any[]> {
    return this.http.get<any[]>("https://maps.googleapis.com/maps/api/geocode/json", {
      params: spec
    });
  }

  setHiddenMap(){
    let value = this.form.getValue();
    if(!value.lng || !value.lat){
      window.navigator.geolocation.getCurrentPosition((x) => {
        this.position = {
          lat: x.coords.latitude,
          lng: x.coords.longitude
        }
      })
    }
    else {
      this.position = {
        lat: value.lat,
        lng: value.lng
      };

      this.marker = {
        lat: value.lat, lng: value.lng
      };
    }
    this.mapHidden = !this.mapHidden;
  }

  public action() {
    switch (this.data.type) {
      case FilialCreateComponentType.Create:
        this.create();
        break;
      case FilialCreateComponentType.Copy:
        this.copy();
        break
    }
  }

  private create() {
    if (this.form.isDisabled()) {
      let filial = this.getFormValue();
      this.store.dispatch(new LoadAction(createFilialActionTypes.submit, filial));
      this.form.clear()
    }
  }

  private copy() {
    if (this.form.isDisabled()) {
      let filial = this.getFormValue();
      this.filialsService
        .copy(this.companyId, this.placeId, filial)
        .subscribe(async () => {
          this.router.navigate(['companies', this.companyId]);
          this.form.clear();
        });
    }
  }

  private getFormValue() {
    let filial = this.form.getValue();
    filial.organizationId = this.companyId;
    if (filial.roomType == 0) {
      filial.roomType = null;
      filial.room = null
    }
    return filial;
  }

  ngOnInit() {
    this.form.buildForm();
  }
}
