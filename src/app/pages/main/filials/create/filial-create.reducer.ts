import { RequestSubmitActions, RequestState } from "app/shared/core/ngrx/reducer/responce.reducer";

export const createFilialActionTypes: RequestSubmitActions = new RequestSubmitActions('FILIAL_CREATE');

export const createFilialState = {
    filialState: {
        actions: createFilialActionTypes,
        initialState: RequestState.getInitialSave(),
        reducer: (s, a): any => {
            return RequestState.getReducer(s, a, createFilialActionTypes)
        }
    }
}
