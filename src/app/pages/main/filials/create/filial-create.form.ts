import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BaseForm} from "app/shared/core/form/base.form";
import {Injectable} from '@angular/core';
import {Address} from "../../../../shared/google-place-autocomplete/model/address.model";
import {FilialCreate} from "../../../../shared/filial/models/filial-create.model";

@Injectable()
export class FilialCreateForm extends BaseForm<FilialCreate> {
  public roomTypes = [
    {value: 0, name: 'Не выбрано'},
    {value: 1, name: 'Офис'},
    {value: 2, name: 'Квартира'}
  ]

  protected getForm(): FormGroup {
    return this.fb.group({
      name: [this.data.name, [Validators.required, Validators.pattern("^.{1,40}$")]],
      lat: [this.data.lat, [Validators.required, Validators.pattern("^[-]?\\d*[.,]?\\d+$")]],
      lng: [this.data.lng, [Validators.required, Validators.pattern("^[-]?\\d*[.,]?\\d+$")]],
      phone: [this.data.phone, [Validators.pattern("^(\\d{7,11}){1}((,|\\s,\\s|\\s,|,\\s){1}(\\d{7,11}))*$")]],
      website: [this.data.website, [Validators.pattern("^(https?:\\/\\/)?([\\d,a-z\\.\\-]+)\\.([a-zA-Z\\.]{2,6})([\\/\\w \\.-]*)*\\/?$")]],
      address: [this.data.address, [Validators.required, Validators.pattern("^.{0,100}$")]],
      priceLevel: [this.data.priceLevel],
      isActive: [this.data.isActive],
      roomType: [this.data.roomType],
      room: [this.data.room],
    });
  }

  protected getValidationMessages() {
    return {
      name: {required: this.required, pattern: "Поле не должно превышать 40 символов"},
      lat: {required: this.required, pattern: "Не правильный формат"},
      lng: {required: this.required, pattern: "Не правильный формат"},
      phone: {pattern: "Номера должны состоять из 7-11 цифр без символов и разделяться запятыми"},
      website: {pattern: "Пример поля: UirWebSite.ru"},
      address: {required: this.required, pattern: "Значение должно состоять из 100 цифр"},
      roomType: {},
      room: {},
    };
  }

  protected getLables() {
    return {
      name: 'Название',
      phone: 'Телефон',
      website: "Web сайт",
      address: "Адрес",
      priceLevel: "Уровень цен",
      isActive: "Активность",
      lat: "Долгота",
      lng: 'Широта',
      roomType: 'Тип помещения',
    };
  }

  setAddressPlace(address: Address) {
    let lat = null;
    let lng = null;
    let error = '';
    if (address.street == null && address.street == "") {
      error = 'Укажите точный адрес'
    } else {
      lat = address.lat;
      lng = address.lng;
    }
    this.setFormData({...this.getValue(), lat: lat, lng: lng, address: address.formattedAddress});
    this.formErrors.address = error
  }

  getRoomType() {
    return this.getValueData('roomType')
  }

  getTypeName() {
    const roomType = this.roomTypes.find(x => x.value == this.getRoomType());
    return roomType ? roomType.name : null;
  }

  checkType(e) {
    this.setFormData({...this.getValue(), roomType: e.value})
  }

  isInput(f) {
    return (f != 'isActive' && f != 'address' && f != 'roomType' && f != 'room') ||
      (f == 'room' && this.getRoomType() !== 0)
  }

  public isError(field: any): boolean {
    if (field == 'room') return this.getRoomType() != 0 && this.getValueData('room') == ''
    return super.isError(field)
  }

  public getError(field): string {
    if (field == 'room')
      return this.isError(field) ? 'Обязательно для заполнения' : null
    return super.getError(field);
  }

  isDisabled() {
    return this.isValid() && !this.isError('room')
  }

  public getlabel(f) {
    if (f == 'room') {
      switch (this.getRoomType()) {
        case 0: {
          return null
        }
        case 1: {
          return 'Номер офиса'
        }
        case 2: {
          return 'Номер квартиры'
        }
      }
    }
    return super.getlabel(f);
  }

  constructor(fb: FormBuilder) {
    super({
      name: '',
      phone: '',
      website: "",
      address: "",
      priceLevel: 0,
      lat: null,
      lng: null,
      isActive: true,
      roomType: 0,
      room: ''
    }, fb);

    this.nonVisibleKeys = ['priceLevel'];
    this.validationMessages = this.getValidationMessages();
    this.form = this.getForm();
    this.lables = this.getLables();
  }
}
