import {Actions} from '@ngrx/effects';
import {Effect} from '@ngrx/effects';
import {FilialsService} from './filials.service';
import {Injectable} from "@angular/core";
import {Router} from '@angular/router';
import {createFilialActionTypes} from './create/filial-create.reducer';
import {dashboardFilialActionTypes} from "app/pages/main/filials/dashboard/filial-dashboard.component";
import {editFilialActionType} from './edit/filial-edit.component';
import {getRequestEffectWithData} from "app/shared/core/ngrx/effect/responce.effects";
import {imageActionTypes, imageMultipleLoadActionTypes} from './images/filial-images.reducer';
import {videosActionTypes} from './videos/filial-videos.reducer';

@Injectable()
export class FilialsEffects {

  @Effect()
  filialCreate$ = getRequestEffectWithData(this.actions$, createFilialActionTypes.submit, this.filialService.create)

  @Effect()
  filialGet$ = getRequestEffectWithData(this.actions$, editFilialActionType.get, this.filialService.get)

  @Effect()
  filialDashboard$ = getRequestEffectWithData(this.actions$, dashboardFilialActionTypes.get, this.filialService.get)

  @Effect()
  filialEdit$ = getRequestEffectWithData(this.actions$, editFilialActionType.submit, this.filialService.edit)

  @Effect()
  filialImages$ = getRequestEffectWithData(this.actions$, imageActionTypes.get, this.filialService.images)

  @Effect()
  imagesMultipleLoad$ = getRequestEffectWithData(this.actions$, imageMultipleLoadActionTypes.submit, this.filialService.imagesLoad)

  @Effect()
  imagesDelete$ = getRequestEffectWithData(this.actions$, imageActionTypes.delete, this.filialService.imageDelete)

  @Effect()
  filialVideos$ = getRequestEffectWithData(this.actions$, videosActionTypes.get, this.filialService.videos)

  @Effect()
  videoGet$ = getRequestEffectWithData(this.actions$, videosActionTypes.getItem, this.filialService.videoGet)

  @Effect()
  videoSave$ = getRequestEffectWithData(this.actions$, videosActionTypes.submit, this.filialService.videoSave)

  @Effect()
  videoDelete$ = getRequestEffectWithData(this.actions$, videosActionTypes.delete, this.filialService.videoDelete)

  constructor(private actions$: Actions, private filialService: FilialsService, private router: Router) {
  }
}
