import {DashboardFilial} from '../dashboard/dashboard-filials/dashboard-filials.model';
import {FilialEdit} from "app/pages/main/filials/edit/filial-edit.model";
import {Image} from "app/pages/main/filials/images/filial-image.model";
import {Injectable} from '@angular/core';
import {Video} from "app/pages/main/filials/videos/filial-video.model";
import {VideoEdit} from './videos/edit/video-edit.model';
import {environment} from "environments/environment";
import {Sale} from "../../../shared/sales/sale.model";
import {Observable} from "rxjs";
import {HttpService} from "../../../../../projects/uir-core/src/lib/services/http.service";

@Injectable({providedIn: "root"})
export class FilialsService {

  constructor(private http: HttpService) {
  }

  private image = environment.api + 'api/places/$id/images/$imageId';

  private imageMultipleLoad = environment.api + 'api/places/$id/images/multiple_upload';

  private filial = (id): string => {
    return environment.api + 'api/companies/' + id + '/places';
  }

  private filialGet = (companyId, filialId): string => {
    return environment.api + 'api/companies/' + companyId + '/places/' + filialId;
  }

  private filialEdit = (companyId, filialId): string => {
    return environment.api + 'api/companies/' + companyId + '/places/' + filialId;
  }

  private filialImages = (filialId): string => {
    return environment.apiPublic + 'api/places/' + filialId + '/images';
  }

  private filialVideos = (filialId): string => {
    return environment.apiPublic + 'api/places/' + filialId + '/videos';
  }

  private filialVideoEdit = (filialId, videoId: string): string => {
    return environment.api + 'api/places/' + filialId + '/videos/' + videoId;
  }

  private video = (id): string => {
    return environment.api + 'api/video/' + id;
  }

  private videoDeleteUrl = (filialId, id): string => {
    return environment.api + 'api/places/' + filialId + '/videos/' + id;
  }

  private filialSales = (filialId): string => {
    return environment.api + 'api/places/' + filialId + '/sales';
  }

  create = (filial): Observable<any> => {
    let url = this.filial(filial.organizationId);
    return this.http.POST<any>(url, filial);
  }

  get = (data): Observable<FilialEdit> => {
    let url = this.filialGet(data.companyId, data.filialId);
    return this.http.GET<FilialEdit>(url);
  }

  edit = (data): Observable<any> => {
    let url = this.filialEdit(data.infoBaseId, data.id);
    return this.http.POST<any>(url, data);
  }

  dashboard = (data): Observable<DashboardFilial> => {
    let url = this.filialGet(data.companyId, data.filialId);
    return this.http.GET<DashboardFilial>(url);
  }

  images = (filialId): Observable<Image[]> => {
    let url = this.filialImages(filialId);
    return this.http.GET<Image[]>(url);
  }

  imagesLoad = (data): Observable<any> => {
    let url = this.imageMultipleLoad.replace("$id", data.filialId);
    return this.http.POST(url, data.images);
  }

  imageDelete = (data): Observable<any> => {
    let url = this.image.replace('$id', data.id).replace("$imageId", data.imageId);
    return this.http.DELETE<any>(url, url);
  }

  videos = (filialId): Observable<Video[]> => {
    let url = this.filialVideos(filialId);
    return this.http.GET<Video[]>(url);
  }

  videoGet = (id): Observable<VideoEdit> => {
    let url = environment.apiPublic + 'api/video/' + id + '/info';
    return this.http.GET<VideoEdit>(url);
  }

  videoSave = (data): Observable<any> => {
    let url = this.filialVideoEdit(data.filialId, data.id);
    return this.http.POST<any>(url, data);
  }

  videoDelete = (data: { filialId: number, id: number }): Observable<any> => {
    let url = this.videoDeleteUrl(data.filialId, data.id);
    return this.http.DELETE<any>(url, data.id);
  }

  salesGet = (id): Observable<Sale> => {
    let url = this.filialSales(id);
    return this.http.GET<Sale>(url);
  }

  salesCreate = (data): Observable<any> => {
    let url = this.filialSales(data.filialId);
    return this.http.POST<any>(url, data);
  }

  salesEdit = (data): Observable<any> => {
    let url = this.filialSales(data.filialId) + '/' + data.id;
    return this.http.POST<any>(url, data);
  }

  salesDelete = (data): Observable<any> => {
    let url = this.filialSales(data.filialId) + '/' + data.saleId;
    return this.http.DELETE<any>(url);
  }

  public copy(companyId: number, placeId: number, filial): Observable<any> {
    const api = environment.api + `api/companies/${companyId}/places/${placeId}/copy`;
    return this.http.POST<any>(api, filial);
  }
}

