import {Component} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {map} from "rxjs/operators";

@Component({
  selector: 'app-filial-sales',
  template: `
    <div class="main-content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div>
              <div class="card-header card-header-text" data-background-color="rose">
                <h4 class="card-title">{{title}}</h4>
              </div>
              <div>
                <app-sales *ngIf="filialId" [owner]="1" [ownerId]="filialId"></app-sales>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  `
})
export class FilialSalesComponent {

  public filialId: number;
  public title = "Скидки филиала";

  constructor(private router: ActivatedRoute) {
    this.router.params
      .pipe(map(params => +params['filialId']))
      .subscribe(id => {
        if (id != null)
          this.filialId = id;
      });
  }
}
