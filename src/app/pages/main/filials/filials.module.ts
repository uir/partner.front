import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppRoutes} from "app/app.routing";
import {CommonModule} from '@angular/common';
import {EffectsModule} from '@ngrx/effects';
import {FileUploadModule} from 'ng2-file-upload';
import {FilialCreateComponent} from './create/filial-create.component';
import {FilialCreateForm} from './create/filial-create.form';
import {FilialDashboardComponent} from './dashboard/filial-dashboard.component';
import {FilialEditComponent} from './edit/filial-edit.component';
import {FilialEditForm} from "app/pages/main/filials/edit/filial-edit.form";
import {FilialImagesComponent} from './images/filial-images.component';
import {FilialSalesComponent} from './sales/filial-sales.component';
import {FilialVideosComponent} from './videos/filial-videos.component';
import {FilialsEffects} from './filials.effects';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {VgBufferingModule} from 'videogular2/buffering';
import {VgControlsModule} from 'videogular2/controls';
import {VgCoreModule} from 'videogular2/core';
import {VgOverlayPlayModule} from 'videogular2/overlay-play';
import {VideoEditComponent} from './videos/edit/video-edit.component';
import {VideoEditForm} from './videos/edit/video-edit.form';
import {Ng2DropdownModule} from "ng2-material-dropdown";
import {ImageMultipleLoaderComponent} from "./images/image-multiple-loader/image-multiple-loader.component";
import {HttpClientModule} from "@angular/common/http";
import {GooglePlaceAutocompleteModule} from "../../../shared/google-place-autocomplete/google-place-autocomplete.module";
import {ImagesLoaderComponent} from './images/images-loader/images-loader.component';
import {ImagesListComponent} from './images/images-list/images-list.component';
import {VideosLoaderComponent} from './videos/videos-loader/videos-loader.component';
import {VideosListComponent} from './videos/videos-list/videos-list.component';
import {QuillEditorModule} from 'ngx-quill-editor';
import {SalesModule} from "../../../shared/sales/sales.module";
import {AppMapModule} from "../../../shared/core/components/map/app-map.module";
import {MatSlideToggleModule} from "@angular/material";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    EffectsModule.forFeature([FilialsEffects]),
    RouterModule,
    FileUploadModule,
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule,
    QuillEditorModule,
    Ng2DropdownModule,
    HttpClientModule,
    GooglePlaceAutocompleteModule,
    SalesModule,
    AppMapModule,
    MatSlideToggleModule
  ],
  declarations: [
    FilialCreateComponent,
    FilialEditComponent,
    FilialDashboardComponent,
    FilialImagesComponent,
    FilialVideosComponent,
    VideoEditComponent,
    FilialSalesComponent,
    ImageMultipleLoaderComponent,
    ImagesLoaderComponent,
    ImagesListComponent,
    VideosLoaderComponent,
    VideosListComponent
  ],
  providers: [
    FilialsEffects,
    FilialCreateForm,
    FilialEditForm,
    VideoEditForm
  ],
})
export class FilialsModule {
}
