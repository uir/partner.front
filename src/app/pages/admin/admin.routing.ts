import {Routes} from "@angular/router";
import {AdminPageComponent} from "./components/admin-page/admin-page.component";
import {RegisterStepperComponent} from "./components/register-stepper/register-stepper.component";
import {OrganizationsListComponent} from "./components/organizations-list/organizations-list.component";
import {ManagerListComponent} from "./components/manager-list/manager-list.component";
import {LoginComponent} from "./components/login/login.component";
import {AuthTokenGuard} from "../../../../projects/uir-core/src/lib/guards/auth-token.guard";
import {OverviewsListComponent} from "./components/overviews-list/overviews-list.component";

export var AdminRoutes: Routes = [
  {
    path: '',
    redirectTo: 'organizations',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '',
    component: AdminPageComponent,
    canActivateChild: [AuthTokenGuard],
    children: [
      {
        path: 'register',
        component: RegisterStepperComponent
      },
      {
        path: 'organizations',
        component: OrganizationsListComponent,
        data: {
          title: "Организации"
        }
      },
      {
        path: 'managers',
        component: ManagerListComponent,
        data: {
          title: "Менаджеры"
        }
      },
      {
        path: 'overviews',
        component: OverviewsListComponent,
        data: {
          title: "Обзоры"
        }
      }
    ]
  }
];
