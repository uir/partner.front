export class OrganizationList {
  id: number;
  name: string;
  address: string;
  fio: string;
}
