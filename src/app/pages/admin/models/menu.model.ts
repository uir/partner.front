export enum MenuType {
  orgs = 'orgs',
  managers = 'managers',
  overviews = 'overviews',
  orgCreate = 'orgCreate'
}

export interface Menu {
  menuType: MenuType,
  link: string;
  name: string;
}
