import {NgModule} from '@angular/core';
import {AdminPageComponent} from './components/admin-page/admin-page.component';
import {RouterModule} from "@angular/router";
import {AdminRoutes} from "./admin.routing";
import {
  MatButtonModule,
  MatCardModule,
  MatIconModule,
  MatListModule,
  MatStepperModule,
  MatToolbarModule
} from "@angular/material";
import {OrganizationModule} from "../../shared/organization/organization.module";
import {CategoriesCheckerModule} from "../../shared/categories-checker/categories-checker.module";
import {FilialModule} from "../../shared/filial/filial.module";
import {SalesModule} from "../../shared/sales/sales.module";
import {OrganizationsListComponent} from "./components/organizations-list/organizations-list.component";
import {SmartTableModule} from "../../../../projects/smart-table/src/lib/smart-table.module";
import {CommonModule} from "@angular/common";
import {RegisterStepperComponent} from "./components/register-stepper/register-stepper.component";
import {LoginComponent} from "./components/login/login.component";
import {SharedFormlyModule} from "../../../../projects/shared-formly/src/lib/uir-formly.module";
import {ManagerListComponent} from './components/manager-list/manager-list.component';
import {UirCoreModule} from "../../../../projects/uir-core/src/lib/uir-core.module";
import {UserService} from "./services/user.service";
import {AuthTokenGuard} from "../../../../projects/uir-core/src/lib/guards/auth-token.guard";
import {AdminOrganizationService} from "./services/organization-admin.service";
import {LOGIN_URL} from "../../../../projects/uir-core/src/lib/services/auth.service";
import {MenuService} from "./services/menu.service";
import {OverviewsListComponent} from './components/overviews-list/overviews-list.component';
import {LoaderModule} from "../../../../projects/loader/src/lib/loader.module";

const options = {
  storageKey: 'uir-admin-partner-auth-tokens'
};

@NgModule({
  imports: [
    CommonModule,
    SmartTableModule,
    UirCoreModule.forRoot(options),
    OrganizationModule,
    CategoriesCheckerModule,
    SharedFormlyModule.forRoot(),
    FilialModule,
    SalesModule,
    RouterModule.forChild(AdminRoutes),
    MatStepperModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    LoaderModule.forRoot()
  ],
  declarations: [
    AdminPageComponent,
    RegisterStepperComponent,
    OrganizationsListComponent,
    ManagerListComponent,
    LoginComponent,
    OverviewsListComponent
  ],
  providers: [
    MenuService,
    AuthTokenGuard,
    UserService,
    AdminOrganizationService,
    {provide: LOGIN_URL, useValue: '/admin/login'}
  ]
})
export class AdminModule {
}
