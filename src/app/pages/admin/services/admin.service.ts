import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {environment} from "../../../../environments/environment";
import {HttpService} from "../../../../../projects/uir-core/src/lib/services/http.service";

@Injectable()
export class AdminService {

  private _setCategoriesApi = environment.api + "api/companies/$id/categories";

  constructor(private http: HttpService) {
  }

  setCategories(orgId: number, ids: number[]): Observable<any> {
    return this.http.PATCH<any>(this._setCategoriesApi.replace('$id', orgId.toString()), ids);
  }
}
