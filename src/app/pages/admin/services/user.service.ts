import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {FakeUser} from "../models/fake-user.model";
import {environment} from "../../../../environments/environment";
import {HttpService} from "../../../../../projects/uir-core/src/lib/services/http.service";

@Injectable()
export class UserService {

  private _createUserApi = environment.api + "api/admin/user/fake";

  constructor(private http: HttpService) {
  }

  create(): Observable<FakeUser> {
    return this.http.POST<FakeUser>(this._createUserApi);
  }
}
