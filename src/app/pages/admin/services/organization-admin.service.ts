import {Inject, Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {environment} from "../../../../environments/environment";
import {
  AUTH_STORAGE_KEY,
  AuthService,
  retrieveTokens
} from "../../../../../projects/uir-core/src/lib/services/auth.service";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {AuthTokenModel} from "../../../../../projects/uir-core/src/lib/model/auth/auth-tokens-model";
import {tap} from "rxjs/operators";
import {uirPartnerStorageKey} from "../../../app.module";

@Injectable({
  providedIn: 'root'
})
export class AdminOrganizationService {

  loginApi = 'api/admin/organizations/login';
  private auth: AuthService;

  constructor(private http: HttpClient, @Inject(AUTH_STORAGE_KEY) private storageKey: string) {
  }

  login(id: number): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded');
    headers = headers.set('Authorization', 'bearer ' + retrieveTokens(this.storageKey).access_token);

    let data = {
      username: id.toString(),
      password: id.toString()
    };
    let reqData = {...data, ...{grant_type: 'password', scope: 'openid offline_access'}};
    let params = new HttpParams();
    Object.keys(reqData).forEach(key => params = params.append(key, reqData[key]));
    return this.http
      .post<AuthTokenModel>(environment.api + this.loginApi, params.toString(), {headers: headers})
      .pipe(
        tap(res => {
          const tokens: AuthTokenModel = res;
          const now = new Date();
          tokens.expiration_date = new Date(now.getTime() + tokens.expires_in * 1000).getTime().toString();
          AuthService.storeToken(tokens, uirPartnerStorageKey);
          this.auth = new AuthService(this.http, uirPartnerStorageKey);
        }),
      );
  }
}
