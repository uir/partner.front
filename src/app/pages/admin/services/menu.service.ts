import {Injectable} from '@angular/core';
import {Menu, MenuType} from "../models/menu.model";
import {BehaviorSubject, Observable} from "rxjs";
import {AuthService} from "../../../../../projects/uir-core/src/lib/services/auth.service";

@Injectable()
export class MenuService {

  private default: Menu[] = [
    {
      menuType: MenuType.orgs,
      link: 'organizations',
      name: 'Организации'
    },
    {
      menuType: MenuType.managers,
      link: 'managers',
      name: 'Менаджеры'
    },
    {
      menuType: MenuType.overviews,
      link: 'overviews',
      name: 'Обзоры'
    }
  ];
  private _menus: BehaviorSubject<Menu[]> = new BehaviorSubject<Menu[]>([]);

  constructor(private auth: AuthService) {
    this.auth.info$.subscribe(i => this.hide(MenuService.adminSpec(i)))
  }

  public get menus$(): Observable<Menu[]> {
    return this._menus;
  }

  hide(...types: MenuType[]) {
    let newMenus = this.default.filter(x => !types.includes(x.menuType));
    this._menus.next(newMenus);
  }

  public static adminSpec = (info) => {
    if (info.scope !== "0")
      return MenuType.managers
  }
}
