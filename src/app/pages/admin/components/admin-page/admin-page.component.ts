import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../../../../../projects/uir-core/src/lib/services/auth.service";
import {MenuService} from "../../services/menu.service";

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.css']
})
export class AdminPageComponent implements OnInit {

  constructor(
    private auth: AuthService,
    private menu: MenuService) {
  }

  ngOnInit() {
  }

  logout() {
    this.auth.logout();
  }
}
