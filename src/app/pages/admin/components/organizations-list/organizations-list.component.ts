import {Component, OnInit, ViewChild} from '@angular/core';
import {OrganizationList} from "../../models/organization-list.model";
import {AdminOrganizationService} from "../../services/organization-admin.service";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog} from "@angular/material";
import {environment} from "../../../../../environments/environment";
import {Column} from "../../../../../../projects/smart-table/src/lib/models/column.model";
import {SmartTableOptions} from "../../../../../../projects/smart-table/src/lib/models/table-options.model";
import {RestApi} from "../../../../../../projects/smart-table/src/lib/services/rest-api";
import {EMPTY} from "rxjs";
import {Action, ActionContext} from "../../../../../../projects/smart-table/src/lib/models/action.model";

@Component({
  selector: 'app-organizations-list',
  templateUrl: './organizations-list.component.html',
  styleUrls: ['./organizations-list.component.css']
})
export class OrganizationsListComponent implements OnInit {
  @ViewChild('placesTable') placesTable;
  @ViewChild('statusColumn') statusColumn;
  @ViewChild('createTmp') createTmp;

  columns: Column<OrganizationList>[];
  options: SmartTableOptions;
  detailsForm = [
    {
      key: 'id',
      type: 'input',
      templateOptions: {
        label: 'id',
        required: true
      },
      hideExpression: true
    },
    {
      key: 'name',
      type: 'input',
      templateOptions: {
        disabled: true,
        label: 'Название',
        required: true
      },
    },
    {
      key: 'fio',
      type: 'input',
      templateOptions: {
        disabled: true,
        label: 'ФИО',
        required: true
      },
    },
    {
      key: 'address',
      type: 'input',
      templateOptions: {
        disabled: true,
        label: 'Адрес',
        required: true
      },
    },
    {
      key: 'login',
      type: 'input',
      templateOptions: {
        disabled: true,
        label: 'Логин',
        required: true
      },
    },
  ];

  constructor(
    private route: ActivatedRoute,
    private orgAdminService: AdminOrganizationService,
    private router: Router,
    private dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.columns = [
      {
        name: '№',
        maxWidth: 80,
        key: 'id',
      },
      {
        name: 'Статус',
        maxWidth: 50,
        template: this.statusColumn
      },
      {
        name: 'Менаджер',
        key: 'manager'
      },
      {
        name: 'Название',
        key: 'name',
      },
      {
        name: 'Адрес',
        key: 'address',
      },
      {
        name: 'ФИО',
        key: 'fio',
      }
    ];
    this.options = {
      rest: new RestApi(environment.api, 'api/admin', 'organizations'),
      list: {
        filterOptions: {
          desc: true,
          orderByStr: 'id'
        }
      },
      create: {
        dialogOptions: {
          form: [],
          title: () => "Регистрация новой организации",
          afterFormTemplate: this.createTmp,
        },
        actionOptions: {
          buttonText: "Создать новую организацию"
        }
      },
      details: {
        dialogOptions: {
          form: this.detailsForm,
          title: (value) => "Организация №" + value.id,
          afterFormTemplate: this.placesTable
        }
      },
      edit: {
        actionOptions: {
          action: new Action((context: ActionContext) => {
            this.login(context.value.id);
            return EMPTY;
          }),
          icon: 'edit'
        }
      }
    }
  }

  login(id: number) {
    this.orgAdminService
      .login(id)
      .subscribe(() => {
        this.router.navigate(['companies', id]);
        this.dialog.closeAll();
      })
  }

  public getlink(data) {
    if (data.isDraft)
      return environment.partnerSite + 'user/register/' + data.login;
  }
}
