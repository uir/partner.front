import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {RestApi} from "../../../../../../projects/smart-table/src/lib/services/rest-api";
import {environment} from "../../../../../environments/environment";
import {SmartTableOptions} from "../../../../../../projects/smart-table/src/lib/models/table-options.model";
import {Column} from "../../../../../../projects/smart-table/src/lib/models/column.model";
import {OrganizationList} from "../../models/organization-list.model";
import {SmartTable} from "../../../../../../projects/smart-table/src/lib/table/smart-table";

@Component({
  selector: 'app-overviews-list',
  templateUrl: './overviews-list.component.html',
  styleUrls: ['./overviews-list.component.css']
})
export class OverviewsListComponent implements SmartTable, OnInit {

  @ViewChild('statusColumn') statusColumn;

  options: SmartTableOptions;
  columns: Column<OrganizationList>[];

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.options = {
      rest: new RestApi(environment.api, 'api/admin', 'overviews'),
      list: {
        filterOptions: {
          desc: true,
          orderByStr: 'id'
        }
      },
      edit: {
        dialogOptions: {
          title: value => `Редактирование обзора №${value.id} (${value.name})`,
          form: [
            {
              key: 'placeName',
              type: 'input',
              templateOptions: {
                disabled: true,
                label: 'Филиал',
                required: true
              },
            },
            {
              key: 'createDate',
              type: 'input',
              templateOptions: {
                disabled: true,
                label: 'Дата создания',
                required: true
              },
            },
            {
              key: 'isConfirmed',
              type: 'toggle',
              templateOptions: {
                label: 'Статус',
                required: true
              },
            },
            {
              key: 'plus',
              type: 'textarea',
              templateOptions: {
                disabled: true,
                label: 'Плюсы',
                required: true
              },
            },
            {
              key: 'minus',
              type: 'textarea',
              templateOptions: {
                disabled: true,
                label: 'Минусы',
                required: true
              },
            },
            {
              key: 'description',
              type: 'quill',
              templateOptions: {
                readOnly: true,
                label: 'Описание',
                quillOptions: {
                  placeholder: "Описание филиала",
                  modules: {
                    toolbar: []
                  }
                }
              },
            },
          ]
        }
      }
    };
    this.columns = [
      {
        name: '№',
        maxWidth: 80,
        key: 'id',
      },
      {
        name: 'Статус',
        maxWidth: 50,
        template: this.statusColumn
      },
      {
        name: 'Дата создания',
        key: 'createDate',
      },
      {
        name: 'Филиал',
        key: 'placeName',
      }
    ];
  }

}
