import {Component, OnInit} from '@angular/core';
import {Column} from "../../../../../../projects/smart-table/src/lib/models/column.model";
import {OrganizationList} from "../../models/organization-list.model";
import {SmartTableOptions} from "../../../../../../projects/smart-table/src/lib/models/table-options.model";
import {ActivatedRoute} from "@angular/router";
import {emailFiled, passwordField, phoneField} from "../../../../shared/core/form/formly/fromly.fields";
import {RestApi} from "../../../../../../projects/smart-table/src/lib/services/rest-api";
import {environment} from "../../../../../environments/environment";

@Component({
  selector: 'app-manager-list',
  templateUrl: './manager-list.component.html',
  styleUrls: ['./manager-list.component.css']
})
export class ManagerListComponent implements OnInit {

  columns: Column<OrganizationList>[] = [
    {
      name: '№',
      maxWidth: 80,
      key: 'id',
    },
    {
      name: 'Роль',
      maxWidth: 80,
    },
    {
      name: 'ФИО',
      key: 'name',
    },
    {
      name: 'E-mail',
      key: 'email',
    },
    {
      name: 'Телефон',
      key: 'phoneNumber',
    },
    {
      name: 'Кол-во организаций',
      key: 'orgCount',
    }
  ];
  createForm = [
    {
      key: 'fio',
      type: 'input',
      templateOptions: {
        label: 'ФИО',
        required: true
      },
    },
    emailFiled,
    {
      key: 'managerType',
      type: 'select',
      templateOptions: {
        label: 'Тип',
        required: true,
        options: [
          {value: 0, label: 'Админ'},
          {value: 1, label: 'Менаджер'}
        ],
      },
    },
    phoneField,
    {
      key: 'login',
      type: 'input',
      templateOptions: {
        label: 'Логин',
        required: true
      },
    },
    passwordField,

  ];
  options: SmartTableOptions;

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.options = {
      rest: new RestApi(environment.api, 'api/admin', 'managers'),
      list: {
        filterOptions: {
          desc: true,
          orderByStr: 'id'
        }
      },
      create: {
        dialogOptions: {
          title: () => "Создание нового менаджера",
          submitButtonText: "Добавить",
          form: this.createForm
        }
      },
      delete: {}
    }
  }
}
