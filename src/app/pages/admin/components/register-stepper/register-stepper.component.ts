import {Component, OnInit, ViewChild} from '@angular/core';
import {UserService} from "../../services/user.service";
import {AdminService} from "../../services/admin.service";
import {MatHorizontalStepper, MatStep} from "@angular/material";
import {FakeUser} from "../../models/fake-user.model";
import {environment} from "../../../../../environments/environment";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AUTH_STORAGE_KEY, AuthService} from "../../../../../../projects/uir-core/src/lib/services/auth.service";
import {HttpService} from "../../../../../../projects/uir-core/src/lib/services/http.service";
import {tap} from "rxjs/operators";

@Component({
  selector: 'app-register-stepper',
  templateUrl: './register-stepper.component.html',
  styleUrls: ['./register-stepper.component.css'],
  providers: [
    AdminService,
    AuthService,
    HttpService,
    {provide: AUTH_STORAGE_KEY, useValue: 'uir-partner-draft-auth-tokens'}
  ]
})
export class RegisterStepperComponent implements OnInit {

  @ViewChild("stepper") stepper: MatHorizontalStepper;
  @ViewChild("createOrgStep") _createOrgStep: MatStep;

  public fakeUser: FakeUser;

  createOrgStep: FormGroup = this.fb.group({orgId: [null, Validators.required]});
  setCategoriesStep: FormGroup = this.fb.group({categoriesIds: [null, Validators.required]});
  createFilialStep: FormGroup = this.fb.group({filialId: [null, Validators.required]});

  get orgId() {
    return this.createOrgStep.value.orgId;
  }

  get filialId() {
    return this.createFilialStep.value.filialId;
  }

  get categoriesIds() {
    return this.createOrgStep.value.categoriesIds;
  }


  public get link() {
    if (this.fakeUser)
      return environment.partnerSite + 'user/register/' + this.fakeUser.login;
  }

  constructor(
    private userService: UserService,
    private adminService: AdminService,
    private fb: FormBuilder,
    private auth: AuthService) {
    this.userService
      .create()
      .pipe(
        tap(x => this.auth.login({
          password: x.password,
          username: x.login
        }).subscribe())
      ).subscribe(x => this.fakeUser = x);
  }

  ngOnInit() {
  }

  onCategoryCheck(ids: number[]) {
    this.setCategoriesStep.setValue({categoriesIds: ids});
    this.adminService.setCategories(this.orgId, ids).subscribe();
  }

  onFilialCreate(filialId: number) {
    this.createFilialStep.setValue({filialId: filialId});
    this.stepper.next();
  }

  onOrgCreate(orgId: number) {
    this.createOrgStep.setValue({orgId: orgId});
    this.stepper.next();
  }

}
