import {Component} from '@angular/core';
import {FormComponent} from "../../../../shared/core/form/form-component";
import {AuthService} from "../../../../../../projects/uir-core/src/lib/services/auth.service";
import {FormlyFieldConfig} from "@ngx-formly/core";
import {passwordField} from "../../../../shared/core/form/formly/fromly.fields";
import {Router} from "@angular/router";

const fields = [
  {
    key: 'login',
    type: 'input',
    templateOptions: {
      label: 'Логин',
      required: true
    }
  },
  passwordField
];

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent extends FormComponent<{ login: string, password: string }> {

  get fields(): FormlyFieldConfig[] {
    return fields;
  }

  constructor(private auth: AuthService, private router: Router) {
    super();
  }

  submit() {
    this.auth
      .login({
        username: this.form.value.login,
        password: this.form.value.password,
      })
      .subscribe(() => {
        this.router.navigate(['admin']);
      });
  }
}
