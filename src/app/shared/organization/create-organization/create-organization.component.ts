import {Component, Input} from '@angular/core';
import {ActionFormComponent} from "../../core/form/form-component";
import {FormlyFieldConfig} from "@ngx-formly/core";
import {createOrganizationFields} from "../forms/create-org.form";
import {OrganizationService} from "../services/organization.service";

@Component({
  selector: 'app-create-organization',
  templateUrl: './create-organization.component.html',
  styleUrls: ['./create-organization.component.css']
})
export class CreateOrganizationComponent extends ActionFormComponent<any, OrganizationService> {

  @Input() ownerId: string;

  constructor(service: OrganizationService) {
    super(service);
  }

  get fields(): FormlyFieldConfig[] {
    return createOrganizationFields;
  }


  beforeCreate(data: any): any {
    data.ownerId = this.ownerId;
    return data;
  }
}
