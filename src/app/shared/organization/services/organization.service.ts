import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {ICreateService, IListService} from "../../core/service/crud.service";
import {environment} from "../../../../environments/environment";
import {OrganizationList} from "../../../pages/admin/models/organization-list.model";
import {HttpService} from "../../../../../projects/uir-core/src/lib/services/http.service";

@Injectable({
  providedIn: 'root'
})
export class OrganizationService implements ICreateService<any>, IListService<any, OrganizationList> {

  constructor(private http: HttpService) {
  }

  createApi = environment.api + 'api/companies';
  infoApi = environment.api + 'api/companies/ref/$referral/info';
  listApi = environment.api + 'api/admin/organizations';

  create(data: any, urlProvider: () => string): Observable<number> {
    return this.http.POST<number>(this.createApi, data);
  }

  getInfoByRef(ref: string): Observable<any> {
    let url = this.infoApi.replace('$referral', ref);
    return this.http.GET<number>(url);
  }

  list(data: any, urlProvider: () => string): Observable<OrganizationList> {
    return;
  }
}
