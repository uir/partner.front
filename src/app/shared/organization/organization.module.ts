import {NgModule} from '@angular/core';
import {CreateOrganizationComponent} from './create-organization/create-organization.component';
import {SharedFormlyModule} from "../../../../projects/shared-formly/src/lib/uir-formly.module";
import {OrganizationService} from "./services/organization.service";

@NgModule({
  imports: [
    SharedFormlyModule.forRoot(),
  ],
  declarations: [CreateOrganizationComponent],
  exports: [CreateOrganizationComponent],
  providers: [OrganizationService]
})
export class OrganizationModule {
}
