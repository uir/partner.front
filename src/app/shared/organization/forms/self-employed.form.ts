import {FormlyFieldConfig} from "@ngx-formly/core";

const selfEployedFieldhideExpression = (model: any, formState: any) => {
  return model.activeType === 1 || model.activeType === 2 || !model.activeType;
};
export const selfEployedFields = [
  {
    key: 'numberPassport',
    type: 'input',
    templateOptions: {
      label: 'Номер паспорта',
      required: true
    },
    validators: {
      numberPassport: {
        expression: (c) => /^\d{6}$/.test(c.value),
        message: (error, field: FormlyFieldConfig) => `Значение должно содержать 6 цифр`,
      },
    },
    hideExpression: selfEployedFieldhideExpression,
  },
  {
    key: 'seriesPassport',
    type: 'input',
    templateOptions: {
      label: 'Серия паспорта',
      required: true,
    },
    validators: {
      seriesPassport: {
        expression: (c) => /^\d{4}$/.test(c.value),
        message: (error, field: FormlyFieldConfig) => `Значение должно содержать 4 цифры`,
      },
    },
    hideExpression: selfEployedFieldhideExpression,
  },
  {
    key: 'residenceAddress',
    type: 'input',
    templateOptions: {
      label: 'Адрес проживания по паспорту',
      required: true,
    },
    hideExpression: selfEployedFieldhideExpression,
  },
  {
    key: 'birthday',
    type: 'input',
    templateOptions: {
      label: 'День рождения',
      required: true,
    },
    validators: {
      birthday: {
        expression: (c) => /^\d{2}\.\d{2}\.\d{4}$/.test(c.value),
        message: (error, field: FormlyFieldConfig) => `Дата должна быть в формате дд.мм.гггг`,
      },
    },
    hideExpression: selfEployedFieldhideExpression,
  },
  {
    key: 'passportDepartment',
    type: 'input',
    templateOptions: {
      label: 'Кем выдан паспорт',
      required: true,
    },
    hideExpression: selfEployedFieldhideExpression,
  },
  {
    key: 'datePassport',
    type: 'input',
    templateOptions: {
      label: 'Когда выдан папорт',
      required: true,
    },
    validators: {
      birthday: {
        expression: (c) => /^\d{2}\.\d{2}\.\d{4}$/.test(c.value),
        message: (error, field: FormlyFieldConfig) => `Дата должна быть в формате дд.мм.гггг`,
      },
    },
    hideExpression: selfEployedFieldhideExpression,
  }
];
