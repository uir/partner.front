import {selfEployedFields} from "./self-employed.form";

const companyFieldhideExpression = (model: any) => {
  return model.activeType === 3 || !model.activeType;
};
const ipFieldhideExpression = (model: any) => {
  return model.activeType === 3 || model.activeType === 2 || !model.activeType;
};

export const createOrganizationFields = [
  {
    key: 'logo',
    type: 'img-upload',
    templateOptions: {
      label: 'Логотип',
      required: true
    }
  },
  {
    key: 'name',
    type: 'org-autocomplete',
    templateOptions: {
      label: 'Название',
      required: true,
      max: 100,
      onSelect: (suggestion, form) => {
        let data: any = {
          fio: suggestion.data.type == 'INDIVIDUAL' ? suggestion.data.name.full : suggestion.data.management.name,
          address: suggestion.data.address.value,
          name: suggestion.value,
          activeType: suggestion.data.inn.length === 10 ? 1 : 2
        };
        if (data.activeType === 1)
          data = {
            ...data,
            inn: suggestion.data.inn,
            kpp: suggestion.data.kpp,
            ogrn: suggestion.data.ogrn
          };
        if (data.activeType === 2)
          data = {
            ...data,
            inn: suggestion.data.inn,
            ogrn: suggestion.data.ogrn
          };
        form.setValue({...form.value, ...data});
      }
    }
  },
  {
    key: 'fio',
    type: 'input',
    templateOptions: {
      label: 'ФИО',
      required: true,
    }
  },
  {
    key: 'address',
    type: 'input',
    templateOptions: {
      label: 'Юр. адрес',
      required: true,
    }
  },
  {
    key: 'activeType',
    type: 'select',
    templateOptions: {
      label: 'Тип',
      required: true,
      options: [
        {value: 1, label: 'Организация'},
        {value: 2, label: 'Индвидуальный предприниматель'},
        {value: 3, label: 'Самозанятость'}
      ],
    },
  },
  {
    key: 'inn',
    type: 'input',
    templateOptions: {
      label: 'ИНН',
      required: true,
    },
    hideExpression: companyFieldhideExpression,
  },
  {
    key: 'kpp',
    type: 'input',
    templateOptions: {
      label: 'КПП',
      required: true,
    },
    hideExpression: ipFieldhideExpression,
  },
  {
    key: 'ogrn',
    type: 'input',
    templateOptions: {
      label: 'ОГРН',
      required: true,
    },
    hideExpression: companyFieldhideExpression,
  },
  ...selfEployedFields
];
