import {NgModule} from '@angular/core';
import {CreateUserComponent} from "./create-user/create-user.component";
import {SharedFormlyModule} from "../../../../projects/shared-formly/src/lib/uir-formly.module";

@NgModule({
  imports: [
    SharedFormlyModule.forRoot()
  ],
  declarations: [CreateUserComponent],
  exports: [CreateUserComponent]
})
export class UserModule {
}
