import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {CreateUser} from "./model/create-user.model";
import {tap} from "rxjs/operators";
import {environment} from "../../../environments/environment";
import {CreateUserByRef} from "./model/create-user-by-ref.model";
import {HttpService} from "../../../../projects/uir-core/src/lib/services/http.service";

@Injectable({providedIn: "root"})
export class CreateUserService {
  private _userReg = environment.api + 'api/auth/reg/partner';
  private _userRegByRef = environment.api + 'api/auth/reg/partner/$ref';
  private _regData = new BehaviorSubject<CreateUser>(null);
  private _regByRefData = new BehaviorSubject<CreateUserByRef>(null);

  constructor(private http: HttpService) {
  }

  public getRegData(): BehaviorSubject<CreateUser> {
    return this._regData;
  }
  public regByRefData(): BehaviorSubject<CreateUserByRef> {
    return this._regByRefData;
  }

  createUser(data: CreateUser): Observable<any> {
    return this.http.POST<number>(this._userReg, data).pipe(tap(() => this._regData.next(data)));
  }

  registerByRef(data: CreateUserByRef): Observable<any> {
    let url = this._userRegByRef.replace('$ref', data.referral);
    return this.http.POST<number>(url, data).pipe(tap(() => this._regByRefData.next(data)));
  }
}
