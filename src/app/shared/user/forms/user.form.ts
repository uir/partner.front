import {emailFiled, passwordField, phoneField} from "../../core/form/formly/fromly.fields";

export const userFields = [
  {
    key: 'name',
    type: 'input',
    templateOptions: {
      label: 'ФИО',
      required: true,
      max: 100
    }
  },
  emailFiled,
  phoneField,
  passwordField
];
