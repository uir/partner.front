export class CreateUser {
    email: string;
    phone: string;
    name: string;
    password: string;
}
