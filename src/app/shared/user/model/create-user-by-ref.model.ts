import {CreateUser} from "./create-user.model";

export class CreateUserByRef extends CreateUser {
  orgName: string;
  address: string;
  referral: string;
}
