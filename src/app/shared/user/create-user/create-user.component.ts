import {Component} from '@angular/core';
import {FormlyFieldConfig} from "@ngx-formly/core";
import {CreateUser} from "../model/create-user.model";
import {Router} from "@angular/router";
import {CreateUserService} from "../create-user.service";
import {FormComponent} from "../../core/form/form-component";
import {userFields} from "../forms/user.form";

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent extends FormComponent<CreateUser> {

  get fields(): FormlyFieldConfig[] {
    return userFields;
  }

  constructor(private router: Router, private registerService: CreateUserService) {
    super();
  }

  submit() {
    if (this.form.valid)
      this.registerService.createUser(this.form.value)
        .subscribe(() => this.router.navigate(['user', 'register', 'confirm']));
  }

}
