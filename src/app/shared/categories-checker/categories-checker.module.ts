import {NgModule} from '@angular/core';

import {CategoriesCheckerComponent} from './categories-checker.component';
import {CommonModule} from "@angular/common";

@NgModule({
  imports: [CommonModule],
  exports: [CategoriesCheckerComponent],
  declarations: [CategoriesCheckerComponent]
})
export class CategoriesCheckerModule {
}
