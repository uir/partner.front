import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CategoriesCheckerService} from "./categories-checker.service";
import {TemplateCompanyCreate} from "../../pages/user/modules/companies/create/create.company.model";

@Component({
  selector: 'app-categories-checker',
  templateUrl: './categories-checker.component.html',
  styleUrls: ['./categories-checker.component.css']
})
export class CategoriesCheckerComponent implements OnInit {

  @Input() templates: TemplateCompanyCreate[];
  @Input() size: number;
  @Output() onCheck = new EventEmitter<number[]>();
  @Input() categoriesCheckedIds: number[];

  constructor(private categoriesCheckerService: CategoriesCheckerService) {
    this.categoriesCheckedIds = [];
    this.size = 3
  }

  check(category, e) {
    if (this.categoriesCheckedIds.includes(category.id))
      this.categoriesCheckedIds = this.categoriesCheckedIds.filter(item => item != category.id);
    else if (this.categoriesCheckedIds.length < this.size)
      this.categoriesCheckedIds.push(category.id);
    this.onCheck.emit(this.categoriesCheckedIds);
  }

  isCheck(category) {
    return this.categoriesCheckedIds.includes(category.id);
  }

  isDisable(id) {
    if (this.categoriesCheckedIds.length == this.size && !this.categoriesCheckedIds.includes(id))
      return true;
    else return false;
  }

  ngOnInit(): void {
    if (!this.templates)
      this.categoriesCheckerService.getCategories().subscribe(x => this.templates = x);
  }
}
