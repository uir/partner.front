import {Injectable} from '@angular/core';
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs";
import {TemplateCompanyCreate} from "../../pages/user/modules/companies/create/create.company.model";
import {HttpService} from "../../../../projects/uir-core/src/lib/services/http.service";

@Injectable({
  providedIn: 'root'
})
export class CategoriesCheckerService {
  private shortcategories = environment.api + 'api/category/all/short';

  constructor(private http: HttpService) {
  }

  getCategories(): Observable<TemplateCompanyCreate[]> {
    return this.http.GET(this.shortcategories);
  }
}
