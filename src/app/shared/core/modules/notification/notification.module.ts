import {EffectsModule} from "@ngrx/effects";
import {NgModule} from "@angular/core";
import {NotificationEffects} from "app/shared/core/modules/notification/notification.effects";
import {NotificationService} from "app/shared/core/modules/notification/notification.service";

@NgModule({
  imports: [
    EffectsModule.forFeature([NotificationEffects]),
  ],
  declarations: [],
  providers: [NotificationService, NotificationEffects]
})

export class NotificationModule {
}
