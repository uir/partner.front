import { StateBase } from '../../ngrx/reducer/base.state';
import { type } from '../../util/utils';

export const notifyActions = {
    notify: type("APP_NOTIFICATION"),
    err: type("APP_NOTIFICATION_ERR")
}
export const notifyState: StateBase = {
    actions: [notifyActions.notify, notifyActions.err],
    initialState: {},
    reducer: (s, a): any => { return s }
}
