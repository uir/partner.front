export const notificationType = {
    info: "info",
    success: 'success',
    warning: 'warning',
    danger: 'danger',
    rose: 'rose',
    primary: 'primary'
}