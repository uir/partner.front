import {Actions, Effect, ofType} from "@ngrx/effects";
import {Injectable} from '@angular/core';
import {NotificationService} from "app/shared/core/modules/notification/notification.service";
import {notificationType} from "app/shared/core/modules/notification/notification.model";
import {notifyActions} from './notification.reducer';
import {ActionWithPayload} from "../../ngrx/action/base.action";
import {filter, switchMap} from "rxjs/operators";
import {of} from "rxjs";

@Injectable()
export class NotificationEffects {

  @Effect({dispatch: false})
  notifyErr$ = this.actions$
    .pipe(
      ofType<ActionWithPayload>(notifyActions.notify),
      switchMap(action => of(this.notifyservice.notify(notificationType.danger, action.payload) as any))
    );

  @Effect({dispatch: false})
  delete$ = this.actions$
    .pipe(
      filter(action => action.type.indexOf('_DELETE_SUCCESS') > 0),
      switchMap(() => of(this.notifyservice.alertDeleteSuccess() as any))
    );

  @Effect({dispatch: false})
  save$ = this.actions$
    .pipe(
      filter(action => action.type.indexOf('_SAVE_SUCCESS') > 0),
      switchMap(() => of(this.notifyservice.alertSaveSuccess() as any))
    );

  constructor(private actions$: Actions<ActionWithPayload>, private notifyservice: NotificationService) {

  }
}
