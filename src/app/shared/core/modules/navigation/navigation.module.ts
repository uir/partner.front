import {Actions, Effect, EffectsModule, ofType} from '@ngrx/effects';
import {Injectable, NgModule} from '@angular/core';
import {Router} from '@angular/router';
import {select, Store} from '@ngrx/store';
import {createFilialActionTypes} from '../../../../pages/main/filials/create/filial-create.reducer';
import {ActionWithPayload, getActionTypeSuccess} from 'app/shared/core/ngrx/action/base.action';
import {getCompanyId} from 'app/pages/main/admin-layout.reducer';
import {type} from 'app/shared/core/util/utils';
import {map, switchMap, withLatestFrom} from "rxjs/operators";
import {of} from "rxjs";

export const navigationActions = {
  navigate: type("APP_NAVIGATE"),
  navigateToDashboard: type("APP_NAVIGATE_TO_DASHBOARD"),
  navigateToMainSite: type("APP_NAVIGATE_TO_MAIN_SITE"),
};

@Injectable()
export class NavigationEffects {

  @Effect({dispatch: false})
  navigate$ = this.actions$
    .pipe(
      ofType<ActionWithPayload>(navigationActions.navigate),
      switchMap(action => of(this.route.navigate([action.payload])) as any)
    );

  @Effect()
  navigateToDashboard$ = this.actions$
    .pipe(
      ofType(navigationActions.navigateToDashboard, getActionTypeSuccess(createFilialActionTypes.submit)),
      withLatestFrom(this.store.pipe(select(getCompanyId))),
      map(arr => arr[1]),
      switchMap(id => of(new NavigationAction('/companies/' + id)))
    );

  constructor(private actions$: Actions<ActionWithPayload>, private route: Router, private store: Store<any>) {

  }
}

@NgModule({
  imports: [
    EffectsModule.forRoot([NavigationEffects]),
  ],
  exports: [],
  declarations: [],
  providers: [],
})
export class NavigationModule {
}

export class NavigationAction implements ActionWithPayload {
  type = navigationActions.navigate;
  payload: any;

  constructor(path: string) {
    this.payload = path;
  }
}
