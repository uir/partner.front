import {FormlyFieldConfig} from "@ngx-formly/core";

export const newPasswordField = {
  key: 'password',
  type: 'input',
  templateOptions: {
    type: 'password',
    label: 'Новый пароль',
    required: true
  },
  validators: {
    validation: ['password']
  },
};

export const passwordField = {
  key: 'password',
  type: 'input',
  templateOptions: {
    type: 'password',
    label: 'Пароль',
    required: true
  },
  validators: {
    validation: ['password']
  },
};

export const emailFiled = {
  key: 'email',
  type: 'input',
  templateOptions: {
    label: 'E-mail',
    required: true,
  },
  validators: {
    email: {
      expression: (c) => /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(c.value),
      message: (error, field: FormlyFieldConfig) => `"${field.formControl.value}" неправильный формат. Пример поля: uir@uir.ru`,
    },
  },
};

export const codeField = {
  key: 'code',
  type: 'input',
  templateOptions: {
    label: 'Код',
    required: true
  }
};

export const phoneField = {
  key: 'phone',
  type: 'input',
  templateOptions: {
    label: 'Номер телефона',
    required: true
  },
  validators: {
    phone: {
      expression: (c) => /^\+7[\d]{10}$/.test(c.value),
      message: (error, field: FormlyFieldConfig) => `"${field.formControl.value}" неправильный формат. Пример поля: +7XXXXXXXXXX`,
    },
  },
};
