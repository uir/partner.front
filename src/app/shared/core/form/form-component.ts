import {FormGroup} from "@angular/forms";
import {FormlyFieldConfig, FormlyFormOptions} from "@ngx-formly/core";
import {EventEmitter, OnInit, Output} from "@angular/core";
import {CrudService, ICreateService, IDeleteService, IEditService} from "../service/crud.service";

export class FormComponent<T> implements OnInit {

  protected _fields: FormlyFieldConfig[] = [];

  public form = new FormGroup({});
  public model: T = null;
  public options: FormlyFormOptions = {};

  public get fields(): FormlyFieldConfig[] {
    return this._fields;
  }

  ngOnInit(): void {
  }
}

export abstract class ActionFormComponent<T, TService extends ICreateService<T> | IEditService<T> | IDeleteService> extends FormComponent<T> {

  @Output() afterCreate = new EventEmitter<any>();

  constructor(protected service: TService, fileds: FormlyFieldConfig[] = []) {
    super();
    this._fields = fileds;
  }

  create() {
    let data: T = this.form.value;
    data = this.beforeCreate(data);
    (<ICreateService<T>>this.service).create(data, null).subscribe(x => this.afterCreate.emit(x));
  }

  beforeCreate(data: T): T {
    return data
  }

  update() {

  }

  delete() {

  }
}

export abstract class TableComponent<T> implements OnInit {
  public rows: T[] = [];

  protected constructor(protected service: CrudService<T>) {
  }

  abstract getDeleteUrlParams(item: T): any;

  abstract getGetUrlParams();

  delete(item: T) {
    let url = this.service.build(this.service.deleteApi, this.getDeleteUrlParams(item));
    this.service
      .delete(url)
      .subscribe(() => {
        this.afterDelete(item);
      });
  }

  afterDelete(item: T) {
  }

  ngOnInit(): void {
    this.update();
  }

  update() {
    let url = this.service.build(this.service.getApi, this.getGetUrlParams());
    this.service
      .get(url)
      .subscribe(x => this.rows = [...x]);
  }
}
