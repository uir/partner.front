import {FormBuilder, FormGroup} from '@angular/forms/forms';

export abstract class BaseForm<T> {

  protected required = 'Обязательно для заполнения';
  public data: T;
  public formErrors;
  public placeholders;
  public validationMessages;
  public form;
  public lables;
  public icons;
  public nonVisibleKeys: string[];
  public readOnly: string[];
  public validators;

  protected onValueChanged(data?: any) {
    if (!this.form) {
      return;
    }
    const form = this.form;

    // tslint:disable-next-line:forin
    for (const field in this.data) {
      Object.defineProperty(this.formErrors, field, {
        value: '',
        writable: true,
        enumerable: true,
        configurable: true
      });
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && !control.valid) {
        const messages = this.validationMessages[field];
        // tslint:disable-next-line:forin
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  public clear() {
    this.setFormData(this.data)
  }

  public getValueData(f) {
    return this.form.value[f]
  }

  public isReadOnly(f) {
    return this.readOnly.indexOf(f) > -1
  }


  public buildForm(): void {
    this.onValueChanged(this.data);
    this.form.valueChanges
      .subscribe(data => this.onValueChanged(data));
  }

  public isValid = (): boolean => this.form.valid;

  public getValue = (): T => this.form.value;

  public getKeys = (): string[] => {
    let keys = Object.keys(this.data);
    if (this.nonVisibleKeys != null) {
      return keys.filter(key => !this.nonVisibleKeys.includes(key))
    }
    return keys;
  };

  public getlabel(field: any): string {
    return this.lables[field];
  }

  public getIcon(field: any): string {
    return this.icons[field];
  }

  public isError(field: any): boolean {
    if (!this.formErrors[field]) return
    return this.formErrors[field].length > 0;
  }

  public getError(field): string {
    return this.formErrors[field];
  }

  public getPlaceholder(field): string {
    if (this.isError(field)) {
      return this.getError(field);
    } else {
      return this.placeholders[field];
    }
  }

  public setFormData(data, options?) {
    if (data != null)
      Object.keys(data).forEach(name => {
        if (this.form.controls[name]) {
          this.form.controls[name].setValue(data[name], options);
        }
      });
  }

  protected getForm(): FormGroup {
    let formGroup = {};
    Object.keys(this.data).forEach(key => {
      let val = [this.data[key], this.validators[key]];
      Object.defineProperty(formGroup, key, {
        value: val,
        writable: true,
        enumerable: true,
        configurable: true
      });
    });
    return this.fb.group(formGroup);
  };

  protected abstract getValidationMessages();

  protected getPlaceholders() {
    let obj = {} as T;
    let plhldr = {};
    Object.keys(obj).forEach(name => {
      Object.defineProperty(plhldr, name, {
        value: '',
        writable: true,
        enumerable: true,
        configurable: true
      });
    });
    return plhldr;
  }

  protected getIcons() {
  };

  protected isRequired(field): boolean {
    return this.validationMessages[field] && this.validationMessages[field].required;
  }

  constructor(data: T, protected fb: FormBuilder) {
    this.data = data;
    this.formErrors = {};
    this.placeholders = this.getPlaceholders();
  }
}
