import {BaseForm} from "./base.form";
import {FormBuilder} from "@angular/forms";

export interface FormConfig {
  data: {},
  placeholders?: {},
  labels?: {},
  validators?: {},
  messages?: {}
}

export const FormConfig = {
  messages: {
    required: 'Обязательно для заполнения'
  }
};

export class Form extends BaseForm<any> {

  protected getValidationMessages() {
  }

  constructor(config: FormConfig, fb: FormBuilder){
    super(config.data, fb);
    this.validators = config.validators;
    this.lables = config.labels;
    this.placeholders = config.placeholders;
    this.validationMessages = config.messages;
    this.form = this.getForm();
  }
}
