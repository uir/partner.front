import {Observable} from "rxjs";
import {HttpService} from "../../../../../projects/uir-core/src/lib/services/http.service";

export interface IListService<TData, TList> {
  list(data: TData, urlProvider: () => string): Observable<TList>;
}

export interface ICreateService<T> {
  create(data: T, urlProvider: () => string): Observable<number>;
}

export interface IEditService<T> {
  edit(data: T, urlProvider: () => string): Observable<any>;
}

export interface IDeleteService {
  delete(url: string): Observable<any>;
}

export interface IGetService<TOut> {
  get(url: string): Observable<TOut>;
}

export interface IUrlBuilder {
  build(url: string, params: { [key: string]: string }): string;
}

export enum RequestType {
  Get, Create, Delete, Edit
}

export abstract class CrudService<T>
  implements ICreateService<T>, IEditService<T>, IDeleteService, IGetService<T[]>, IUrlBuilder {

  public abstract get getApi(): string;
  public abstract get createApi(): string;
  public abstract get deleteApi(): string;
  public abstract get editApi(): string;

  public getDefaultUrlProvider(type: RequestType): () => string {
    switch (type) {
      case RequestType.Get:
        return () => this.getApi;
      case RequestType.Create:
        return () => this.createApi;
      case RequestType.Delete:
        return () => this.deleteApi;
      case RequestType.Edit:
        return () => this.editApi;
    }
  }

  protected constructor(protected http: HttpService) {
  }

  create(data: T, urlProvider: () => string = null): Observable<number> {
    let url = (urlProvider && this.getDefaultUrlProvider(RequestType.Create))();
    return this.http.POST<number>(url, data);
  }

  edit(data: T, urlProvider: () => string = null): Observable<any> {
    let url = (urlProvider && this.getDefaultUrlProvider(RequestType.Edit))();
    return this.http.POST<number>(url, data);
  }

  delete(url): Observable<any> {
    return this.http.DELETE<any>(url, url);
  }

  get(url): Observable<T[]> {
    return this.http.GET<T[]>(url);
  }

  build(url: string, params: { [p: string]: string | number }): string {
    for (let p of Object.keys(params)) {
      url = url.replace("$" + p, params[p] + "")
    }
    return url;
  }
}
