import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css']
})
export class CompanyComponent implements OnInit {

  @Input() name: string;
  @Input() adress: string;
  @Input() id: number;
  @Input() logo: string;

  constructor() {
  }

  ngOnInit() {
  }

}
