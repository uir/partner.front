import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {MatIconModule, MatListModule} from "@angular/material";
import {AppRoutes} from "../../../../app.routing";
import {AppMapComponent} from "./components/app-map.component";
import {AgmCoreModule} from "@agm/core";

@NgModule({
  imports: [
    CommonModule,
    MatListModule,
    RouterModule,
    MatIconModule,
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyAhdRieYCmfU9EU3nqEYYNH74noY5E_Oac"
    }),
  ],
  declarations: [
    AppMapComponent,
  ],
  exports: [
    AppMapComponent
  ]
})
export class AppMapModule {
}
