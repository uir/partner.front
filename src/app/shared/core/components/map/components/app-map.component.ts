import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, EventEmitter,
  Input,
  OnChanges,
  OnInit, Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import {googleMapStyles} from "./google-map.styles";
import {MouseEvent} from "@agm/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-map',
  templateUrl: 'app-map.component.html',
  styleUrls: ['app-map.component.css']
})
export class AppMapComponent implements OnChanges {
  constructor(private cd: ChangeDetectorRef){
  }

  @Input() position: {lat: number,lng: number};
  @Input() marker: {lat: number,lng: number};
  @Input() public zoom = 11;
  @Output() mapClick: EventEmitter<MouseEvent> = new EventEmitter<MouseEvent>();

  public styles = googleMapStyles;

  mapClickEmit(e){
    this.mapClick.emit(e)
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.cd.detectChanges();
  }
}
