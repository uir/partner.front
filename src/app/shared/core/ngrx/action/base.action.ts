import { Action } from '@ngrx/store';

export interface ActionWithPayload extends Action{
  payload?: any
}

export enum RequestType {
    LOAD, SUCCESS, ERROR
}

export function getActionType(prefix: string, rtype: RequestType): string {
    return prefix + '_' + RequestType[rtype];
}
export function getActionTypeLoad(prefix: string) {
    return getActionType(prefix, RequestType.LOAD);
}
export function getActionTypeSuccess(prefix) {
    return getActionType(prefix, RequestType.SUCCESS);
}
export function getActionTypeError(prefix) {
    return getActionType(prefix, RequestType.ERROR);
}
export class LoadAction implements Action {
    type = getActionType(this.prefix, RequestType.LOAD);
    constructor(private prefix: string, public payload?: any) {
    }
}
export class SuccessAction<T> implements Action {
    type = getActionType(this.prefix, RequestType.SUCCESS);
    constructor(private prefix: string, public payload?: T) {
    }
}
export class ErrorAction implements Action {
    type = getActionType(this.prefix, RequestType.ERROR);
    constructor(private prefix: string, public payload?: any) {
    }
}
