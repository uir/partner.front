import {AppRequest, requestInitial} from "app/shared/core/model/responce.model";
import {getActionTypeError, getActionTypeLoad, getActionTypeSuccess} from '../action/base.action';
import {requestReducer} from "app/shared/core/ngrx/reducer/base.reducer";
import {type} from 'app/shared/core/util/utils';

export class RequestActionsBase {
  public getActionsArr(): string[] {
    let arr = [];
    let keys = Object.keys(this);
    for (let i = 0; i < keys.length; i++) {
      let element = this[keys[i]];
      arr.push(getActionTypeLoad(element));
      arr.push(getActionTypeSuccess(element));
      arr.push(getActionTypeError(element));
    }
    return arr;
  }
}

export class RequestActions extends RequestActionsBase {
  get: string;
  delete: string;
  submit: string;

  constructor(prefixType?: string) {
    super();
    if (type != null) {
      this.get = type(prefixType);
      this.delete = type(prefixType + "_DELETE");
      this.submit = type(prefixType + "_SUBMIT");
    }
  }
}

export class EditableListActions extends RequestActionsBase {
  getItem: string;
  get: string;
  delete: string;
  submit: string;

  constructor(prefixType?: string) {
    super();
    if (type != null) {
      this.get = type(prefixType);
      this.delete = type(prefixType + "_DELETE");
      this.submit = type(prefixType + "_SUBMIT");
      this.getItem = type(prefixType + "_ITEM");
    }
  }
}

export class RequestGetActions extends RequestActionsBase {
  get: string;

  constructor(prefixType?: string, withoutLoader?: boolean) {
    super();
    if (type != null) {
      this.get = type(prefixType);
      if (withoutLoader)
        this.get += "_NO_LOADER";
    }
  }
}

export class RequestSubmitActions extends RequestActionsBase {
  submit: string;

  constructor(prefixType?: string) {
    super();
    if (type != null) {
      this.submit = type(prefixType + "_SUBMIT");
    }
  }
}

export class RequestEditActions extends RequestActionsBase {
  get: string;
  submit: string;

  constructor(prefixType?: string) {
    super();
    if (type != null) {
      this.submit = type(prefixType + "_SUBMIT");
      this.get = type(prefixType);
    }
  }
}

export class RequestDeleteActions extends RequestActionsBase {
  delete: string;

  constructor(prefixType?: string) {
    super();
    if (type != null) {
      this.delete = type(prefixType + "_DELETE");
    }
  }
}

export class RequestListState<T> {
  items: T[];
  itemsLoad: AppRequest;
  itemsDelete?: AppRequest;
  itemSave?: AppRequest;

  public static getInitial<TItem>(): RequestListState<TItem> {
    return {
      items: [],
      itemsLoad: requestInitial,
      itemsDelete: requestInitial,
      itemSave: requestInitial
    };
  }

  public static getInitialLoad<TItem>(): RequestListState<TItem> {
    return {
      items: [],
      itemsLoad: requestInitial
    };
  }

  public static getReducer(state, action, actions) {
    if (state['itemsLoad'] && actions.get) {
      let newstate = requestReducer(state, action, actions.get, 'itemsLoad', 'items');
      if (newstate != null)
        return newstate;
    }

    if (state['itemsDelete'] && actions.delete) {
      let newstate = requestReducer(state, action, actions.delete, 'itemsDelete');
      if (newstate != null)
        return newstate;
    }

    if (state['itemSave'] && actions.submit) {
      let newstate = requestReducer(state, action, actions.submit, 'itemSave');
      if (newstate != null)
        return newstate;
    }

    return state;
  }

  public static createReducer<TState, TItem>(actions: RequestActions): (s, a) => TState {
    return (s = RequestListState.getInitial<TItem>(), a): TState => {
      return RequestListState.getReducer(s, a, actions)
    };
  }
}

export class RequestEditableListState<T> {
  items: T[];
  item: T;
  itemsLoad: AppRequest;
  itemLoad: AppRequest;
  itemsDelete: AppRequest;
  itemSave: AppRequest;

  public static getInitial<TItem>(): RequestEditableListState<TItem> {
    return {
      items: [],
      item: null,
      itemLoad: requestInitial,
      itemsLoad: requestInitial,
      itemsDelete: requestInitial,
      itemSave: requestInitial
    };
  }

  public static getReducer(state, action, actions) {
    if (state['itemLoad'] && actions.getItem) {
      let newstate = requestReducer(state, action, actions.getItem, 'itemLoad', 'item');
      if (newstate != null) {
        newstate = Object.assign({}, newstate, {
          itemSave: requestInitial
        })
        return newstate;
      }
    }

    if (state['itemsLoad'] && actions.get) {
      let newstate = requestReducer(state, action, actions.get, 'itemsLoad', 'items');
      if (newstate != null)
        return newstate;
    }

    if (state['itemsDelete'] && actions.delete) {
      let newstate = requestReducer(state, action, actions.delete, 'itemsDelete');
      if (newstate != null)
        return newstate;
    }

    if (state['itemSave'] && actions.submit) {
      let newstate = requestReducer(state, action, actions.submit, 'itemSave');
      if (newstate != null)
        return newstate;
    }

    return state;
  }
}

export class RequestObjectState<T> {
  item: T;
  itemLoad?: AppRequest;
  itemDelete?: AppRequest;
  itemSave?: AppRequest;

  public static getInitial<TItem>(): RequestObjectState<TItem> {
    return {
      item: null,
      itemLoad: requestInitial,
      itemDelete: requestInitial,
      itemSave: requestInitial
    };
  }

  public static getInitialLoad<TItem>(): RequestObjectState<TItem> {
    return {
      item: null,
      itemLoad: requestInitial
    };
  }

  public static getInitialEdit<TItem>(): RequestObjectState<TItem> {
    return {
      item: null,
      itemLoad: requestInitial,
      itemSave: requestInitial
    };
  }

  public static getInitialSave<TItem>(): RequestObjectState<TItem> {
    return {
      item: null,
      itemSave: requestInitial
    };
  }

  public static getReducer(state, action, actions: any) {
    if (state['itemLoad'] && actions.get) {
      let newstate = requestReducer(state, action, actions.get, 'itemLoad', 'item');
      if (newstate != null) {
        newstate = Object.assign({}, newstate, {
          itemSave: requestInitial
        })
        return newstate;
      }
    }

    if (state['itemDelete'] && actions.delete) {
      let newstate = requestReducer(state, action, actions.delete, 'itemDelete');
      if (newstate != null)
        return newstate;
    }

    if (state['itemSave'] && actions.submit) {
      let newstate = requestReducer(state, action, actions.submit, 'itemSave', 'item');
      if (newstate != null)
        return newstate;
    }

    return state;
  }
}

export class RequestState {
  itemLoad?: AppRequest;
  itemDelete?: AppRequest;
  itemSave?: AppRequest;

  public static getInitialLoad(): RequestState {
    return {
      itemLoad: requestInitial
    };
  }

  public static getInitial(): RequestState {
    return {
      itemLoad: requestInitial,
      itemDelete: requestInitial,
      itemSave: requestInitial
    };
  }

  public static getInitialSave(): RequestState {
    return {
      itemSave: requestInitial
    };
  }

  public static getInitialDelete(): RequestState {
    return {
      itemDelete: requestInitial
    };
  }

  public static getReducer<TState>(state: TState, action, actions: any): TState {
    if (state['itemLoad'] && actions.get) {
      let newstate = requestReducer(state, action, actions.get, 'itemLoad');
      if (newstate != null)
        return newstate;
    }

    if (state['itemDelete'] && actions.delete) {
      let newstate = requestReducer(state, action, actions.delete, 'itemDelete');
      if (newstate != null)
        return newstate;
    }

    if (state['itemSave'] && actions.submit) {
      let newstate = requestReducer(state, action, actions.submit, 'itemSave');
      if (newstate != null)
        return newstate;
    }

    return state;
  }

  public static createReducer<TState>(actions: RequestActions): (s, a) => TState {
    return (s = RequestState.getInitial(), a): TState => {
      return RequestState.getReducer(s, a, actions)
    };
  }
}
