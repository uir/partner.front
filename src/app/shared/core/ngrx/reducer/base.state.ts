import {
  EditableListActions,
  RequestActions,
  RequestDeleteActions,
  RequestEditActions,
  RequestEditableListState,
  RequestGetActions,
  RequestListState,
  RequestObjectState,
  RequestSubmitActions,
  RequestState,
} from './responce.reducer';

export class StateBase {
  initialState: any;
  actions: any;
  reducer: (s, a) => any

  static create<T>(actionsTypes: RequestActions | RequestGetActions | RequestSubmitActions | RequestEditActions | RequestDeleteActions,
                   initialFunc: () => any,
                   reducerFunc: (s, a, actionsTypes) => any): StateBase {
    return {
      actions: actionsTypes.getActionsArr(),
      initialState: initialFunc(),
      reducer: (s, a): any => {
        return reducerFunc(s, a, actionsTypes)
      }
    }
  }

  static createForEditableList<T>(actionsTypes: EditableListActions): StateBase {
    return {
      actions: actionsTypes.getActionsArr(),
      initialState: RequestEditableListState.getInitial(),
      reducer: (s, a): any => {
        return RequestEditableListState.getReducer(s, a, actionsTypes)
      }
    }
  }

  static createForAdd<T>(actionsTypes: RequestSubmitActions): StateBase {
    return {
      actions: actionsTypes.getActionsArr(),
      initialState: RequestState.getInitialSave(),
      reducer: (s, a): any => {
        return RequestState.getReducer(s, a, actionsTypes)
      }
    }
  }

  static createForSubmit<T>(actionsTypes: RequestSubmitActions): StateBase {
    return {
      actions: actionsTypes.getActionsArr(),
      initialState: RequestState.getInitialSave(),
      reducer: (s, a): any => {
        return RequestState.getReducer(s, a, actionsTypes)
      }
    }
  }

  static createForList<T>(actionsTypes: RequestGetActions): StateBase {
    return {
      actions: actionsTypes.getActionsArr(),
      initialState: RequestListState.getInitialLoad(),
      reducer: (s, a): any => {
        return RequestListState.getReducer(s, a, actionsTypes)
      }
    }
  }

  static createForObject<T>(actionsTypes: RequestGetActions): StateBase {
    return {
      actions: actionsTypes.getActionsArr(),
      initialState: RequestObjectState.getInitialLoad(),
      reducer: (s, a): any => {
        return RequestObjectState.getReducer(s, a, actionsTypes)
      }
    }
  }

  static createForEdit<T>(actionsTypes: RequestEditActions): StateBase {
    return {
      actions: actionsTypes.getActionsArr(),
      initialState: RequestObjectState.getInitialEdit(),
      reducer: (s, a): any => {
        return RequestObjectState.getReducer(s, a, actionsTypes)
      }
    }
  }

  static createForDelete<T>(actionsTypes: RequestDeleteActions): StateBase {
    return {
      actions: actionsTypes.getActionsArr(),
      initialState: RequestState.getInitialDelete(),
      reducer: (s, a): any => {
        return RequestState.getReducer(s, a, actionsTypes)
      }
    }
  }
}
