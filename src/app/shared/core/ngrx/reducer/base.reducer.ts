import {RequestType, getActionType, ActionWithPayload} from "app/shared/core/ngrx/action/base.action";

import { Action } from '@ngrx/store';

export function requestReducer<T, TData>(state: T, action: Action, prefix: string, stateResponceProp: string, stateResponceDataProp?: string) {
    switch (action.type) {
        case getActionType(prefix, RequestType.LOAD): {
            return forLoad(state, stateResponceProp);
        }
        case getActionType(prefix, RequestType.SUCCESS): {
            return forSuccess<TData>(state, action, stateResponceProp, stateResponceDataProp);
        }
        case getActionType(prefix, RequestType.ERROR): {
            return forError(state, action, stateResponceProp);
        }
    }
    return null;
}

function forLoad(state, stateResponceProp: string) {
    let newState = {};
    let responcePropValue = Object.assign({}, state[stateResponceProp], {
        isLoading: true,
        isSuccess: false,
    });
    Object.defineProperty(newState, stateResponceProp, {
        value: responcePropValue,
        writable: true,
        enumerable: true,
        configurable: true
    });
    return Object.assign({}, state, newState);
}

function forSuccess<TData>(state, action: ActionWithPayload, stateResponceProp: string, stateResponceDataProp?: string) {
    let newState = {};
    let responcePropValue = Object.assign({}, state[stateResponceProp], {
        isLoading: false,
        isSuccess: true
    });
    if (stateResponceDataProp != null) {
        Object.defineProperty(newState, stateResponceDataProp, {
            value: action.payload as TData,
            writable: true,
            enumerable: true,
            configurable: true
        });
    }
    Object.defineProperty(newState, stateResponceProp, {
        value: responcePropValue,
        writable: true,
        enumerable: true,
        configurable: true
    });
    state = Object.assign({}, state, newState);
    return state;
}

function forError(state, action: ActionWithPayload, stateResponceProp: string) {
    let newState = {};
    let responcePropValue = Object.assign({}, state[stateResponceProp], {
        isLoading: false,
        isSuccess: false,
        message: action.payload
    });
    Object.defineProperty(newState, stateResponceProp, {
        value: responcePropValue,
        writable: true,
        enumerable: true,
        configurable: true
    });
    return Object.assign({}, state, newState);
}
