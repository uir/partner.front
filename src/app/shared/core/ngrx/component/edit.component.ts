import { ActivatedRoute } from '@angular/router';
import { BaseForm } from '../../form/base.form';
import { EditableListActions } from '../reducer/responce.reducer';
import { Observable } from 'rxjs';
import { OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { getActionTypeLoad } from 'app/shared/core/ngrx/action/base.action';

export class EditComponent<TItem, TForm extends BaseForm<TItem>> implements OnInit {
    public item: Observable<TItem>;

    constructor(
        public form: TForm,
        protected store: Store<any>,
        protected actionsTypes: EditableListActions,
        private itemSelectorFunc: (state: any) => TItem) {
        this.item = this.store.select(itemSelectorFunc);
    }

    ngOnInit(): void {
        this.form.buildForm();
        this.item.subscribe(item => this.form.setFormData(item));
    }

    protected load(action) {
        this.store.dispatch(action);
    }

    protected save() {
        let data = this.form.getValue();
        if (this.form.isValid()) {
            this.store.dispatch({
                type: getActionTypeLoad(this.actionsTypes.submit),
                payload: data
            });
        }
    }
}
