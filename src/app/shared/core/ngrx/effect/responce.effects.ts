import {ActionWithPayload, ErrorAction, getActionTypeLoad, SuccessAction} from "app/shared/core/ngrx/action/base.action";
import {Action} from '@ngrx/store';
import {Actions, ofType} from '@ngrx/effects';
import {Observable, of} from "rxjs";
import {catchError, concat, flatMap, map, mergeMap, switchMap} from "rxjs/operators";

export function getRequestEffect<T>(action: Actions, actionType: string, serviceFunc: () => Observable<T>): Observable<Action> {
  return action
    .pipe(
      ofType(getActionTypeLoad(actionType)),
      switchMap(() => serviceFunc()
        .pipe(
          map((data: T) => new SuccessAction<T>(actionType, data)),
          catchError(() => of(new ErrorAction(actionType, 'ошибка')))
        )
      )
    );
}


export function getRequestEffectWithData<T, TData>(
  action: Actions<ActionWithPayload>,
  actionType: string,
  serviceFunc: (data: TData) => Observable<T>,
  ...beforeActions: ActionWithPayload[]): Observable<ActionWithPayload> {
  return action
    .pipe(
      ofType<ActionWithPayload>(getActionTypeLoad(actionType)),
      flatMap(a => serviceFunc(a.payload)
        .pipe(
          mergeMap((data: T) => {
            let actions$ = of(new SuccessAction<T>(actionType, data));
            for (let i = 0; i < beforeActions.length; i++) {
              actions$.pipe(concat(of(beforeActions[i])));
            }
            return actions$;
          }),
          catchError(err => of(new ErrorAction(actionType, err)))
        )
      )
    );
}
