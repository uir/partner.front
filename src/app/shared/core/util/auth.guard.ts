import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';

@Injectable()
export class AuthGuard implements CanActivate {
  roles: string[];

  constructor(private store: Store<any>, private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.roles != null && !this.roles.includes(route.data.role) || this.roles == null) {
      this.router.navigate(['user', 'login']);
      return false;
    }
    return true;
  }
}
