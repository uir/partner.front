export interface NoParamConstructor<T> {
    new(): T;
}
