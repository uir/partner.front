import {HttpErrorResponse} from "@angular/common/http";

export class Error {
  message: string;
  status: number;
  error?: string;

  public static parse(err: any): Error {
    if (err instanceof HttpErrorResponse) {
      let ex = err as HttpErrorResponse;
      return {
        message: Error.getErrorMessage(ex),
        status: ex.status
      };
    }
    if (err._body) {
      let body = JSON.parse(err._body);
      if (body["error"])
        return {
          status: err.status,
          error: body.error,
          message: Error.getMessageRus(body.error)
        };
      return {
        message: JSON.parse(err._body).message,
        status: err.status
      };
    }
    if (typeof err === "string") {
      let error = JSON.parse(err);
      return {
        message: error.message ? error.message : error.Message,
        status: 400
      }
    }
  }

  private static getMessageRus(err: string) {
    if (err == "invalid_grant")
      return "Неверный логин или пароль";
  }

  private static getErrorMessage(err: HttpErrorResponse) {
    if (err.error && err.error.error && err.error.error == "invalid_grant")
      return "Неверный логин или пароль";
    if (typeof err.error === 'object')
      if (err.error.message)
        return err.error.message;
      else
        return err.error.Message;
    return err.error;
  }
}
