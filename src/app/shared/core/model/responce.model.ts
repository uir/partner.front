export class AppRequest {
    message: string;
    isSuccess: boolean;
    isLoading: boolean;
}

export const requestInitial = {
    isSuccess: false,
    isLoading: false,
    message: ''
}
