import {Address} from "../../google-place-autocomplete/model/address.model";
import {FormGroup} from "@angular/forms";
import {FormlyFieldConfig} from "@ngx-formly/core";

export const createFilialFields = [
  {
    key: 'name',
    type: 'input',
    templateOptions: {
      label: 'Название',
      required: true,
    }
  },
  {
    key: 'phone',
    type: 'input',
    templateOptions: {
      label: 'Телефон',
    },
    validators: {
      phone: {
        expression: (c) => /^(\d{7,11}){1}((,|\s,\s|\s,|,\s){1}(\d{7,11}))*$/.test(c.value),
        message: (error, field: FormlyFieldConfig) => `Номера должны состоять из 7-11 цифр без символов и разделяться запятыми`,
      },
    },
  },
  {
    key: 'website',
    type: 'input',
    templateOptions: {
      label: 'Web сайт',
    }
  },
  {
    key: 'address',
    type: 'address-autocomplete',
    templateOptions: {
      label: 'Адрес',
      required: true,
      onSelect: (address: Address, form: FormGroup) => {
        form.get("address").setValue(address.formattedAddress);
        form.get("lat").setValue(address.lat);
        form.get("lng").setValue(address.lng);
      }
    }
  },
  {
    fieldGroupClassName: 'display-flex',
    fieldGroup: [
      {
        className: 'flex-50',
        key: 'lat',
        type: 'input',
        templateOptions: {
          label: 'Долгота',
          required: true,
        }
      },
      {
        className: 'flex-50',
        key: 'lng',
        type: 'input',
        templateOptions: {
          label: 'Широта',
          required: true,
        }
      },
    ]
  },
  {
    key: 'isActive',
    type: 'toggle',
    defaultValue: false,
    templateOptions: {
      label: 'Активность',
    },
  },
  {
    key: 'description',
    type: 'quill',
    templateOptions: {
      label: 'Описание',
      quillOptions: {
        placeholder: "Описание филиала",
        modules: {
          toolbar: [
            ['bold', 'italic', 'underline', 'strike'],
            ['blockquote'],
            [{'list': 'ordered'}, {'list': 'bullet'}],
            [{'indent': '-1'}, {'indent': '+1'}],
            [{'size': ['small', false, 'large']}],
            [{'color': []}, {'background': []}],
            [{'align': []}],
            ['image']
          ]
        }
      }
    },
  },
  {
    key: 'roomType',
    type: 'select',
    templateOptions: {
      label: 'Тип помещения',
      options: [
        {value: 1, label: 'Офис'},
        {value: 2, label: 'Квартира'},
      ],
    },
  },
  {
    key: 'room',
    type: 'input',
    templateOptions: {
      label: 'Номер офиса',
    },
    hideExpression: (model: any, formState: any) => {
      return !model.roomType;
    },
    expressionProperties: {
      'templateOptions.label': (model: any, formState: any) => {
        switch (model.roomType) {
          case 0: {
            return ''
          }
          case 1: {
            return 'Номер офиса'
          }
          case 2: {
            return 'Номер квартиры'
          }
        }
      },
    }
  }
];
