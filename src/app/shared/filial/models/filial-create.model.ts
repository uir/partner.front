export class FilialCreate {
  name: string;
  phone: string;
  website: string;
  address: string;
  priceLevel: number;
  lat: number;
  lng: number;
  isActive: boolean;
  organizationId?: number;
  roomType: number;
  room: string;
}
