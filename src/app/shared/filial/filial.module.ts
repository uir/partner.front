import {NgModule} from '@angular/core';
import {CreateFilialComponent} from './components/create-filial/create-filial.component';
import {ImagesUploaderComponent} from './components/images-uploader/images-uploader.component';
import {VideosUploaderComponent} from './components/videos-uploader/videos-uploader.component';
import {NgxDatatableModule} from "@swimlane/ngx-datatable";
import {FileUploadModule} from "ng2-file-upload";
import { UploadButtonComponent } from './components/upload-button/upload-button.component';
import {SharedFormlyModule} from "../../../../projects/shared-formly/src/lib/uir-formly.module";

@NgModule({
  imports: [
    SharedFormlyModule.forRoot(),
    FileUploadModule,
    NgxDatatableModule
  ],
  declarations: [CreateFilialComponent, ImagesUploaderComponent, VideosUploaderComponent, UploadButtonComponent],
  exports: [CreateFilialComponent, ImagesUploaderComponent, VideosUploaderComponent]
})
export class FilialModule { }
