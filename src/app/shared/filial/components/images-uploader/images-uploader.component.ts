import {Component} from '@angular/core';
import {Image} from "../../../../pages/main/filials/images/filial-image.model";
import {ImagesService} from "../../services/images.service";
import {FilesCrudTableComponent} from "../files-crud-table/files-crud-table.component";

@Component({
  selector: 'app-images-uploader',
  templateUrl: './images-uploader.component.html',
  styleUrls: ['./images-uploader.component.css'],
  providers: [ImagesService]
})
export class ImagesUploaderComponent extends FilesCrudTableComponent<Image, ImagesService> {

  constructor(service: ImagesService) {
    super(service);
  }

  getDeleteUrlParams(item: Image): any {
    return {...this.getGetUrlParams(), imageId: item.id};
  }
}
