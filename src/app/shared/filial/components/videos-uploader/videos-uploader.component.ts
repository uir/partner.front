import {Component} from '@angular/core';
import {Video} from "../../../../pages/main/filials/videos/filial-video.model";
import {FilesCrudTableComponent} from "../files-crud-table/files-crud-table.component";
import {VideosService} from "../../services/videos.service";

@Component({
  selector: 'app-videos-uploader',
  templateUrl: './videos-uploader.component.html',
  styleUrls: ['./videos-uploader.component.css'],
  providers: [VideosService]
})
export class VideosUploaderComponent extends FilesCrudTableComponent<Video, VideosService> {

  constructor(service: VideosService) {
    super(service);
  }

  getDeleteUrlParams(item: Video): any {
    return {filialId: this.filialId, $videoId: item.id};
  }
}
