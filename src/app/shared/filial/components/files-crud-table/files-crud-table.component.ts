import {Input} from "@angular/core";
import {TableComponent} from "../../../core/form/form-component";
import {CrudService} from "../../../core/service/crud.service";
import {FileUploader} from "ng2-file-upload";

export abstract class FilesCrudTableComponent<T extends { id: number }, TService extends CrudService<T>> extends TableComponent<T> {

  @Input() filialId: number;
  public urlFunc;
  public uploader = new FileUploader({});

  protected constructor(service: TService) {
    super(service);
  }

  afterDelete(item: T) {
    this.rows = this.rows.filter((x: any) => x.id != item.id);
  }

  ngOnInit(): void {
    this.urlFunc = () => this.service.build(this.service.createApi, this.getGetUrlParams());
    super.ngOnInit();
  }

  getGetUrlParams() {
    return {filialId: this.filialId};
  }

  abstract getDeleteUrlParams(item: T);
}
