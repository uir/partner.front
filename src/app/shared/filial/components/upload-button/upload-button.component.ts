import {Component, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import {FileItem, FileUploader} from "ng2-file-upload";
import {AUTH_STORAGE_KEY, retrieveTokens} from "../../../../../../projects/uir-core/src/lib/services/auth.service";

@Component({
  selector: 'app-upload-button',
  templateUrl: './upload-button.component.html',
  styleUrls: ['./upload-button.component.css']
})
export class UploadButtonComponent implements OnInit {
  @Input() urlFunc: () => string;
  @Input() uploader: FileUploader;
  @Output() upload = new EventEmitter<any>();

  constructor(@Inject(AUTH_STORAGE_KEY) private storageKey: string) {
  }

  ngOnInit() {
    this.uploader.setOptions({
      isHTML5: true,
      url: this.urlFunc(),
      headers: [
        {
          name: 'Authorization',
          value: 'bearer ' + retrieveTokens(this.storageKey).access_token
        }
      ]
    });
    this.uploader.onAfterAddingAll = (files: FileItem[]) => {
      this.uploader.uploadAll();
    };
    this.uploader.onCompleteAll = () => {
      this.upload.emit();
    }
  }

}
