import {Component, Input} from '@angular/core';
import {ActionFormComponent} from "../../../core/form/form-component";
import {createFilialFields} from "../../forms/create-filial.form";
import {FilialService} from "../../services/filial.service";
import {FilialCreate} from "../../models/filial-create.model";

@Component({
  selector: 'app-create-filial',
  templateUrl: './create-filial.component.html',
  styleUrls: ['./create-filial.component.css'],
  providers: [FilialService]
})
export class CreateFilialComponent extends ActionFormComponent<FilialCreate, FilialService> {

  @Input() orgId: number;

  constructor(service: FilialService) {
    super(service, createFilialFields);
  }

  beforeCreate(data: FilialCreate): FilialCreate {
    data.organizationId = this.orgId;
    return data;
  }
}
