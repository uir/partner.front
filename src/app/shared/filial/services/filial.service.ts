import {Injectable} from '@angular/core';
import {ICreateService} from "../../core/service/crud.service";
import {Observable} from "rxjs";
import {environment} from "../../../../environments/environment";
import {FilialCreate} from "../models/filial-create.model";
import {HttpService} from "../../../../../projects/uir-core/src/lib/services/http.service";

@Injectable({
  providedIn: 'root'
})
export class FilialService implements ICreateService<FilialCreate> {

  private filial(id): string {
    return environment.api + 'api/companies/' + id + '/places';
  }

  constructor(protected http: HttpService) {
  }

  create(data: FilialCreate, urlProvider: () => string = null): Observable<number> {
    let url = this.filial(data.organizationId);
    return this.http.POST<number>(url, data);
  }
}
