import {Injectable} from '@angular/core';
import {CrudService} from "../../core/service/crud.service";
import {Image} from "../../../pages/main/filials/images/filial-image.model";
import {environment} from "../../../../environments/environment";
import {HttpService} from "../../../../../projects/uir-core/src/lib/services/http.service";

@Injectable({
  providedIn: 'root'
})
export class ImagesService extends CrudService<Image> {

  constructor(http: HttpService) {
    super(http);
  }

  getApi = environment.apiPublic + 'api/places/$filialId/images';
  deleteApi = `${environment.api}api/places/$filialId/images/$imageId`;

  get editApi(): string {
    return "";
  }

  get createApi(): string {
    return environment.api + 'api/places/$filialId/images/upload';
  }
}
