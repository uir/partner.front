import { Injectable } from '@angular/core';
import {CrudService} from "../../core/service/crud.service";
import {Video} from "../../../pages/main/filials/videos/filial-video.model";
import {environment} from "../../../../environments/environment";
import {HttpService} from "../../../../../projects/uir-core/src/lib/services/http.service";

@Injectable({
  providedIn: 'root'
})
export class VideosService extends CrudService<Video>{

  constructor(http: HttpService) {
    super(http);
  }

  getApi = environment.apiPublic + 'api/places/$filialId/videos';
  createApi = environment.api + 'api/places/$filialId/videos/upload';
  deleteApi = environment.api + 'api/places/$filialId/videos/$videoId';

  get editApi(): string {
    return "";
  }
}
