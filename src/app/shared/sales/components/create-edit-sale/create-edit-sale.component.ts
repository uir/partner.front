import {Component, Inject, OnInit} from '@angular/core';
import {FormGroup} from "@angular/forms";
import {FormlyFieldConfig, FormlyFormOptions} from "@ngx-formly/core";
import {saleFormFields} from "../../forms/sale.form";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {Sale} from "../../sale.model";
import {Observable} from "rxjs";

export class SaleWindowData {
  model: Sale;
  title: string;
  button: string;
  submitFunc: (data: Sale) => Observable<any>;
}

@Component({
  selector: 'app-create-edit-sale',
  templateUrl: './create-edit-sale.component.html',
  styleUrls: ['./create-edit-sale.component.css']
})
export class CreateEditSaleComponent implements OnInit {

  public form = new FormGroup({});
  public model: Sale = null;
  public options: FormlyFormOptions = {};
  public fields: FormlyFieldConfig[] = saleFormFields;
  public title: string;
  public button: string;

  constructor(private dialogRef: MatDialogRef<CreateEditSaleComponent>,
              @Inject(MAT_DIALOG_DATA) private data: SaleWindowData) {
    this.model = this.data.model;
    this.title = this.data.title;
    this.button = this.data.button;
  }

  ngOnInit() {
  }

  submit() {
    this.data.submitFunc({...this.model, ...this.form.value}).subscribe(response => this.close(this.form.value, response));
  }

  close(res?: Sale, response?) {
    this.dialogRef.close({data: res, response: response});
  }

}
