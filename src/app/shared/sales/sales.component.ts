import {Component, Input, OnInit} from "@angular/core";
import {Sale, SaleOwnerType} from "./sale.model";
import {SalesService} from "./sales.service";
import {MatDialog} from "@angular/material";
import {SmartTableDialogComponent} from "../../../../projects/smart-table/src/lib/table-dialog/smart-table-dialog.component";
import {DialogData, DialogMode} from "../../../../projects/smart-table/src/lib/models/dialog-data.model";
import {saleFormFields} from "./forms/sale.form";

@Component({
  selector: 'app-sales',
  templateUrl: 'sales.component.html',
  styleUrls: ['sales.component.css'],
  providers: [SalesService]
})
export class SalesComponent implements OnInit {

  @Input() owner: SaleOwnerType;
  @Input() ownerId: number;

  public rows: Sale[];
  public columns = [
    {name: '№', prop: 'id', flexGrow: 1},
    {name: 'Название', prop: 'name', flexGrow: 1},
    {name: 'Описание', prop: 'description', flexGrow: 2},
    {name: 'Активность', prop: 'isActive', checkboxable: true, flexGrow: 1, minWidth: 10},
    {name: 'Тип', prop: 'discountType', flexGrow: 1},
    {name: 'Время окончания', prop: 'discountEndDate', flexGrow: 1},
    {name: 'Эксклюзив', prop: 'isExclusive', checkboxable: true, flexGrow: 1, minWidth: 10},
  ];

  constructor(private salesService: SalesService, public dialog: MatDialog) {
  }

  edit(sale) {
    this.dialog.open(SmartTableDialogComponent, {
      data: <DialogData>{
        model: sale,
        title: "Редактирование скидки №" + sale.id,
        submitButtonText: "Сохранить",
        submitFunc: data => this.salesService.edit(data, this.ownerId, this.owner)
      },
      panelClass: 'default-dialog'
    });
  }

  add() {
    const dialogRef = this.dialog.open(SmartTableDialogComponent, {
      data: <DialogData>{
        mode: DialogMode.create,
        form: saleFormFields,
        model: null,
        title: "Cоздание скидки",
        submitButtonText: "Сохранить",
        submitFunc: data => this.salesService.create(data, this.ownerId, this.owner)
      },
      panelClass: 'default-dialog'
    });
    let sub = dialogRef
      .afterClosed()
      .subscribe(x => {
        x.data.id = x.response;
        this.rows.push(x.data);
        this.rows = [...this.rows];
        sub.unsubscribe();
      });
  }

  delete(sale) {
    let sub = this.salesService
      .delete(sale, this.ownerId, this.owner)
      .subscribe(() => {
        this.rows = this.rows.filter(r => r.id !== sale.id);
        sub.unsubscribe();
      });
  }

  ngOnInit(): void {
    this.salesService.sales(this.ownerId, this.owner).subscribe(x => this.rows = x);
  }
}


