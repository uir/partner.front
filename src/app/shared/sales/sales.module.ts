import {NgModule} from "@angular/core";
import {NgxDatatableModule} from "@swimlane/ngx-datatable";
import {SalesComponent} from "./sales.component";
import {MatDialogModule, MatNativeDateModule} from "@angular/material";
import {SharedFormlyModule} from "../../../../projects/shared-formly/src/lib/uir-formly.module";
import {SmartTableModule} from "../../../../projects/smart-table/src/lib/smart-table.module";

@NgModule({
  imports: [
    SharedFormlyModule.forRoot(),
    SmartTableModule,
    MatNativeDateModule,
    NgxDatatableModule,
    MatDialogModule
  ],
  declarations: [
    SalesComponent,
  ],
  exports: [
    SalesComponent
  ],
})
export class SalesModule {
}
