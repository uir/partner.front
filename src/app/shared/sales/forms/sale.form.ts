export const saleFormFields = [
  {
    key: 'id',
    type: 'input',
    templateOptions: {
      label: 'id',
      required: true
    },
    hideExpression: true
  },
  {
    key: 'name',
    type: 'input',
    templateOptions: {
      label: 'Название',
      required: true
    },
  },
  {
    key: 'description',
    type: 'textarea',
    templateOptions: {
      label: 'Описание',
      required: true,
    }
  },
  {
    key: 'discountType',
    type: 'select',
    templateOptions: {
      label: 'Тип',
      required: true,
      options: [
        { value: 0, label: 'Скидка'  },
        { value: 1, label: 'Акция'  }
      ],
    },
  },
  {
    key: 'amount',
    type: 'input',
    templateOptions: {
      label: 'Размер скидки',
      required: true,
      max: 100
    },
    hideExpression: (model: any, formState: any) => {
      return model.discountType === 1;
    },
  },
  {
    key: 'discountEndDate',
    type: 'datepicker',
    templateOptions: {
      label: 'Дата окончания',
    },
  },
  {
    key: 'isActive',
    type: 'toggle',
    defaultValue: false,
    templateOptions: {
      label: 'Активность',
    },
  },
  {
    key: 'isExclusive',
    type: 'toggle',
    defaultValue: false,
    templateOptions: {
      label: 'Эксклюзив',
    },
  }
];
