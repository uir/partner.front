import {Injectable} from "@angular/core";
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs";
import {Sale, SaleOwnerType} from "./sale.model";
import {HttpService} from "../../../../projects/uir-core/src/lib/services/http.service";

@Injectable({
  providedIn: 'root'
})
export class SalesService {

  constructor(private http: HttpService) {
  }

  private salesApi = (id, ownerType: SaleOwnerType): string => {
    let owner = "";
    switch (ownerType) {
      case SaleOwnerType.Filial:
        owner = "places";
        break;
      case SaleOwnerType.Organization:
        owner = "companies";
        break;
    }
    return `${environment.api}api/${owner}/${id}/sales`;
  };

  sales(ownerId: number, ownerType: SaleOwnerType): Observable<Sale[]> {
    let url = this.salesApi(ownerId, ownerType);
    return this.http.GET<Sale[]>(url);
  }

  create(data: Sale, ownerId: number, ownerType: SaleOwnerType): Observable<number> {
    let url = this.salesApi(ownerId, ownerType);
    return this.http.POST<any>(url, data);
  }

  delete(data: Sale, ownerId: number, ownerType: SaleOwnerType): Observable<any> {
    let url = this.salesApi(ownerId, ownerType) + '/' + data.id;
    return this.http.DELETE<any>(url);
  }

  edit(data: Sale, ownerId: number, ownerType: SaleOwnerType): Observable<any> {
    let url = this.salesApi(ownerId, ownerType) + '/' + data.id;
    return this.http.POST<any>(url, data);
  }
}
