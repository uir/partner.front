export class Sale {
  id: number;
  name: string;
  description: string;
  amount: number;
  discountType: DiscountType;
  discountEndDate: string;
  isActive: boolean;
  isExclusive: boolean;
}

export enum DiscountType
{
  Common = 0,
  Stock = 1
}

export enum SaleOwnerType {
  Filial = 1,
  Organization = 2
}


