import {Directive, ElementRef, EventEmitter, OnInit, Output} from '@angular/core';
import {Address} from "../model/address.model";

declare var google: any;

@Directive({
  selector: '[app-google-place-autocomplete]',
})
export class GooglePlaceAutocompleteComponent implements OnInit {
  @Output() setAddress: EventEmitter<any> = new EventEmitter();

  constructor(private el: ElementRef) {
  }

  ngOnInit() {

    let autocomplete = new google.maps.places.Autocomplete(this.el.nativeElement, {
      types: ["address"]
    });
    autocomplete.addListener("place_changed", () => {
      let place = autocomplete.getPlace();

      if (place.geometry === undefined || place.geometry === null) {
        return;
      }

      let model = new Address();
      model.building = this.getAddressComponent(place, 'street_number');
      model.street = this.getAddressComponent(place, 'route');
      model.locality = this.getAddressComponent(place, 'administrative_area_level_2');
      model.region = this.getAddressComponent(place, 'administrative_area_level_1');
      model.country = this.getAddressComponent(place, 'country');
      model.index = Number(this.getAddressComponent(place, 'postal_code'));
      model.formattedAddress = place.formatted_address;
      model.lat = place.geometry.location.lat();
      model.lng = place.geometry.location.lng();

      this.setAddress.emit(model);
    });
  }

  getAddressComponent(place, type) {
    let addComponent = place.address_components.find(x => x.types && x.types[0] == type);
    return addComponent ? addComponent.long_name : '';
  }
}
