import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GooglePlaceAutocompleteComponent} from './google-place-autocomplete/google-place-autocomplete.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [GooglePlaceAutocompleteComponent],
  exports: [GooglePlaceAutocompleteComponent]
})
export class GooglePlaceAutocompleteModule {
}
