export const environment = {
  production: false,
  api: 'https://test.partner.api.uir.one/',
  apiPublic: 'https://test.public.api.uir.one/',
  mainSite: 'https://test.uir.one/',
  partnerSite: 'https://test.partner.uir.one/',
};
