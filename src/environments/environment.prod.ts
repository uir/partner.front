export const environment = {
  production: true,
  api: '//partner.api.uir.one/',
  apiPublic: '//public.api.uir.one/',
  mainSite: '//uir.one/',
  partnerSite: '//partner.uir.one/',
};
