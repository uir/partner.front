import { FrontPartnerPage } from './app.po';

describe('front-partner App', () => {
  let page: FrontPartnerPage;

  beforeEach(() => {
    page = new FrontPartnerPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
