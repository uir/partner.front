FROM node:lts-alpine AS build
RUN mkdir src
COPY package.json package-lock.json ./src/
WORKDIR /src
RUN npm i
COPY . .
ARG configuration=prod
RUN npm run build:$configuration

FROM nginx:1.15-alpine
RUN rm -rf /usr/share/nginx/html/*
COPY --from=build /src/dist /usr/share/nginx/html
COPY --from=build /src/nginx.conf /etc/nginx/conf.d/default.conf
CMD ["nginx", "-g", "daemon off;"]
